package org.onestep.relief.organization.model;

import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * The geographic location of a Person or Organization.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Location {

    private float Lat;
    private float Long;

    // code snippet is is borrowed from:
    // https://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude
    // following elements are used to calculate the distance between two lat long points in distance function
    // r2d - radian to degree, d2r - degree to radian, d2km - degree to kilometer
    private static final float r2d = (float) 180.0D / (float) 3.141592653589793D;
    private static final float d2r = (float) 3.141592653589793D / (float) 180.0D;
    private static final float d2km = (float) 111189.57696D * r2d;

    /**
     * Calculates the distance between this location and another specified location.
     *
     * @param location
     *            location to measure distance to
     *
     * @return The distance between 2 lat long points
     */
    public double distance(Location location) {
        final float x = Lat * d2r;
        final float y = location.Lat * d2r;
        return Math.acos(Math.sin(x) * Math.sin(y) + Math.cos(x) * Math.cos(y) * Math.cos(d2r * (Long - location.Long)))
                * d2km / 1000;
    }
}