import { useMemoizedFn } from 'ahooks'
import { Button, Card, Form, Result, Select, Spin, message } from 'antd'
import { useState, type FC } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { PageTitle } from '../../components'
import { personRoleOptions } from '../../options'
import {
  skipToken,
  useAppSelector,
  useGetAccreditationEntitiesByIdQuery,
  useGetOrganizationQuery,
  useGetProjectQuery,
  useLazyAddCompositeRoleToEntityQuery,
} from '../../store'

export const Join: FC = () => {
  const navigate = useNavigate()
  const { organizationId, projectId } = useParams<{ organizationId?: string; projectId?: string }>()

  const { person } = useAppSelector((state) => state.auth)

  const [isJoining, setIsJoining] = useState(false)

  const { isLoading: isLoadingAccreditationEntities, data: accreditationEntities } =
    useGetAccreditationEntitiesByIdQuery(person?.uuid || skipToken)
  const { isLoading: isLoadingOrganization, data: organization } = useGetOrganizationQuery(
    organizationId || skipToken
  )
  const { isLoading: isLoadingProject, data: project } = useGetProjectQuery(projectId || skipToken)

  const [addCompositeRoleToEntity] = useLazyAddCompositeRoleToEntityQuery()

  const handleSubmit = useMemoizedFn(async ({ role }: { role: string }) => {
    try {
      setIsJoining(true)
      const entityType = organization ? 'organization' : project ? 'project' : ''
      if (!entityType) return

      const entityId = organization?.id || project?.id
      await addCompositeRoleToEntity({
        personId: person?.uuid,
        role,
        entityType,
        entityId,
      }).unwrap()

      navigate(`/${entityType}s/${organization.id}`)
      message.success('Join success')
    } catch (err) {
      console.log('Join failed', err)
      message.error('Join failed')
    } finally {
      setIsJoining(false)
    }
  })

  if (isLoadingAccreditationEntities || isLoadingOrganization || isLoadingProject) {
    return <Spin className='flex justify-center items-center h-96' size='large' />
  }

  if (!accreditationEntities?.length) {
    return (
      <Result
        title='Accreditations are required to join an organiation or project.'
        extra={
          <a href='/accreditation'>
            <Button type='primary' size='large'>
              Request Accreditation
            </Button>
          </a>
        }
      />
    )
  }

  return (
    <div>
      <PageTitle>Join Request</PageTitle>
      <Card className='rounded-xl' title={organization?.name || project?.name}>
        <Form className='flex flex-col items-center' onFinish={handleSubmit}>
          <Form.Item label='Select a role' name='role'>
            <Select style={{ width: 256 }} options={personRoleOptions} />
          </Form.Item>
          <Form.Item shouldUpdate>
            {(form) => (
              <Button
                type='primary'
                size='large'
                htmlType='submit'
                disabled={!form.getFieldValue('role')}
                loading={isJoining}
              >
                Request To Join
              </Button>
            )}
          </Form.Item>
        </Form>
      </Card>
    </div>
  )
}
