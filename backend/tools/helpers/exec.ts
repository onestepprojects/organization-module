import {commandSync} from 'execa';

export function exec(cmd: string): void;
export function exec(cmd: string, captureOutput: true): string ;
export function exec(cmd: string, captureOutput?: true): string | void {
  console.log(`[cmd]: ${cmd}\n`);
  const {stdout} = commandSync(cmd, {
    ...(captureOutput ? {} : {stdio: 'inherit'}),
    shell: true,
  });
  if (captureOutput) {
    return stdout;
  }
}
