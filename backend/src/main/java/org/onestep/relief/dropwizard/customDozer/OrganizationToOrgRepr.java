package org.onestep.relief.dropwizard.customDozer;

import org.onestep.relief.dropwizard.representations.OrganizationRepr;
import org.onestep.relief.dropwizard.representations.RepDescription;
import org.onestep.relief.dropwizard.representations.RepLocation;
import org.dozer.CustomConverter;
import org.onestep.relief.organization.model.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class OrganizationToOrgRepr implements CustomConverter {
    @Override
    public Object convert(Object dest, Object source, Class<?> aClass, Class<?> aClass1) {
        if (source == null)
            return null;

        /*
         * OrganizationRepr -> Organization
         */
        if (source instanceof OrganizationRepr) {
            OrganizationRepr op = (OrganizationRepr) source;
            Organization org = new Organization();

            org.setId(op.getId());
            org.setName(op.getName());
            org.setType((op.getType() != null) ? Organization.OrganizationType.valueOf(op.getType())
                    : Organization.OrganizationType.HYBRID);
            org.setTagline(op.getTagline());

            org.setLogo(Utils.toFromFileObj(op.getLogo()));
            org.setEmail(op.getEmail());
            org.setPhones(op.getPhones());
            org.setWebsite(op.getWebsite());

            org.setAddress(Utils.toAddress(op.getAddress()));
            org.setCreator(op.getCreatorUUID());
            org.setStatus(new OrganizationStatus(toFromOrgStatus(op.getStatusLabel()),
                    Utils.timestampToDate(op.getStatusLastActive())));

            org.setDescription(getDescription(op.getDescription()));
            org.setSupplier(op.isSupplier());

            if (op.getRequesterRoles() != null)
                org.setRequesterRoles(
                        op.getRequesterRoles().stream().map(r -> Role.valueOf(r.toUpperCase())).collect(toList()));

            org.setSocialMedia(op.getSocialMedia());
            org.setPaymentAccount(Utils.toFromPaymentAccount(op.getPaymentAccount()));

            return org;

            /*
             * Organization -> OrganizationRepr
             */
        } else if (source instanceof Organization) {
            Organization org = (Organization) source;
            OrganizationRepr oRepr = new OrganizationRepr();

            oRepr.setId(org.getId());
            oRepr.setName(org.getName());
            oRepr.setType(org.getType().toString());
            oRepr.setTagline(org.getTagline());

            oRepr.setLogo(Utils.toFromFileObj(org.getLogo()));
            oRepr.setEmail(org.getEmail());
            oRepr.setPhones(org.getPhones());
            oRepr.setWebsite(org.getWebsite());

            oRepr.setAddress(Utils.toAddress(org.getAddress()));
            oRepr.setCreatorUUID(org.getCreator());

            if (org.getStatus() != null) {
                OrganizationStatus.ActivityStatus status = org.getStatus().getStatus();
                if (status != null) {
                    oRepr.setStatusLabel(status.toString());
                }
                oRepr.setStatusLastActive(Utils.dateToTimestamp(org.getStatus().getLastActive()));
            }

            oRepr.setDescription(getDescription(org.getDescription()));
            oRepr.setSupplier(org.isSupplier());

            if (org.getRequesterRoles() != null)
                oRepr.setRequesterRoles(org.getRequesterRoles().stream().map(Enum::toString).collect(toList()));

            oRepr.setSocialMedia(org.getSocialMedia());
            oRepr.setPaymentAccount(Utils.toFromPaymentAccount(org.getPaymentAccount()));

            return oRepr;

        } else
            return null;
    }

    private Description getDescription(RepDescription rd) {
        if (rd == null)
            return null;

        Description desc = new Description();

        List<Location> locs = new ArrayList<>();
        for (RepLocation repLoc : rd.getServiceArea()) {
            locs.add(Utils.toLocation(repLoc));
        }

        desc.setDescription(rd.getDescription());
        desc.setServiceArea(locs);
        desc.setProjectTypes(rd.getProjectTypes());

        return desc;
    }

    private RepDescription getDescription(Description d) {
        if (d == null)
            return null;

        RepDescription rDesc = new RepDescription();

        List<RepLocation> repLocs = new ArrayList<>();
        for (Location loc : d.getServiceArea()) {
            repLocs.add(Utils.toLocation(loc));
        }

        rDesc.setDescription(d.getDescription());
        rDesc.setServiceArea(repLocs);
        rDesc.setProjectTypes(d.getProjectTypes());

        return rDesc;
    }

    private OrganizationStatus.ActivityStatus toFromOrgStatus(String status) {
        return (status != null) ? OrganizationStatus.ActivityStatus.valueOf(status) : null;
    }
}
