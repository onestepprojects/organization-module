import { type FC, type PropsWithChildren } from 'react'

interface Props {
  caption?: string
}

export const PageTitle: FC<PropsWithChildren<Props>> = ({
  caption = 'ONE STEP APPLICATION',
  children,
}) => {
  return (
    <div className='my-8'>
      <div className='text-xs font-normal text-slate-500'>{caption}</div>
      <div className='text-3xl font-normal text-slate-600'>{children}</div>
    </div>
  )
}
