package org.onestep.relief.organization.service;

import com.google.inject.Inject;
import org.onestep.relief.dropwizard.SystemEnvValues;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class HealthProjectService {

    private SystemEnvValues systemEnvValues;

    @Inject
    public HealthProjectService(SystemEnvValues systemEnvValues) {
        this.systemEnvValues = systemEnvValues;
    }

    /**
     * Helper method to post json data to health module
     *
     * @param authToken  - requester's auth token; includes "Bearer " prefix
     * @param methodType - "POST" for create & "PUT" for update
     * @param path       - health module endpoint path without needing baseUrl
     * @param projectID  - project id
     * @throws OrganizationServiceException - exception
     */
    public void sendHealthProject(String authToken, String methodType, String path, String projectID)
            throws OrganizationServiceException {
        try {
            // build request
            java.net.URL url = new URL(systemEnvValues.HEALTH_BASE_URL + path);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(methodType);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", authToken);
            connection.setDoOutput(true);
            String jsonInputString = "{\"id\": \"" + projectID + "\"}";

            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            // read response
            BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String output;
            StringBuilder response = new StringBuilder();
            while ((output = input.readLine()) != null) {
                response.append(output);
            }
            System.out.println("Printing response " + response);
            System.out.println("Response code: " + connection.getResponseCode());
            input.close();

        } catch (Exception e) {
            throw new OrganizationServiceException("Post Health Project data", "Cannot post health project data");
        }
    }

    public void deleteHealthProject(String authToken, String projectID) throws OrganizationServiceException {
        try {
            // build request
            URL url = new URL(systemEnvValues.HEALTH_BASE_URL + "/" + projectID);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("DELETE");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", authToken);
            connection.setDoInput(true);

            System.out.println("Response code: " + connection.getResponseCode());
        } catch (Exception e) {
            throw new OrganizationServiceException("Delete Health Project", "Cannot delete a health project");
        }
    }

}
