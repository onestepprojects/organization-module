package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.Organization;

import java.util.List;

public class OrganizationDynamoDBService {

    DynamoDBMapper mapper;

    @Inject
    public OrganizationDynamoDBService(DynamoDBMapperBuilder builder) {
        this.mapper = builder.buildMapper("Organization");
    }

    public void save(Object obj) {
        this.mapper.save(obj);
    }

    public void update(Object obj) {
        this.mapper.save(obj);
    }

    public Organization load(String organizationId) {
        return this.mapper.load(Organization.class, organizationId);
    }

    public void delete(String organizationId) {
        Organization organization = load(organizationId);
        if (organization != null) {
            this.mapper.delete(organization);
        }
    }

    public PaginatedScanList<Organization> scan() {
        return this.mapper.scan(Organization.class, new DynamoDBScanExpression());
    }

    public PaginatedScanList<Organization> scan(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Organization.class, dynamoDBScanExpression);
    }

    public List<Organization> query(DynamoDBQueryExpression<Organization> queryExpression) {
        return this.mapper.query(Organization.class, queryExpression);
    }
}