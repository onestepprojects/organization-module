import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import type { RootState } from '../store'

const tagTypes = [
  'Organization',
  'Person',
  'PersonRole',
  'Project',
  'Task',

  // /onestep/accreditation
  'Accreditation',
] as const

/** Initialize an empty api service that we'll inject endpoints into later as needed */
export const api = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_ORG_MODULE_API_URL,

    prepareHeaders: (headers, api) => {
      const state = api.getState() as RootState
      const token = state.auth.token || localStorage.getItem('authtoken')
      if (token) {
        headers.set('Authorization', `Bearer ${token}`)
      }
      return headers
    },
  }),

  endpoints: () => ({}),

  tagTypes,
})
