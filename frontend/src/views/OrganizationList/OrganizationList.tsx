import { Space, Spin } from 'antd'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { ListItemCard, PageTitle, ServerError } from '../../components'
import {
  useAppSelector,
  useGetOrganizationsByPersonQuery,
  useGetOrganizationsQuery,
  useGetSubOrganizationsQuery,
} from '../../store'

export const OrganizationList: FC = () => {
  const myPersonId = useAppSelector((state) => state.auth.person?.uuid)
  const { organizationId, personId } = useParams<{ organizationId?: string; personId?: string }>()
  const { isLoading, data, error } = personId
    ? // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetOrganizationsByPersonQuery(personId)
    : organizationId
    ? // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetSubOrganizationsQuery(organizationId)
    : // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetOrganizationsQuery()

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>
        {personId === myPersonId ? 'My ' : organizationId ? 'Sub ' : myPersonId ? '' : 'All '}
        Organizations
      </PageTitle>

      {error ? (
        <ServerError />
      ) : (
        <Spin spinning={isLoading} delay={500}>
          <Space className='mb-8' wrap size={['large', 48]}>
            {data?.map((organization) => (
              <ListItemCard
                key={organization.id}
                cover={organization.logo.url}
                coverFallback='https://onestep-org-pics.s3.amazonaws.com/defaultorg.svg'
                title={organization.name}
                description={organization.description?.description}
                link={`/organizations/${organization.id}`}
              />
            ))}
          </Space>
        </Spin>
      )}
    </div>
  )
}
