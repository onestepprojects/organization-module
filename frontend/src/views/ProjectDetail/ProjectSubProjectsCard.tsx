import { Button, Card, Empty } from 'antd'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import { type Project } from '../../store'

export interface ProjectSubProjectsCardProps {
  loading?: boolean
  project: Project
  subProjects: Project[]
  isAdmin?: boolean
}

export const ProjectSubProjectsCard: FC<ProjectSubProjectsCardProps> = ({
  loading,
  project,
  subProjects,
  isAdmin,
}) => {
  return (
    <Card
      className='rounded-lg'
      loading={loading}
      title='Sub Projects'
      extra={
        isAdmin && (
          <Link to={`/projects/create?parentType=PROJECT&parentId=${project?.id}`}>
            <Button type='primary' ghost icon={<i className='mr-2 fas fa-plus-circle' />}>
              Create
            </Button>
          </Link>
        )
      }
    >
      {subProjects?.length ? (
        <div className='flex gap-4 text-xl'>
          <div>{subProjects.length}</div>
          <div>Total</div>
          <Link to={`/projects/${project?.id}/projects`}>view</Link>
        </div>
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='No projects' />
      )}
    </Card>
  )
}
