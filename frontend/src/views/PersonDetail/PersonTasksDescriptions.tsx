import { type FC } from 'react'
import { TaskDescriptions, type TaskDescriptionsProps } from '../../components'
import { useGetPersonQuery, type Person, type Task } from '../../store'

interface PersonTasksDescriptionsProps {
  person?: Person
  tasks: Task[]
  onDeleteTask?: (projectId: string, taskId: string) => Promise<void>
}

export const PersonTasksDescriptions: FC<PersonTasksDescriptionsProps> = ({
  person,
  tasks,
  onDeleteTask,
}) => {
  return (
    <div>
      {tasks?.map((task) => (
        <PersonTaskDescriptions
          key={task.id}
          task={task}
          editLink={
            task.assignedPerson &&
            task.assignedPerson === person?.uuid &&
            `/tasks/${task.id}/update`
          }
          onDelete={async () => {
            // TODO: How to get the projectId of this task?
            // onDeleteTask?.({ projectId, taskId });
          }}
        />
      ))}
    </div>
  )
}

const PersonTaskDescriptions: FC<{ task?: Task } & Partial<TaskDescriptionsProps>> = ({
  task,
  ...restProps
}) => {
  const { data: assignedPerson } = useGetPersonQuery(task.assignedPerson)

  return <TaskDescriptions task={task} assignedPerson={assignedPerson} {...restProps} />
}
