import { Button, Card, List, message } from 'antd'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { PageTitle, ServerError } from '../../components'
import {
  skipToken,
  useGetCompositeRoleRequestsQuery,
  useGetOrganizationQuery,
  useGetProjectQuery,
  useLazyUpdateCompositeRoleStatusQuery,
  type PersonRole,
} from '../../store'

export const RequestList: FC = () => {
  const { organizationId, projectId } = useParams<{ organizationId?: string; projectId?: string }>()
  const entityId = organizationId || projectId

  const {
    isLoading: isLoadingRoleRequests,
    data: roleRequests,
    error: getRoleRequestsError,
  } = useGetCompositeRoleRequestsQuery(entityId || skipToken)
  const { data: organization } = useGetOrganizationQuery(organizationId || skipToken)
  const { data: project } = useGetProjectQuery(projectId || skipToken)

  const [updateCompositeRoleStatus] = useLazyUpdateCompositeRoleStatusQuery()

  const handleButtonClick = async (role: PersonRole, decision: 'ACTIVE' | 'INACTIVE') => {
    try {
      await updateCompositeRoleStatus({
        roleId: role.id,
        role: role.roles[0],
        entityId,
        decision,
      }).unwrap()

      message.success('Update role success')
    } catch (err) {
      console.log('Update role failed', err)
      message.error('Update role failed')
    }
  }

  return (
    <div className='mx-8'>
      <PageTitle>Role Requests</PageTitle>

      {getRoleRequestsError ? (
        <ServerError />
      ) : (
        <div className='flex justify-center'>
          <Card
            className='flex-1 max-w-xl rounded-xl'
            title={organization?.name || project?.name}
            extra={roleRequests?.length}
          >
            <List
              loading={isLoadingRoleRequests}
              dataSource={roleRequests}
              renderItem={(item) => (
                <List.Item
                  actions={[
                    <Button
                      key='accept'
                      type='primary'
                      onClick={() => handleButtonClick(item, 'ACTIVE')}
                    >
                      Accept
                    </Button>,
                    <Button
                      key='reject'
                      type='primary'
                      danger
                      onClick={() => handleButtonClick(item, 'ACTIVE')}
                    >
                      Reject
                    </Button>,
                  ]}
                >
                  <List.Item.Meta title={item.name} description={item.roles.join(', ')} />
                </List.Item>
              )}
            />
          </Card>
        </div>
      )}
    </div>
  )
}
