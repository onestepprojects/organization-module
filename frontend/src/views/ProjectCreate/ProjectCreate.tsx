import { useMemoizedFn } from 'ahooks'
import { Card } from 'antd'
import { useMemo, type FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { PageTitle, ProjectForm } from '../../components'
import { useAppSelector, useCreateProjectMutation, type Project } from '../../store'

export const ProjectCreate: FC = () => {
  const navigate = useNavigate()
  const person = useAppSelector((state) => state.auth.person)

  const { parentType, parentId } = useMemo(() => {
    const params = new URLSearchParams(window.location.search)
    return {
      parentType: params.get('parentType') as Project['parentType'],
      parentId: params.get('parentId') as Project['parentId'],
    }
  }, [])

  const [createProject] = useCreateProjectMutation()

  const handleSubmit = useMemoizedFn(async (values: Project) => {
    const newProject: Project = {
      ...values,
      parentType,
      parentId,
      creationDate: Date.now().toString(),
      requesterRoles: ['ADMIN'],
    }

    console.log('Create project', newProject)

    try {
      const res = await createProject(newProject).unwrap()
      if (parentId) {
        if (parentType === 'ORGANIZATION') {
          navigate(`/organizations/${parentId}/projects`)
        }
        if (parentType === 'PROJECT') {
          navigate(`/projects/${parentId}/projects`)
        }
      } else {
        navigate(`/projects/${res.id}`)
      }
    } catch (err) {
      console.log('Create project failed', err)
      throw err
    }
  })

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Create Project</PageTitle>

      <Card className='max-w-screen-lg rounded-lg'>
        <ProjectForm
          onFinish={handleSubmit}
          // onValuesChange={console.log}
        />
      </Card>
    </div>
  )
}
