package org.onestep.relief.organization.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Note {
    String authorID;
    Date date;
    String category;
    String title;
    String description;
}
