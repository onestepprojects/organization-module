package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.DynamoDB.ObjectConverters.*;

import java.util.*;

/**
 * Project
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    private String name;

    @DynamoDBAttribute
    private String description;

    @DynamoDBAttribute
    private Map<String, String> socialMedia;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = LocationMapTypeConverter.class)
    private Map<String, Location> targetArea = new HashMap<>();

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private ProjectType primaryType;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private ProjectType secondaryType;

    @DynamoDBAttribute
    private String tagline;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = FileTypeConverter.class)
    private File profilePicture = new File();

    @DynamoDBAttribute
    private Date creationDate;

    @DynamoDBAttribute
    private Date startDate;

    @DynamoDBAttribute
    private Date closeDate;

    /** @deprecated Put all status into `statusUpdatesHistory` */
    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = ProjectStatusUpdateTypeConverter.class)
    private ProjectStatusUpdate currentStatusUpdate;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = ProjectStatusUpdateListTypeConverter.class)
    private List<ProjectStatusUpdate> statusUpdatesHistory = new ArrayList<>();

    // name, link to org profile or a website (optional)
    @DynamoDBAttribute
    private Map<String, String> partnerOrganizations = new HashMap<>();

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private UrgencyLevel urgencyLevel;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private ProjectStage projectStage;

    @DynamoDBAttribute
    private boolean isHealth;

    @DynamoDBAttribute
    private boolean isActive = true;

    @DynamoDBIgnore
    private List<Role> requesterRoles;

    /**
     * Enums
     */
    public enum ProjectStage {
        PENDING, INITIATED, ASSESSMENT, DESIGN, IMPLEMENTATION, FOLLOWUP, COMPLETED, CLOSED
    }

    public enum UrgencyLevel {
        LOW, MEDIUM, HIGH, CRITICAL, EMERGENCY,
    }

    public enum ProjectType {
        POVERTY_ALLEVIATION("Poverty Alleviation"), ZERO_HUNGER("Zero Hunger"),
        HEALTH_AND_WELL_BEING("Health and Well-being"), EDUCATION("Education"), GENDER_EQUALITY("Gender Equality"),
        CLEAN_WATER_AND_SANITATION("Clean Water and Sanitation"),
        AFFORDABLE_AND_CLEAN_ENERGY("Affordable and Clean Energy"), ECONOMIC_GROWTH("Economic Growth"),
        INDUSTRY_INNOVATION_AND_INFRASTRUCTURE("Industry, Innovation and Infrastructure"),
        REDUCING_INEQUALITY("Reducing Inequality"),
        SUSTAINABLE_CITIES_AND_COMMUNITIES("Sustainable Cities and Communities"), WASTE_REDUCTION("Waste Reduction"),
        CLIMATE_ACTION("Climate Action"), PROTECTING_MARINE_ECOSYSTEMS("Protecting Marine Ecosystems"),
        PROTECTING_TERRESTRIAL_ECOSYSTEMS("Protecting Terrestrial Ecosystems"),
        PEACE_JUSTICE_AND_STRONG_INSTITUTIONS("Peace, Justice and Strong Institutions"),
        PROMOTING_UN_SUSTAINABLE_DEVELOPMENT_GOALS("Promoting U.N. Sustainable Development Goals"),
        DISASTER_RELIEF("Disaster Relief"),;

        private final String label;
        private static final Map<String, ProjectType> BY_LABEL = new HashMap<>();

        static {
            for (ProjectType e : values()) {
                BY_LABEL.put(e.label, e);
            }
        }

        ProjectType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public static ProjectType valueOfLabel(String label) {
            return BY_LABEL.get(label);
        }
    }
}
