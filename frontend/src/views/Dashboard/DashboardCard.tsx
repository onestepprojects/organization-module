import { Button, Card } from 'antd'
import { type FC, type ReactNode } from 'react'
import { Link } from 'react-router-dom'

export interface DashboardCardProps {
  logo: ReactNode
  title: string
  loading?: boolean
  count?: number
  link?: string
  emptyText?: string
  description?: string
  action?: {
    text: string
    link: string
  }
}

export const DashboardCard: FC<DashboardCardProps> = ({
  logo,
  title,
  loading,
  count,
  link,
  emptyText,
  description,
  action,
}) => {
  return (
    <Card className='w-80 border-neutral-200 rounded-xl shadow'>
      <Card.Meta
        avatar={logo}
        title={<div className='text-2xl font-light'>{title}</div>}
        description={
          <div>
            <div className='min-h-[3rem] text-neutral-600'>
              {loading ? (
                'loading...'
              ) : count > 0 ? (
                <div className='flex items-end'>
                  <span className='font-normal text-3xl'>{count}</span>
                  <span className='mx-2 text-2xl'>Total</span>
                  {!!link && (
                    <Link to={link}>
                      <Button type='link' size='small'>
                        view
                      </Button>
                    </Link>
                  )}
                </div>
              ) : (
                <span>{emptyText}</span>
              )}
            </div>
            {!!description && <div>{description}</div>}
            {!!action && (
              <Link to={action.link}>
                <Button type='link' size='small'>
                  {action.text}
                </Button>
              </Link>
            )}
          </div>
        }
      />
    </Card>
  )
}
