import { act, render, screen, waitFor } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import type { Project } from '../../store'
import { wrapper } from '../../tests'
import { ProjectStatusUpdatesCard } from './ProjectStatusUpdatesCard'

describe('ProjectStatusUpdatesCard', () => {
  it('renders no status if status update are empty', () => {
    render(<ProjectStatusUpdatesCard project={{} as Project} myPersonId='1' />, { wrapper })

    expect(screen.getByText('No status')).toBeInTheDocument()
  })

  it('renders current and subsequent status update', () => {
    render(
      <ProjectStatusUpdatesCard
        project={
          {
            statusUpdatesHistory: [
              {
                title: 'Initial project',
                description: 'Create empty project',
              },
            ],
          } as Project
        }
        myPersonId='1'
      />,
      { wrapper }
    )

    expect(screen.getByText('Initial project')).toBeInTheDocument()
    expect(screen.getByText('Create empty project')).toBeInTheDocument()
  })

  it('renders a create button if admin', async () => {
    render(<ProjectStatusUpdatesCard project={{} as Project} myPersonId='1' isAdmin />, { wrapper })

    const createButton = screen.getByRole('button', { name: 'Create' })

    expect(createButton).toBeInTheDocument()

    await act(() => userEvent.click(createButton))
    await waitFor(() => screen.getByRole('dialog'))

    expect(screen.getByRole('heading', { name: 'Create Status Update' })).toBeInTheDocument()
    expect(screen.getByPlaceholderText('Title')).toBeInTheDocument()
    expect(screen.getByPlaceholderText('Description')).toBeInTheDocument()
  })
})
