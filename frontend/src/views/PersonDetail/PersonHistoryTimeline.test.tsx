import { render, screen } from '@testing-library/react'
import type { Person } from '../../store'
import { wrapper } from '../../tests'
import { PersonHistoryTimeline } from './PersonHistoryTimeline'

describe('PersonHistoryTimeline', () => {
  it('renders correctly', () => {
    render(
      <PersonHistoryTimeline
        person={
          { history: [{ date: '1636588800000', message: 'Updated person profile' }] } as Person
        }
      />,
      { wrapper }
    )

    expect(screen.getByText('2021-11', { exact: false })).toBeInTheDocument()
    expect(screen.getByText('Updated person profile')).toBeInTheDocument()
  })
})
