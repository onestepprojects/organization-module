package org.onestep.relief.dropwizard.resources;

import org.dozer.DozerBeanMapper;
import org.onestep.relief.dropwizard.representations.RolesTripleResponse;
import org.onestep.relief.organization.model.*;
import org.onestep.relief.organization.model.Project;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;
import org.onestep.relief.organization.service.OrganizationServiceImpl;

import javax.inject.Inject;
import javax.validation.Validator;
import javax.validation.constraints.Pattern;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/composite")
@Produces(MediaType.APPLICATION_JSON)
public class RelationResources {
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;
    private final Validator validator;

    /**
     * Constructor initializes a Dozer mapper, a Validator, and the model interface.
     *
     * @param validator
     *            Validator class received from environment
     */
    @Inject
    public RelationResources(OrganizationServiceImpl orgInterface, Validator validator) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        this.dtoMapper = mapper;
        this.validator = validator;
        this.orgInterface = orgInterface;
    }

    @GET
    @Path("health")
    public Response health() {
        return Response.status(Response.Status.OK).entity("Healthy! XD").build();
    }

    @GET
    @Path("add-role-to-entity/{personToBeAddedId}/{role}/{entityType}/{entityId}")
    public Response addPersonRoleToOrganization(@PathParam("personToBeAddedId") String personToBeAddedId,
            @PathParam("role") @Pattern(regexp = ReliefValidators.rolesRegex) String role,
            @PathParam("entityType") @Pattern(regexp = "organization|project") String entityType,
            @PathParam("entityId") String entityId, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            Role role_ = Role.valueOf(role.toUpperCase());
            String token = authToken.orElse("UNKNOWN");

            if (entityType.equals("organization"))
                orgInterface.addPersonRoleToOrganization(token, personToBeAddedId, role_, entityId);
            else
                orgInterface.addPersonRoleToProject(token, personToBeAddedId, role_, entityId);

            return Response.status(Response.Status.OK).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("update-person-role/{oldRole}/{role}/{parentEntityId}/{personId}")
    public Response updatePersonRole(
            @PathParam("oldRole") @Pattern(regexp = ReliefValidators.rolesRegex) String oldRole,
            @PathParam("role") @Pattern(regexp = ReliefValidators.rolesRegex) String role,
            @PathParam("parentEntityId") String parentEntityId, @PathParam("personId") String personId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            Role oldRole_ = Role.valueOf(oldRole.toUpperCase());
            Role role_ = Role.valueOf(role.toUpperCase());
            String token = authToken.orElse("UNKNOWN");

            orgInterface.updatePersonRole(token, oldRole_, role_, parentEntityId, personId);

            return Response.status(Response.Status.OK).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("update-person-role-status/{role}/{parentEntityId}/{personId}/{requestStatus}")
    public Response updatePersonRoleStatus(
            @PathParam("role") @Pattern(regexp = ReliefValidators.rolesRegex) String role,
            @PathParam("parentEntityId") String parentEntityId, @PathParam("personId") String personId,
            @PathParam("requestStatus") @Pattern(regexp = ReliefValidators.requestStatuses) String requestStatus,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            String token = authToken.orElse("UNKNOWN");
            Role role_ = Role.valueOf(role.toUpperCase());
            PersonRole.RequestStatus status = PersonRole.RequestStatus.valueOf(requestStatus.toUpperCase());

            orgInterface.updatePersonRoleStatus(token, role_, parentEntityId, personId, status);

            return Response.status(Response.Status.OK).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("role-requests/{parentEntityId}")
    public Response getRoleRequestsByParent(@PathParam("parentEntityId") String parentEntityId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            Map<Person, Set<Role>> resp = orgInterface.getRoleRequestsByParent(authToken.orElse("UNKNOWN"),
                    parentEntityId);

            return Response.status(Response.Status.OK).entity(RolesTripleResponse.personTriples(resp)).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("entity-role-invites/{entityType}")
    public Response getEntityRoleInvites(
            @PathParam("entityType") @Pattern(regexp = "organization|project") String entityType,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            if (entityType.equals("organization")) {
                Map<Organization, Set<Role>> resp = orgInterface
                        .getOrganizationRoleInvites(authToken.orElse("UNKNOWN"));

                return Response.status(Response.Status.OK).entity(RolesTripleResponse.organizationTriples(resp))
                        .build();
            } else {
                Map<Project, Set<Role>> resp = orgInterface.getProjectRoleInvites(authToken.orElse(""));

                return Response.status(Response.Status.OK).entity(RolesTripleResponse.projectTriples(resp)).build();
            }

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }
}
