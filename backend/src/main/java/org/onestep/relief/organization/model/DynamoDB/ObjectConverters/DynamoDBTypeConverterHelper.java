package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;

public class DynamoDBTypeConverterHelper {
    // create singleton instance
    private static DynamoDBTypeConverterHelper helper = new DynamoDBTypeConverterHelper();

    // private constructor; singleton pattern
    private DynamoDBTypeConverterHelper() {
    }

    // method to retrieve the singleton
    public static DynamoDBTypeConverterHelper getInstance() {
        return helper;
    }

    public String convert(Object object, String name, Logger log) {
        ObjectMapper objectMapper = new ObjectMapper();

        String jsonStr;
        try {
            jsonStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
            log.debug("marshalled " + name + "to " + jsonStr);
        } catch (JsonProcessingException e) {
            log.error("error marshalling description " + object, e);
            throw new RuntimeException("Cannot write item");
        }

        return jsonStr;
    }
}
