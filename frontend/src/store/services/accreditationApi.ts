import type { AccreditationEntity } from '../types'
import { api } from './api'

export const accreditationApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    /** Possible id: organization, person. */
    getAccreditationEntitiesById: builder.query<AccreditationEntity[], string>({
      query: (id) =>
        `${process.env.REACT_APP_ACCREDITATION_MODULE_ACCREDITATION_API_URL}/application/${id}/entity`,
      providesTags: ['Accreditation'],
    }),
  }),
})

export const { useGetAccreditationEntitiesByIdQuery } = accreditationApi
