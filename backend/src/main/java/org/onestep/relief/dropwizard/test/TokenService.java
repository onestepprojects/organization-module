package org.onestep.relief.dropwizard.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class TokenService {
    private final Map<String, String> cognitoPairs;

    public TokenService() {
        cognitoPairs = new HashMap<>();
    }

    public String getAuthenticationToken() {
        String token = "test-t-" + UUID.randomUUID().toString();
        String permanentId = "test-c-" + UUID.randomUUID().toString();

        cognitoPairs.put(token, permanentId);

        return token;
    }

    public String getAnyAuthenticationToken() {
        return cognitoPairs.keySet().iterator().next();
    }

    public Optional<String> getPermanentId(String authToken) {
        return Optional.ofNullable(cognitoPairs.get(authToken));
    }

    public Map<String, String> getCognitoPairs() {
        return cognitoPairs;
    }
}
