package org.onestep.relief.dropwizard.customDozer;

import org.dozer.CustomConverter;
import org.onestep.relief.dropwizard.representations.EventRepr;
import org.onestep.relief.organization.model.Event;

public class EventToEventRepr implements CustomConverter {
    @Override
    public Object convert(Object dest, Object source, Class<?> aClass, Class<?> aClass1) {
        if (source == null)
            return null;

        if (source instanceof EventRepr) {
            EventRepr rep = (EventRepr) source;
            Event event = new Event();

            event.setDate(Utils.timestampToDate(rep.getDate()));
            event.setMessage(rep.getMessage());

            if (rep.getActivity() != null)
                event.setActivity(Event.Activity.valueOf(rep.getActivity()));

            event.setParentID(rep.getParentID());
            event.setParentType(rep.getParentType());
            event.setRewardPoints(rep.getRewardPoints());

            return event;

        } else if (source instanceof Event) {
            return toEventRepresentation((Event) source);

        } else
            return null;
    }

    public static EventRepr toEventRepresentation(Event event) {
        return new EventRepr(Utils.dateToTimestamp(event.getDate()), event.getMessage(),
                (event.getActivity() != null) ? event.getActivity().toString() : null, event.getParentID(),
                event.getParentType(), event.getRewardPoints());
    }
}
