import { render, screen } from '@testing-library/react'
import { Route } from 'react-router-dom'
import {
  useGetProjectsByOrganizationQuery,
  useGetProjectsByPersonQuery,
  useGetProjectsQuery,
  useGetSubProjectsQuery,
  type Project,
} from '../../store'
import { wrapper } from '../../tests'
import { ProjectList } from './ProjectList'

jest.mock('../../store', () => {
  const store = jest.requireActual('../../store')
  return {
    ...store,
    useGetProjectsByPersonQuery: jest.fn(() => ({
      data: [{ id: '1', name: 'person project' } as Project],
    })),
    useGetProjectsByOrganizationQuery: jest.fn(() => ({})),
    useGetSubProjectsQuery: jest.fn(() => ({})),
    useGetProjectsQuery: jest.fn(() => ({})),
  }
})

describe('ProjectList', () => {
  it('renders my projects', () => {
    render(<Route path='/:personId/projects' component={ProjectList} />, {
      wrapper: wrapper({ initialEntries: ['/personId/projects'] }),
    })

    expect(useGetProjectsByPersonQuery).lastCalledWith('personId')
    expect(screen.getByText('My Projects')).toBeInTheDocument()
    expect(screen.getByText('person project')).toBeInTheDocument()
  })

  it('renders user projects', () => {
    render(<Route path='/:personId/projects' component={ProjectList} />, {
      wrapper: wrapper({ initialEntries: ['/otherPersonId/projects'] }),
    })

    expect(useGetProjectsByPersonQuery).lastCalledWith('otherPersonId')
    expect(screen.getByText('User Projects')).toBeInTheDocument()
  })

  it('renders org projects', () => {
    render(<Route path='/:organizationId/projects' component={ProjectList} />, {
      wrapper: wrapper({ initialEntries: ['/organizationId/projects'] }),
    })

    expect(useGetProjectsByOrganizationQuery).lastCalledWith('organizationId')
    expect(screen.getByText('Org Projects')).toBeInTheDocument()
  })

  it('renders sub projects', () => {
    render(<Route path='/:projectId/projects' component={ProjectList} />, {
      wrapper: wrapper({ initialEntries: ['/projectId/projects'] }),
    })

    expect(useGetSubProjectsQuery).lastCalledWith('projectId')
    expect(screen.getByText('Sub Projects')).toBeInTheDocument()
  })

  it('renders all projects', () => {
    render(<ProjectList />, { wrapper })

    expect(useGetProjectsQuery).lastCalledWith()
    expect(screen.getByText('All Projects')).toBeInTheDocument()
  })
})
