export const projectTypeOptions = [
  {
    value: 'POVERTY_ALLEVIATION',
    label: 'Poverty Alleviation',
  },
  {
    value: 'ZERO_HUNGER',
    label: 'Zero Hunger',
  },
  {
    value: 'HEALTH_AND_WELL_BEING',
    label: 'Health and Well-being',
  },
  {
    value: 'EDUCATION',
    label: 'Education',
  },
  {
    value: 'GENDER_EQUALITY',
    label: 'Gender Equality',
  },
  {
    value: 'CLEAN_WATER_AND_SANITATION',
    label: 'Clean Water and Sanitation',
  },
  {
    value: 'AFFORDABLE_AND_CLEAN_ENERGY',
    label: 'Affordable and Clean Energy',
  },
  {
    value: 'ECONOMIC_GROWTH',
    label: 'Economic Growth',
  },
  {
    value: 'INDUSTRY_INNOVATION_AND_INFRASTRUCTURE',
    label: 'Industry, Innovation and Infrastructure',
  },
  {
    value: 'REDUCING_INEQUALITY',
    label: 'Reducing Inequality',
  },
  {
    value: 'SUSTAINABLE_CITIES_AND_COMMUNITIES',
    label: 'Sustainable Cities and Communities',
  },
  {
    value: 'WASTE_REDUCTION',
    label: 'Waste Reduction',
  },
  {
    value: 'CLIMATE_ACTION',
    label: 'Climate Action',
  },
  {
    value: 'PROTECTING_MARINE_ECOSYSTEMS',
    label: 'Protecting Marine Ecosystems',
  },
  {
    value: 'PROTECTING_TERRESTRIAL_ECOSYSTEMS',
    label: 'Protecting Terrestrial Ecosystems',
  },
  {
    value: 'PEACE_JUSTICE_AND_STRONG_INSTITUTIONS',
    label: 'Peace, Justice and Strong Institutions',
  },
  {
    value: 'PROMOTING_UN_SUSTAINABLE_DEVELOPMENT_GOALS',
    label: 'Promoting U.N. Sustainable Development Goals',
  },
  {
    value: 'DISASTER_RELIEF',
    label: 'Disaster Relief',
  },
]
