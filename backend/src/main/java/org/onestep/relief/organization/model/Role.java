package org.onestep.relief.organization.model;

/**
 * The role a person on the OneStep platform has as either part of an organization or as an individual.
 */

public enum Role {
    COMMUNITYLEADER, COMMUNITYMEMBER, SPONSOR, DONOR, EXPERT, RELIEFORGADMIN, RELIEFWORKER, HEALTHWORKER, VOLUNTEER,
    SUPPLIER, ADMIN, GLOBAL_ADMIN
}
