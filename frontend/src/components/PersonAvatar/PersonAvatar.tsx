import { Avatar } from 'antd'
import { type FC } from 'react'
import type { Person } from '../../store'

export interface PersonAvatarProps {
  person: Person
}

export const PersonAvatar: FC<PersonAvatarProps> = ({ person }) => {
  return (
    <span className='inline-flex items-center'>
      <Avatar className='mr-2 w-5 h-5' src={person?.profilePicture?.url} />{' '}
      <span>{person?.name}</span>
    </span>
  )
}
