import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { PageTitle } from '../../components'
import {
  skipToken,
  useAppSelector,
  useCreateProjectStatusUpdateMutation,
  useDeleteProjectMutation,
  useGetCompositeRoleRequestsQuery,
  useGetPersonRolesByParentQuery,
  useGetProjectQuery,
  useGetSubProjectsQuery,
  useGetTasksByParentQuery,
  useRemoveProjectStatusUpdateMutation,
} from '../../store'
import { ProjectDetailCard } from './ProjectDetailCard'
import { ProjectMapsCard } from './ProjectMapsCard'
import { ProjectStatusUpdatesCard } from './ProjectStatusUpdatesCard'
import { ProjectSubProjectsCard } from './ProjectSubProjectsCard'
import { ProjectTasksCard } from './ProjectTasksCard'

export const ProjectDetail: FC = () => {
  const { projectId } = useParams<{ projectId: string }>()
  const myPersonId = useAppSelector((state) => state.auth.person?.uuid)

  const { isLoading: isLoadingProject, data: project } = useGetProjectQuery(projectId || skipToken)
  const { isLoading: isLoadingSubProjects, data: subProjects } = useGetSubProjectsQuery(
    projectId || skipToken
  )
  const { isLoading: isLoadingPersonRoles, data: personRoles = [] } =
    useGetPersonRolesByParentQuery(projectId || skipToken)
  const { isLoading: isLoadingCompositeRoleRequests, data: compositeRoleRequests } =
    useGetCompositeRoleRequestsQuery(projectId || skipToken)
  const { isLoading: isLoadingTasks, data: tasks } = useGetTasksByParentQuery(
    projectId || skipToken
  )

  const [createProjectStatusUpdate] = useCreateProjectStatusUpdateMutation()
  const [removeProjectStatusUpdate] = useRemoveProjectStatusUpdateMutation()
  const [deleteProject] = useDeleteProjectMutation()

  const canJoin = personRoles?.length && personRoles.every(({ id }) => id !== myPersonId)
  const isAdmin = project?.requesterRoles.some((role) => ['ADMIN', 'GLOBAL_ADMIN'].includes(role))

  return (
    <div className='mx-8'>
      <PageTitle>Project</PageTitle>

      <div className='mb-8 flex flex-col sm:flex-row gap-4'>
        <div className='flex-1 flex flex-col gap-4'>
          <ProjectDetailCard
            loading={isLoadingProject}
            project={project}
            compositeRoleRequests={compositeRoleRequests}
            canJoin={canJoin}
            isAdmin={isAdmin}
            deleteProject={(id) => deleteProject(id).unwrap()}
          />
          <ProjectStatusUpdatesCard
            loading={isLoadingProject}
            project={project}
            myPersonId={myPersonId}
            isAdmin={isAdmin}
            createProjectStatusUpdate={(statusUpdate) =>
              createProjectStatusUpdate({ ...statusUpdate, projectId: project?.id }).unwrap()
            }
            removeProjectStatusUpdate={(statusUpdate) =>
              removeProjectStatusUpdate({ ...statusUpdate, projectId: project?.id }).unwrap()
            }
          />
        </div>

        <div className='flex-1 flex flex-col gap-4'>
          <ProjectMapsCard loading={isLoadingProject} project={project} />
          <ProjectTasksCard
            loading={isLoadingTasks}
            project={project}
            tasks={tasks}
            isAdmin={isAdmin}
          />
          <ProjectSubProjectsCard
            loading={isLoadingSubProjects}
            project={project}
            subProjects={subProjects}
            isAdmin={isAdmin}
          />
        </div>
      </div>
    </div>
  )
}
