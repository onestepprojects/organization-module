package org.onestep.relief.dropwizard.resources;

import org.onestep.relief.dropwizard.representations.OrganizationRepr;
import org.onestep.relief.dropwizard.representations.PersonRepr;
import org.onestep.relief.dropwizard.representations.ProjectRepr;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ReliefValidators {
    final static List<String> phoneLabels = Arrays.asList("phone", "mobile", "sat_phone");
    final static List<String> roles = Arrays.asList("COMMUNITYLEADER", "COMMUNITYMEMBER", "SPONSOR", "DONOR", "EXPERT",
            "RELIEFORGADMIN", "RELIEFWORKER", "HEALTHWORKER", "SUPPLIER", "ADMIN", "GLOBAL_ADMIN", "VOLUNTEER");

    final static String requestStatuses = "INVITED|REQUESTED|ACTIVE|INACTIVE";

    final static String rolesRegex = "COMMUNITYLEADER|COMMUNITYMEMBER|SPONSOR|"
            + "DONOR|EXPERT|RELIEFORGADMIN|RELIEFWORKER|HEALTHWORKER|SUPPLIER|" + "ADMIN|GLOBAL_ADMIN|VOLUNTEER";

    public static boolean invalidPhoneLabels(OrganizationRepr org) {
        return !phoneLabels.containsAll(org.getPhones().keySet());
    }

    public static boolean invalidPhoneLabels(PersonRepr person) {
        Set<String> labels1 = person.getPhones().keySet();
        Set<String> labels2 = person.getEcPhones().keySet();

        boolean isValid = phoneLabels.containsAll(labels1) && phoneLabels.containsAll(labels2);

        return !isValid;
    }

    public static boolean invalidRoles(ProjectRepr projectRepr) {
        return !roles.containsAll(projectRepr.getRequesterRoles());
    }
}
