import { type Picture, type SocialMedia } from './common'

export interface ProjectStatusUpdate {
  authorID: string | null
  date: number
  title: string
  description: string
}

export interface Project {
  parentType: 'ORGANIZATION' | 'PROJECT'
  parentId: string
  id: string
  name: string
  description: string
  targetArea: {
    area_1: {
      latitude: number
      longitude: number
    }
  }
  primaryType: string
  secondaryType: string
  tagline: string
  profilePicture: Picture
  /** Timestamp string */
  creationDate: string
  startDate: number
  closeDate: number
  /** @deprecated Put all status into `statusUpdatesHistory` */
  currentStatusUpdate?: ProjectStatusUpdate
  statusUpdatesHistory: ProjectStatusUpdate[]
  partnerOrganizations: any
  urgencyLevel: string
  projectStage: string
  requesterRoles: string[]
  socialMedia: SocialMedia
  health: boolean
}
