package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventRepr {
    @Pattern(regexp = "[0-9]{13}")
    private String date;

    private String message;

    @Pattern(regexp = StaticValues.eventActivities)
    private String activity;

    private String parentID;

    private String parentType;

    private int rewardPoints;

}
