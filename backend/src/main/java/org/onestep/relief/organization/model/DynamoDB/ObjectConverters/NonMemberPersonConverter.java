package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.NonMemberPerson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type NonMemberPerson to a string and vice-versa.
 */

public class NonMemberPersonConverter implements DynamoDBTypeConverter<String, NonMemberPerson> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(NonMemberPerson person) {
        return DynamoDBTypeConverterHelper.getInstance().convert(person, "person", log);
    }

    @Override
    public NonMemberPerson unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        NonMemberPerson person;
        try {
            person = objectMapper.readValue(s, NonMemberPerson.class);
            log.debug("unmarshalled non  member person to to " + person);
        } catch (IOException e) {
            log.error("error de-marshalling address: " + s, e);
            return null;
        }

        return person;
    }
}