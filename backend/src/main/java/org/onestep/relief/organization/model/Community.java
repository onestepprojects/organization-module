package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.DynamoDB.ObjectConverters.LocationMapTypeConverter;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor

/**
 * Class represents the project's community
 */
public class Community {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    String id;

    @DynamoDBAttribute
    String name;

    @DynamoDBAttribute
    String description;

    @DynamoDBAttribute
    int populationSize;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = LocationMapTypeConverter.class)
    Map<String, Location> location = new HashMap<>();

    // todo : will need a converter
    @DynamoDBAttribute
    List<Note> notes = new ArrayList<>();

    @DynamoDBAttribute
    private boolean isActive = true;

    @DynamoDBIgnore
    private List<Role> requesterRoles;
}
