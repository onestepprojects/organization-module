package org.onestep.relief.organization.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RewardsResponse {
    private String message;
    private List<Reward> data;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Reward {
        private String reason;
        private String rewardType;
        private int amount;
        private String issuerId;
        private String receiverId;
        private String receiverType;
        private String transactionId;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date date;
    }

}