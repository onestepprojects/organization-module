export const personRoleOptions = [
  { label: 'Communityleader', value: 'COMMUNITYLEADER' },
  { label: 'Communitymember', value: 'COMMUNITYMEMBER' },
  { label: 'Sponsor', value: 'SPONSOR' },
  { label: 'Donor', value: 'DONOR' },
  { label: 'Expert', value: 'EXPERT' },
  { label: 'Relieforgadmin', value: 'RELIEFORGADMIN' },
  { label: 'Reliefworker', value: 'RELIEFWORKER' },
  { label: 'Healthworker', value: 'HEALTHWORKER' },
  { label: 'Supplier', value: 'SUPPLIER' },
  { label: 'Admin', value: 'ADMIN' },
  { label: 'Global Admin', value: 'GLOBAL_ADMIN' },
  { label: 'Volunteer', value: 'VOLUNTEER' },
]
