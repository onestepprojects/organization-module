import { Timeline } from 'antd'
import dayjs from 'dayjs'
import { type FC } from 'react'
import { type Person } from '../../store'

interface PersonHistoryTimelineProps {
  person: Person
  limit?: number
}

const formatUnixTimestamp = (timestamp: string | number) =>
  timestamp ? dayjs(Number(timestamp)).format('YYYY-MM-DD hh:mm') : ''

export const PersonHistoryTimeline: FC<PersonHistoryTimelineProps> = ({ person, limit = 100 }) => {
  return (
    <Timeline>
      {person?.history
        .slice(0, limit)
        .sort((a, b) => b.date - a.date)
        .map(({ date, message }, index) => {
          const dateTime = formatUnixTimestamp(date)
          return (
            <Timeline.Item key={index}>
              <time className='mr-2 text-slate-500' dateTime={dateTime}>
                {dateTime}
              </time>
              <span>{message}</span>
            </Timeline.Item>
          )
        })}
    </Timeline>
  )
}
