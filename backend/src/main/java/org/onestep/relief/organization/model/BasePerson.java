package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.DynamoDB.ObjectConverters.AddressTypeConverter;

import java.util.*;

/**
 * Class represents the common properties shared among Non platform user Persons (e.g. emergency contacts) and Platform
 * user persons
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "Person")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Person", description = "Persons")
public abstract class BasePerson {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    private String name;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = AddressTypeConverter.class)
    private Address currentAddress;

    @DynamoDBAttribute
    private String email;

    @DynamoDBAttribute
    private Map<String, String> phones = new HashMap<>();

    @DynamoDBAttribute
    private boolean isActive = true;
}
