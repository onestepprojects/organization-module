import { useMemoizedFn } from 'ahooks'
import { Tree } from 'antd'
import { useState, type FC, type Key } from 'react'
import {
  useLazyGetPersonsOfParentQuery,
  useLazyGetProjectsByOrganizationQuery,
  useLazyGetRootOrganizationsQuery,
  useLazyGetSubOrganizationsQuery,
  useLazyGetSubProjectsQuery,
  useLazyGetTasksByParentQuery,
} from '../../store'
import { buildRootNode } from './buildRootNode'
import { type TreeNode } from './typings'

export const OrganizationTree: FC = () => {
  const [treeData, setTreeData] = useState<TreeNode[]>([buildRootNode()])

  const [getRootOrganizations] = useLazyGetRootOrganizationsQuery()
  const [getSubOrganizations] = useLazyGetSubOrganizationsQuery()
  const [getProjectsByOrganization] = useLazyGetProjectsByOrganizationQuery()
  const [getSubProjectsQuery] = useLazyGetSubProjectsQuery()
  const [getPersonsOfParent] = useLazyGetPersonsOfParentQuery()
  const [getTasksByParent] = useLazyGetTasksByParentQuery()

  const getNodeByKey = useMemoizedFn((key: Key): TreeNode | undefined => {
    const allNodes: TreeNode[] = [...treeData]
    for (let i = 0; i < allNodes.length; i += 1) {
      const item = allNodes[i]
      allNodes.push(...(item.children ?? []))
    }
    return allNodes.find((it) => it.key === key)
  })

  const onLoadData = useMemoizedFn(async ({ key, children }) => {
    if (!children) {
      try {
        const node = getNodeByKey(key)
        switch (node.type) {
          case 'root': {
            const roots = await getRootOrganizations()
            node.organizations = roots.data
            node.children = node.calcChildren?.()
            return
          }
          case 'OrganizationNode': {
            const organization = node.organization
            node.subOrganizations = (await getSubOrganizations(organization.id)).data
            node.projects = (await getProjectsByOrganization(organization.id)).data
            node.persons = (await getPersonsOfParent(organization.id)).data
            node.children = node.calcChildren?.()
            return
          }
          case 'ProjectNode': {
            const project = node.project
            node.subProjects = (await getSubProjectsQuery(project.id)).data
            node.persons = (await getPersonsOfParent(project.id)).data
            node.tasks = (await getTasksByParent(project.id)).data
            node.children = node.calcChildren?.()
            return
          }
        }
      } finally {
        // Force generating a new state object to reflect inner data changes
        setTreeData((data) => [...data])
      }
    }
  })

  return <Tree<TreeNode> loadData={onLoadData} treeData={treeData} defaultExpandAll />
}
