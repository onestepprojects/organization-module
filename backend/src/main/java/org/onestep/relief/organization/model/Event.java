package org.onestep.relief.organization.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * An Event represents a Persons' participation in an activity related to a Project and/or an Organization.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {

    private Date date;
    private String message;
    private Activity activity;
    private String parentID;
    private String parentType;
    private int rewardPoints;

    public enum Activity {
        ORG_CREATED(20, "Created new organization"), ORG_UPDATED(2, "Updated an organization"),
        PERSON_CREATED(30, "Registered on the platform"), PERSON_UPDATED(2, "Updated person profile"),
        PROJECT_CREATED(15, "Created a new project"), PROJECT_UPDATED(2, "Updated a project"),
        TASK_CREATED(5, "Created a new project task"), TASK_UPDATED(1, "Updated a project task"),
        TASK_COMPLETED(10, "Completed a project task"), ORG_JOINED(10, "Joined an organization"),
        PROJECT_JOINED(10, "Joined a project"), ROLE_CHANGED(2, "Updated a member's role"),
        ORG_LEFT(1, "Left an organization"), PROJECT_LEFT(1, "Left a project"), POST(1, "Posted on a discussion board"),
        REWARD(1, "Issued a reward to a user");

        private final int points;
        private final String description;

        Activity(int points, String description) {
            this.points = points;
            this.description = description;
        }

        public int getPoints() {
            return points;
        }

        public String getDescription() {
            return description;
        }
    }

    public Event(Date date, String message, Activity activity, String parentID, String parentType) {
        this.date = date;
        this.message = message;
        this.activity = activity;
        this.parentID = parentID;
        this.parentType = parentType;
        this.rewardPoints = activity.points;
    }
}