import type { Task } from '../types'
import { api } from './api'

export const taskApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getTasksByPerson: builder.query<Task[], string>({
      query: (personId) => `/tasks/by-person/${personId}`,
      providesTags: ['Task'],
    }),

    /** Possible parent: organization, project. */
    getTasksByParent: builder.query<Task[], string>({
      query: (parentId) => `/tasks/by-parent/${parentId}`,
      providesTags: ['Task'],
    }),

    getTask: builder.query<Task, string>({
      query: (id) => `/tasks/${id}`,
      providesTags: ['Task'],
    }),

    createTask: builder.mutation<Task, Task & { projectId: string }>({
      query: ({ projectId, ...body }) => ({
        url: `/tasks/create/${projectId}`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Task'],
    }),

    updateTask: builder.mutation<Task, Task>({
      query: (body) => ({
        url: `/tasks/update/${body.id}`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Task'],
    }),

    deleteTask: builder.mutation<void, { projectId: string; taskId: string }>({
      query: ({ projectId, taskId }) => ({
        url: `/tasks/delete/${projectId}/${taskId}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Task'],
    }),
  }),
})

export const {
  useGetTasksByPersonQuery,
  useGetTasksByParentQuery,
  useLazyGetTasksByParentQuery,
  useGetTaskQuery,

  useCreateTaskMutation,
  useUpdateTaskMutation,
  useDeleteTaskMutation,
} = taskApi
