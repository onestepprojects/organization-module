package org.onestep.relief.organization.service;

import com.google.inject.ImplementedBy;
import org.onestep.relief.organization.model.*;
import org.onestep.relief.organization.model.Community;
import org.onestep.relief.organization.model.Project;
import org.onestep.relief.organization.model.ProjectStatusUpdate;
import org.onestep.relief.organization.model.Task;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Organization module service interface
 */
@ImplementedBy(OrganizationServiceImpl.class)
public interface OrgServiceInterface {
    // CRUD for organization
    Organization createOrganization(String authToken, Organization organization)
            throws OrganizationServiceException, AuthenticationException;

    Organization updateOrganization(String authToken, Organization organization)
            throws OrganizationServiceException, AuthenticationException;

    Organization updateOrganizationImage(String authToken, String orgID, File image)
            throws OrganizationServiceException, AuthenticationException;

    void provisionOrganizationAsset(String orgID, String assetID) throws OrganizationServiceException;

    void deleteOrganization(String authToken, String orgID)
            throws OrganizationServiceException, AuthenticationException;

    Organization getOrganizationById(String authToken, String orgID) throws OrganizationServiceException;

    List<Organization> getAllOrganizations();

    List<Organization> getRootOrganizations();

    Organization getParentOrganization(String authToken, String childOrgID) throws OrganizationServiceException;

    List<Organization> getSubOrganizations(String orgID);

    List<Organization> getOrganizationsByPerson(String personID) throws OrganizationServiceException;

    Map<Organization, Set<Role>> getOrganizationRolesByPerson(String personID) throws OrganizationServiceException;

    // CRUD for Person
    Person createPerson(String authToken, Person person) throws OrganizationServiceException, AuthenticationException;

    Person updatePerson(String authToken, Person person) throws OrganizationServiceException, AuthenticationException;

    Person updatePersonImage(String authToken, String personID, File image)
            throws OrganizationServiceException, AuthenticationException;

    void provisionPersonAsset(String personID, String assetID) throws OrganizationServiceException;

    void deletePerson(String authToken, String personID) throws OrganizationServiceException, AuthenticationException;

    Person getPersonById(String authToken, String personID) throws OrganizationServiceException;

    Person getPersonByCognitoId(String authToken, String cognitoID)
            throws OrganizationServiceException, AuthenticationException;

    List<Person> getAllPersons();

    Map<Person, Set<Role>> getPersonsByParent(String parentID) throws OrganizationServiceException;

    Person getRequesterPerson(String authToken) throws AuthenticationException, OrganizationServiceException;

    Event createEvent(String auth_token, String personID, Event.Activity activity, String message, String parentType,
            String parentId) throws OrganizationServiceException;

    // CRUD for Project
    Project createProject(String authToken, String parentID, String parentType, Project project)
            throws OrganizationServiceException, AuthenticationException;

    Project updateProject(String authToken, Project project)
            throws OrganizationServiceException, AuthenticationException;

    Project updateProjectImage(String authToken, String projectID, File image)
            throws OrganizationServiceException, AuthenticationException;

    void deleteProject(String authToken, String projectID) throws OrganizationServiceException, AuthenticationException;

    Project getProjectByID(String authToken, String projectID) throws OrganizationServiceException;

    List<Project> getAllProjects();

    List<Project> getProjectsByParent(String parentID) throws OrganizationServiceException;

    List<Project> getProjectsByPerson(String personID) throws OrganizationServiceException;

    ProjectStatusUpdate createProjectStatusUpdate(String authToken, String projectID, ProjectStatusUpdate statusUpdate)
            throws OrganizationServiceException, AuthenticationException;

    ProjectStatusUpdate removeProjectStatusUpdate(String authToken, String projectID, ProjectStatusUpdate statusUpdate)
            throws OrganizationServiceException, AuthenticationException;

    // CRUD for Tasks
    Task createTask(String authToken, String projectID, Task task)
            throws OrganizationServiceException, AuthenticationException;

    Task updateTask(String authToken, Task task) throws OrganizationServiceException, AuthenticationException;

    void deleteTask(String authToken, String taskID) throws OrganizationServiceException, AuthenticationException;

    Task getTaskByID(String authToken, String taskID) throws OrganizationServiceException;

    List<Task> getTasksByParent(String parentID) throws OrganizationServiceException;

    List<Task> getTasksByPerson(String personID) throws OrganizationServiceException;

    // CRUD for Communities
    Community createCommunity(String authToken, Community community, Project projectID)
            throws OrganizationServiceException, AuthenticationException;

    Community updateCommunity(String authToken, Community community)
            throws OrganizationServiceException, AuthenticationException;

    void deleteCommunity(String authToken, String communityId)
            throws OrganizationServiceException, AuthenticationException;

    List<Community> getCommunitiesByProject(String projectID) throws OrganizationServiceException;

    // Create and update composite pattern relationships
    void addSubOrganization(String authToken, String parentOrgID, String orgID)
            throws OrganizationServiceException, AuthenticationException;

    void addPersonRoleToOrganization(String authToken, String personID, Role role, String orgID)
            throws OrganizationServiceException, AuthenticationException;

    void addPersonRoleToProject(String authToken, String personID, Role role, String projectID)
            throws OrganizationServiceException, AuthenticationException;

    void addPersonRoleToCommunity(String authToken, String personID, Role role, String communityID)
            throws OrganizationServiceException, AuthenticationException;

    void updatePersonRole(String authToken, Role oldRole, Role role, String parentID, String personID)
            throws OrganizationServiceException, AuthenticationException;

    void updatePersonRoleStatus(String authToken, Role role, String parentID, String personID,
            PersonRole.RequestStatus status) throws OrganizationServiceException, AuthenticationException;

    Map<Person, Set<Role>> getRoleRequestsByParent(String authToken, String parentID)
            throws OrganizationServiceException, AuthenticationException;

    Map<Project, Set<Role>> getProjectRoleInvites(String authToken)
            throws OrganizationServiceException, AuthenticationException;

    Map<Organization, Set<Role>> getOrganizationRoleInvites(String authToken)
            throws OrganizationServiceException, AuthenticationException;

    // issue a reward to an Organization or Person
    void issueReward(String authToken, String reason, int amount, String receiverID, String receiverType)
            throws OrganizationServiceException, AuthenticationException;
}
