package org.onestep.relief.organization.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.dropwizard.SystemEnvValues;
import org.onestep.relief.organization.model.*;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class RewardService {

    // reward service configuration
    private final String REWARD_TYPE = "Step Token";

    private SystemEnvValues systemEnvValues;
    private ServiceTokenService serviceTokenService;
    private CognitoService cognitoUserFetcher;

    @Inject
    public RewardService(SystemEnvValues systemEnvValues,
            ServiceTokenService serviceTokenService, CognitoService cognitoUserFetcher) {
        this.systemEnvValues = systemEnvValues;
        this.serviceTokenService = serviceTokenService;
        this.cognitoUserFetcher = cognitoUserFetcher;
    }

    /**
     * Calls the Rewards Service to issue a Step Token reward to a Person or
     * Organization.
     *
     * @param authToken    - requester's auth token; includes "Bearer " prefix
     * @param reason       - the reason the reward is being issued
     * @param amount       - the number of Step Tokens issued
     * @param receiverID   - the ID of the person or organization being rewarded
     * @param receiverType - indicates whether the receiver is a person or
     *                     organization
     * @throws OrganizationServiceException - thrown due to a data error
     * @throws AuthenticationException      - thrown due to an authentication error
     */
    public void issueReward(String authToken, String reason, int amount, String receiverID, String receiverType)
            throws OrganizationServiceException, AuthenticationException {
        // confirm that the requester is a global admin
        CognitoUser user = cognitoUserFetcher.fetchUser(authToken);
        if (!user.isGlobalAdmin()) {
            throw new AuthenticationException("Issue Reward", "Authentication failed");
        }

        issueReward(reason, amount, receiverID, receiverType);
    }

    /**
     * Attempts to issue a reward to a person on the OneStep platform for completing
     * an activity, such as updating their person profile or completing a task. The
     * issue reward request may fail if they have not yet provisioned their payment
     * account, in which case no reward is issued.
     *
     * @param activity   - the user activity being rewarded
     * @param receiverID - the person to reward
     */
    public void issueActivityReward(Event.Activity activity, String receiverID) {
        try {
            issueReward(activity.getDescription(), activity.getPoints(), receiverID, "Person");
        } catch (OrganizationServiceException e) {
            // request may fail if receiver has no provisioned payment account; do nothing
        }
    }

    /**
     * Calls the Rewards Service to issue a Step Token reward to a Person or
     * Organization. (Private version that does not verify auth token.)
     *
     * @param reason       - the reason the reward is being issued
     * @param amount       - the number of Step Tokens issued
     * @param receiverID   - the ID of the person or organization being rewarded
     * @param receiverType - indicates whether the receiver is a person or
     *                     organization
     */
    private void issueReward(String reason, int amount, String receiverID, String receiverType)
            throws OrganizationServiceException {
        // build and submit issue reward request
        int responseCode;
        String message;
        try {
            // build request
            URL url = new URL(systemEnvValues.APP_BASE_URL + systemEnvValues.ISSUE_REWARD_PATH);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", serviceTokenService.getServiceToken());
            connection.setDoOutput(true);

            // build request body
            IssueRewardRequest reward = new IssueRewardRequest();
            reward.setReason(reason);
            reward.setRewardType(REWARD_TYPE);
            reward.setAmount(amount);
            reward.setReceiverId(receiverID);
            reward.setReceiverType(receiverType);
            ObjectMapper objectMapper = new ObjectMapper();
            String body = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(reward);

            // write request
            OutputStream os = connection.getOutputStream();
            byte[] input = body.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);

            // get response code
            responseCode = connection.getResponseCode();
            message = connection.getResponseMessage();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new OrganizationServiceException("Issue Reward", "Connection failed");
        }

        // confirm whether reward request was successful
        if (responseCode < 200 || responseCode > 299) {
            throw new OrganizationServiceException("Issue Reward",
                    "Reward could not be issued: " + responseCode + " " + message);
        }
    }

    public List<RewardsResponse.Reward> getRewards(String receiverID) throws OrganizationServiceException {
        try {
            // build request
            URL url = new URL(systemEnvValues.APP_BASE_URL + systemEnvValues.GET_REWARDS_PATH + "/" + receiverID);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");

            // read response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String output;
            StringBuilder response = new StringBuilder();
            while ((output = reader.readLine()) != null) {
                response.append(output);
            }
            reader.close();

            // map to IssueRewardResponse object and return its list of rewards
            ObjectMapper objectMapper = new ObjectMapper();
            RewardsResponse rewardResponse = objectMapper.readValue(response.toString(), RewardsResponse.class);
            return rewardResponse.getData();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new OrganizationServiceException("Get Rewards", "Connection failed");
        }
    }
}
