#!/bin/sh
set -e #exit immediately if a command exits with a non-zero status.

if [[ -f .envExports ]]
then
  source .envExports
else
  echo 'No .envExports found, please create one from .envExports.template'
  exit 1
fi

export DDB_ENDPOINT_URL="http://localhost:8000"

mvn clean compile exec:java -Dexec.mainClass="org.onestep.relief.dev.DevApp"
