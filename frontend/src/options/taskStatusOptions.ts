export const taskStatusOptions = [
  { label: 'NEW', value: 'NEW' },
  { label: 'IN_PROGRESS', value: 'IN_PROGRESS' },
  { label: 'COMPLETED', value: 'COMPLETED' },
]
