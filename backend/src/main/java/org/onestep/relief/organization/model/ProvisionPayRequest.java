package org.onestep.relief.organization.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProvisionPayRequest {
    private RequestSender sender;
    private String callbackURL;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RequestSender {
        private String payId;
    }
}
