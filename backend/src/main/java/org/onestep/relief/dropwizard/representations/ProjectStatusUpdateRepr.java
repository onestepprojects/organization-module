package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectStatusUpdateRepr {
    @NotNull
    private String authorID;

    @NotNull
    @Pattern(regexp = "[0-9]{13}")
    private String date;

    @NotNull
    @Size(min = 4)
    private String title;

    @NotNull
    @Size(min = 10)
    private String description;
}
