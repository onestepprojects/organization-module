import type { Organization, PersonRole } from '../types'
import { api } from './api'

export const organizationApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getOrganizations: builder.query<Organization[], void>({
      query: () => `/organizations`,
      providesTags: ['Organization'],
    }),

    getRootOrganizations: builder.query<Organization[], void>({
      query: () => `/organizations/roots`,
      providesTags: ['Organization'],
    }),

    getOrganization: builder.query<Organization, string>({
      query: (id) => `/organizations/${id}`,
      providesTags: ['Organization'],
    }),

    getSubOrganizations: builder.query<Organization[], string>({
      query: (id) => `/organizations/get-sub/${id}`,
      providesTags: ['Organization'],
    }),

    getParentOrganization: builder.query<Organization, string>({
      query: (id) => `/organizations/get-parent/${id}`,
      providesTags: ['Organization'],
    }),

    getOrganizationsByPerson: builder.query<Organization[], string>({
      query: (personId) => `/organizations/get-org-by-person/${personId}`,
      providesTags: ['Organization'],
    }),

    getOrganizationRolesByPerson: builder.query<PersonRole[], string>({
      query: (personId) => `/organizations/get-org-roles-by-person/${personId}`,
      providesTags: ['Organization'],
    }),

    createOrganization: builder.mutation<Organization, Partial<Organization>>({
      query: (body) => ({
        url: `/organizations/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Organization'],
    }),

    updateOrganization: builder.mutation<Organization, Partial<Organization>>({
      query: (body) => ({
        url: `/organizations/update`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Organization'],
    }),

    updateOrganizationImage: builder.mutation<
      Organization,
      Partial<Organization['logo']> & { id: string }
    >({
      query: ({ id, ...body }) => ({
        url: `/organizations/update-image/${id}`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Organization'],
    }),

    deleteOrganization: builder.mutation<void, string>({
      query: (id) => ({
        url: `/organizations/delete/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Organization'],
    }),

    /**
     * Add a sub organization to the current organization
     *
     * FIXME: The backend return a string response, but the frontend expects a json. So it's always
     * throwing an object.
     */
    addSubOrganization: builder.mutation<void, { id: string; subOrganizationId: string }>({
      query: ({ id, subOrganizationId }) => ({
        url: `/organizations/add-sub-organization/${id}/${subOrganizationId}`,
        method: 'GET', // FIXME: This should be a POST at backend.
      }),
      invalidatesTags: ['Organization'],
    }),
  }),
})

export const {
  useGetOrganizationsQuery,
  useGetRootOrganizationsQuery,
  useLazyGetRootOrganizationsQuery,
  useGetOrganizationQuery,
  useGetSubOrganizationsQuery,
  useLazyGetSubOrganizationsQuery,
  useGetParentOrganizationQuery,
  useGetOrganizationsByPersonQuery,
  useGetOrganizationRolesByPersonQuery,
  useCreateOrganizationMutation,
  useUpdateOrganizationMutation,
  useUpdateOrganizationImageMutation,
  useDeleteOrganizationMutation,
  useAddSubOrganizationMutation,
} = organizationApi
