package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectRepr {
    @Pattern(regexp = "PROJECT|ORGANIZATION")
    private String parentType;

    private String parentId;

    private String id;

    @NotNull
    @Size(min = 4)
    private String name;

    @NotNull
    @Size(min = 10)
    private String description;

    @Valid
    private Map<String, RepLocation> targetArea;

    @Pattern(regexp = StaticValues.projectTypes)
    private String primaryType;

    @Pattern(regexp = StaticValues.projectTypes)
    private String secondaryType;

    private String tagline;

    private FileRepr profilePicture;

    @NotNull
    @Pattern(regexp = "[0-9]{13}")
    private String creationDate;

    @NotNull
    @Pattern(regexp = "[0-9]{13}")
    private String startDate;

    @Pattern(regexp = "[0-9]{13}")
    private String closeDate;

    /** @deprecated Put all status into `statusUpdatesHistory` */
    @Valid
    private ProjectStatusUpdateRepr currentStatusUpdate;

    private List<ProjectStatusUpdateRepr> statusUpdatesHistory;

    private Map<String, String> partnerOrganizations;

    @NotNull
    @Pattern(regexp = "LOW|MEDIUM|HIGH|CRITICAL|EMERGENCY")
    private String urgencyLevel;

    @NotNull
    @Pattern(regexp = "PENDING|INITIATED|ASSESSMENT|DESIGN|IMPLEMENTATION|FOLLOWUP|COMPLETED|CLOSED")
    private String projectStage = "PENDING";

    private boolean isHealth = false;

    private List<String> requesterRoles;

    private Map<String, String> socialMedia;
}
