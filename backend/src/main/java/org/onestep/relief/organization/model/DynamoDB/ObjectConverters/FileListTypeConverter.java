package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

import java.io.IOException;

/**
 * Converts a List of the complex type File to a string and vice-versa.
 */

public class FileListTypeConverter implements DynamoDBTypeConverter<String, List<File>> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(List<File> files) {
        return DynamoDBTypeConverterHelper.getInstance().convert(files, "files", log);
    }

    @Override
    public List<File> unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<File> files;

        try {
            files = objectMapper.readValue(s, new TypeReference<List<File>>() {
            });
            log.debug("unmarshalled files to " + files);
        } catch (IOException e) {
            log.error("error de-marshalling files: " + s, e);
            return null;
        }
        return files;
    }
}
