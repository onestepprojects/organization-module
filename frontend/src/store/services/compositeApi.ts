import type { PersonRole } from '../types'
import { api } from './api'

export const compositeApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    /** Possible entity id: organization, project. */
    getCompositeRoleRequests: builder.query<PersonRole[], string>({
      query: (entityId) => `/composite/role-requests/${entityId}`,
      providesTags: ['PersonRole'],
    }),

    addCompositeRoleToEntity: builder.query<
      PersonRole,
      { personId: string; role: string; entityType: 'organization' | 'project'; entityId: string }
    >({
      query: ({ personId, role, entityType, entityId }) => ({
        url: `/composite/add-role-to-entity/${personId}/${role}/${entityType}/${entityId}`,
      }),
    }),

    updateCompositeRoleStatus: builder.query<
      PersonRole,
      { roleId: string; role: string; entityId: string; decision: 'ACTIVE' | 'INACTIVE' }
    >({
      query: ({ roleId, role, entityId, decision }) => ({
        url: `/composite/update-person-role-status/${role}/${entityId}/${roleId}/${decision}`,
      }),
    }),
  }),
})

export const {
  useGetCompositeRoleRequestsQuery,
  useLazyAddCompositeRoleToEntityQuery,
  useLazyUpdateCompositeRoleStatusQuery,
} = compositeApi
