package org.onestep.relief.dropwizard;

public class SystemEnvValues {
    // dynamoDB
    public String DDB_REGION_NAME;
    public String DDB_PREFIX;

    // s3
    public String S3_REGION_NAME;

    // base URLs
    public String APP_BASE_URL;
    public String HEALTH_BASE_URL;

    // organization and project module service credentials
    public String SERVICE_CLIENT_ID;
    public String SERVICE_CLIENT_SECRET;

    // auth service endpoints
    public String AUTH_USER_PATH;
    public String AUTH_SERVICE_LOGIN_PATH;
    public String BIND_COGNITO_ROLE_PATH;

    // blockchain service endpoints
    public String PROVISION_PAY_USER_PATH;

    // reward service endpoints
    public String ISSUE_REWARD_PATH;
    public String GET_REWARDS_PATH;

    // organization and project module endpoints
    public String PAGO_PERSON_CALLBACK_PATH;
    public String PAGO_ORG_CALLBACK_PATH;

    // S3 bucket configuration
    public String ORG_LOGO_BUCKET;
    public String PROJECT_PIC_BUCKET;
    public String PERSON_PIC_BUCKET;

    // default profile images
    public String ORG_PIC_DEFAULT;
    public String PROJECT_PIC_DEFAULT;
    public String PERSON_PIC_DEFAULT;
}
