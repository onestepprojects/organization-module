package org.onestep.relief.organization.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CognitoService {
    private String access_token;
    private int expires_in;
    private String token_type;
}
