package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.Task;

import java.util.List;

public class TaskDynamoDBService {

    DynamoDBMapper mapper;

    @Inject
    public TaskDynamoDBService(DynamoDBMapperBuilder builder) {
        this.mapper = builder.buildMapper("Task");
    }

    public void save(Object obj) {
        this.mapper.save(obj);
    }

    public Task load(String taskId) {
        return this.mapper.load(Task.class, taskId);
    }

    public void delete(String taskId) {
        Task task = load(taskId);
        if (task != null) {
            this.mapper.delete(task);
        }
    }

    public void deleteAll() {
        mapper.generateDeleteTableRequest(Task.class);
    }

    public PaginatedScanList<Task> scan() {
        return this.mapper.scan(Task.class, new DynamoDBScanExpression());
    }

    public PaginatedScanList<Task> scan(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Task.class, dynamoDBScanExpression);
    }

    public List<Task> query(DynamoDBQueryExpression<Task> queryExpression) {
        return this.mapper.query(Task.class, queryExpression);
    }
}