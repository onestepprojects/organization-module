package org.onestep.relief.dropwizard.resources;

import com.google.inject.Inject;
import org.dozer.DozerBeanMapper;

import org.onestep.relief.dropwizard.customDozer.ProjectToProjectRepr;
import org.onestep.relief.dropwizard.representations.FileRepr;
import org.onestep.relief.dropwizard.representations.ProjectRepr;
import org.onestep.relief.dropwizard.representations.ProjectStatusUpdateRepr;
import org.onestep.relief.organization.model.*;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;
import org.onestep.relief.organization.service.OrganizationServiceImpl;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/projects")
@Produces(MediaType.APPLICATION_JSON)
public class ProjectResource {
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;
    private final Validator validator;

    /**
     * Constructor initializes a Dozer mapper, a Validator, and the model interface.
     *
     * @param validator
     *                  Validator class received from environment
     */
    @Inject
    public ProjectResource(OrganizationServiceImpl orgInterface, Validator validator) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        this.dtoMapper = mapper;
        this.validator = validator;
        this.orgInterface = orgInterface;
    }

    @POST
    @Path("/create")
    public Response createProject(ProjectRepr pRequest, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        Set<ConstraintViolation<ProjectRepr>> violations = validator.validate(pRequest);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, Utils.getViolation(violations));

        if (ReliefValidators.invalidRoles(pRequest))
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Invalid Roles.");

        try {
            Project newProject = dtoMapper.map(pRequest, Project.class);

            Project createdProject = orgInterface.createProject(authToken.orElse("UNKNOWN"), pRequest.getParentId(),
                    pRequest.getParentType(), newProject);

            ProjectRepr resp = dtoMapper.map(createdProject, ProjectRepr.class);

            return Response.status(Response.Status.OK).entity(resp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/update")
    public Response updateProject(ProjectRepr pRequest, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        Set<ConstraintViolation<ProjectRepr>> violations = validator.validate(pRequest);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, Utils.getViolation(violations));

        try {
            Project newProject = dtoMapper.map(pRequest, Project.class);
            Project createdProject = orgInterface.updateProject(authToken.orElse("UNKNOWN"), newProject);

            ProjectRepr resp = dtoMapper.map(createdProject, ProjectRepr.class);

            return Response.status(Response.Status.OK).entity(resp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/create-status/{projectId}")
    public Response createProjectStatusUpdate(ProjectStatusUpdateRepr updateRepr,
            @PathParam("projectId") String projectId, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        Set<ConstraintViolation<ProjectStatusUpdateRepr>> violations = validator.validate(updateRepr);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, Utils.getViolation(violations));

        ProjectToProjectRepr mapper = new ProjectToProjectRepr();

        try {
            ProjectStatusUpdate pUpdate = mapper.toProjectStatusUpdate(updateRepr);
            ProjectStatusUpdate newProjectUpdate = orgInterface.createProjectStatusUpdate(authToken.orElse("UNKNOWN"),
                    projectId, pUpdate);

            return Response.status(Response.Status.OK).entity(mapper.toProjectStatusUpdate(newProjectUpdate)).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/remove-status/{projectId}")
    public Response removeProjectStatusUpdate(ProjectStatusUpdateRepr updateRepr,
            @PathParam("projectId") String projectId, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        ProjectToProjectRepr mapper = new ProjectToProjectRepr();

        try {
            ProjectStatusUpdate pUpdate = mapper.toProjectStatusUpdate(updateRepr);
            ProjectStatusUpdate oldProjectUpdate = orgInterface.removeProjectStatusUpdate(authToken.orElse("UNKNOWN"),
                    projectId, pUpdate);

            return Response.status(Response.Status.OK).entity(mapper.toProjectStatusUpdate(oldProjectUpdate)).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    public Response getAllProjects() {
        return Response.status(Response.Status.OK).entity(orgInterface.getAllProjects().stream()
                .map(p -> dtoMapper.map(p, ProjectRepr.class)).collect(Collectors.toList())).build();
    }

    @GET
    @Path("/{id}")
    public Response getProjectById(@PathParam("id") String id,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            Project proj = orgInterface.getProjectByID(authToken.orElse("UNKNOWN"), id);
            ProjectRepr projectRepr = dtoMapper.map(proj, ProjectRepr.class);

            return Response.status(Response.Status.OK).entity(projectRepr).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/sub-projects/{parentId}")
    public Response getProjectsByParent(@PathParam("parentId") String parentId) {
        try {
            List<Project> projects = orgInterface.getProjectsByParent(parentId);

            return Response.status(Response.Status.OK).entity(
                    projects.stream().map(p -> dtoMapper.map(p, ProjectRepr.class)).collect(Collectors.toList()))
                    .build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/projects-by-person/{personID}")
    public Response getProjectsByPerson(@PathParam("personID") String personID) {
        try {
            List<Project> projects = orgInterface.getProjectsByPerson(personID);

            return Response.status(Response.Status.OK).entity(
                    projects.stream().map(p -> dtoMapper.map(p, ProjectRepr.class)).collect(Collectors.toList()))
                    .build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, e.getMessage());
        }
    }

    @DELETE
    @Path("/delete/{projectId}")
    public Response deleteProject(@PathParam("projectId") String projectId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            orgInterface.deleteProject(authToken.orElse("UNKNOWN"), projectId);

            return Response.status(Response.Status.OK).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, e.getMessage());
        }
    }

    @POST
    @Path("update-image/{projectId}")
    public Response updateImage(FileRepr fileRepr, @PathParam("projectId") String projectId,
            @HeaderParam("Authorization") String authToken) {

        try {
            File file = dtoMapper.map(fileRepr, File.class);

            Project proj = orgInterface.updateProjectImage(authToken, projectId, file);
            ProjectRepr projectResp = dtoMapper.map(proj, ProjectRepr.class);

            return Response.status(Response.Status.OK).entity(projectResp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }
}
