package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationRepr {
    @Size(min = 4)
    private String id;

    @NotNull
    private String name;

    @Pattern(regexp = "FOR_PROFIT|NON_PROFIT|FOUNDATION|GOVERNMENTAL|EDUCATIONAL|HYBRID")
    private String type;

    private String tagline;

    private FileRepr logo;

    @Email
    private String email;

    @Size(min = 1)
    private Map<String, String> phones;

    @Size(min = 2)
    private String website;

    @Valid
    private RepAddress address;

    @Size(min = 4)
    private String creatorUUID;

    @Pattern(regexp = "INITIATING|ACTIVE|INACTIVE|PAUSED|CLOSED")
    private String statusLabel;

    @Pattern(regexp = "[0-9]{13}")
    private String statusLastActive;

    @Valid
    private RepDescription description;

    private boolean isSupplier;

    private List<String> requesterRoles;
    private Map<String, String> socialMedia;
    private PaymentAccountRepr paymentAccount;
}
