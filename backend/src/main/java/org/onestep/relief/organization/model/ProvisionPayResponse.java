package org.onestep.relief.organization.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProvisionPayResponse {
    private String blockchainAddress;
    private List<Asset> assets;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Asset {
        private String assetId;
        private String txId;
    }
}