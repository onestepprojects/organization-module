import classNames from 'classnames'
import { type FC } from 'react'
import { type SocialMedia } from '../../store/types/common'
import { SocialMediaLink } from '../SocialMediaLink/SocialMediaLink'

export interface SocialMediaLinkListProps {
  className?: string
  socialMedia?: SocialMedia
  emptyText?: string
}

export const SocialMediaLinkList: FC<SocialMediaLinkListProps> = ({
  className,
  socialMedia,
  emptyText,
}) => {
  const { twitter, facebook, linkedin, youtube } = { ...socialMedia }
  const hasLink = Object.values({ ...socialMedia }).some((link) => link)

  return (
    <div className={classNames('flex justify-center gap-4', className)}>
      <SocialMediaLink type='twitter' link={twitter} />
      <SocialMediaLink type='facebook' link={facebook} />
      <SocialMediaLink type='linkedin' link={linkedin} />
      <SocialMediaLink type='youtube' link={youtube} />

      {!hasLink && emptyText && <div>{emptyText}</div>}
    </div>
  )
}
