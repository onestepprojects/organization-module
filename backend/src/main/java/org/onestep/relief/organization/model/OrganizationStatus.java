package org.onestep.relief.organization.model;

import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Class representing most updated status of the organization and the date of its last activity.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrganizationStatus {

    private ActivityStatus status;
    private Date lastActive;

    public enum ActivityStatus {
        INITIATING, ACTIVE, INACTIVE, PAUSED, CLOSED
    }
}
