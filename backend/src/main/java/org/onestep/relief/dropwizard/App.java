package org.onestep.relief.dropwizard;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.google.inject.Provides;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

public class App {

  public static void main(String[] args) throws Exception {
    new AppWithModule(new ProductionModule()).run(args);
  }

  public static class ProductionModule extends BaseAppModule {
    @Provides
    public AmazonDynamoDB provideAmazonDynamoDB(SystemEnvValues systemEnvValues) {
      final Regions region = Regions.fromName(systemEnvValues.DDB_REGION_NAME);
      return AmazonDynamoDBClientBuilder.standard().withRegion(region).build();
    }

    @Provides
    public S3Client provideS3Client(SystemEnvValues systemEnvValues) {
      Region region = Region.of(systemEnvValues.S3_REGION_NAME);
      return S3Client.builder().region(region).build();
    }
  }

}
