import { render, screen } from '@testing-library/react'
import type { Person } from '../../store'
import { wrapper } from '../../tests'
import { PersonProfileCard } from './PersonProfileCard'

describe('PersonProfileCard', () => {
  it('renders correctly', () => {
    render(<PersonProfileCard person={{} as Person} />, { wrapper })
  })

  it('renders person profile', () => {
    render(
      <PersonProfileCard
        person={
          {
            name: 'One Step',
            profilePicture: { url: '/profile.png' },
            currentAddress: { city: 'Cambridge', state: 'Massachusetts' },
            email: 'test@onestepprojects.org',
          } as Person
        }
      />,
      { wrapper }
    )

    expect(screen.getByText('One Step')).toBeInTheDocument()
    expect(screen.getByRole('img', { name: 'Profile picture' })).toHaveProperty(
      'src',
      expect.stringMatching('/profile.png')
    )
    expect(screen.getByText('Location: Cambridge, Massachusetts')).toBeInTheDocument()
    expect(screen.getByRole('link', { name: 'Profile email' })).toHaveProperty(
      'href',
      'mailto:test@onestepprojects.org'
    )
  })

  it('renders edit/delete buttons if it is editable', () => {
    render(<PersonProfileCard person={{} as Person} editable />, { wrapper })

    expect(screen.getByRole('button', { name: 'Edit Profile' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Delete Profile' })).toBeInTheDocument()
  })
})
