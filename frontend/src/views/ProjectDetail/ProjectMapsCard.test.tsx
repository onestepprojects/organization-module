import { render, screen } from '@testing-library/react'
import type { Project } from '../../store'
import { wrapper } from '../../tests'
import { ProjectMapsCard } from './ProjectMapsCard'

describe('ProjectMapsCard', () => {
  it('renders no location if target area is undefined', () => {
    render(<ProjectMapsCard project={{} as Project} />, { wrapper })

    expect(screen.getByText('No location')).toBeInTheDocument()
  })
})
