import { ProjectOutlined, ReconciliationOutlined, TeamOutlined } from '@ant-design/icons'
import { Space } from 'antd'
import { type Project } from '../../store'
import { buildPersonNode } from './buildPersonNode'
import { buildTaskNode } from './buildTaskNode'
import { type ProjectNode, type TreeNode } from './typings'

export function buildProjectNode(project: Project): ProjectNode {
  const node: ProjectNode = {
    type: 'ProjectNode',
    project,
    key: project.id,
    title: (
      <Space>
        <ProjectOutlined className='text-blue-500' />
        <span>{project.name}</span>
      </Space>
    ),
    calcChildren(): TreeNode[] | undefined {
      if (node.subProjects === undefined) {
        return undefined
      }
      return [
        ...node.subProjects.map((it) => buildProjectNode(it)),
        ...[
          node.persons.length && {
            type: 'PersonsContainerNode',
            title: (
              <Space>
                <TeamOutlined className='text-yellow-500' />
                <span>Persons</span>
              </Space>
            ),
            key: `${project.id}-persons`,
            children: node.persons.map((it) => buildPersonNode(it)),
          },
        ].filter(Boolean),
        ...[
          node.tasks.length && {
            type: 'TaskContainerNode',
            title: (
              <Space>
                <ReconciliationOutlined className='text-green-500' />
                Tasks
              </Space>
            ),
            key: `${project.id}-tasks`,
            children: node.tasks.map((it) => buildTaskNode(it)),
          },
        ].filter(Boolean),
      ] as TreeNode[]
    },
  }
  return node
}
