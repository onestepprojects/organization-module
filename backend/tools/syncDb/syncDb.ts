import {exec} from '../helpers/exec';

export function syncDb(profile: string) {
  const region = exec(`aws configure get region --profile=${profile}`, true);

  const result = exec(`dynamodump list-tables --profile=${profile} --region=${region}`, true);
  const remoteValidTables = result.split(/\s+/).filter(it => it.match(/^[A-Z]/));
  console.log("remoteValidTables: ", remoteValidTables.join(', '));

  const localExistingTables = exec(`dynamodump list-tables --endpoint http://localhost:8000 --region=${region}`, true).split(/\s+/);
  console.log('localExistingTables', localExistingTables.join(', '));

  remoteValidTables.filter(it => localExistingTables.includes(it)).forEach(table => {
    console.log('Delete local existing table:', table);
    try {
      // FIXME The command output sometimes seems frozen (like in vim mode),
      //  so I have to add `| cat` to output the data and avoid freezing
      exec(`aws dynamodb delete-table --endpoint=http://localhost:8000 --table-name=${table} | cat`);
    } catch (error) {
      if (error instanceof Error && error.message.includes('Cannot do operations on a non-existent table')) {
        // ignore
      }
    }
  })

  remoteValidTables.forEach(table => {
    console.log('-----------------------------------');
    console.log('Sync table', table);

    exec(`dynamodump export-schema --profile ${profile} --region=${region} --table=${table} --file=${table}.schema.json`);
    exec(`dynamodump import-schema --region=${region} --endpoint http://localhost:8000 --file=${table}.schema.json --table=${table} --wait-for-active`);
    exec(`rm -rf ${table}.schema.json`);

    exec(`dynamodump export-data --profile ${profile} --region=${region} --table=${table} --file=${table}.data.json`)
    exec(`dynamodump import-data --region=${region} --endpoint http://localhost:8000 --file=${table}.data.json --table=${table}`);
    exec(`rm -rf ${table}.data.json`);
  })

  console.log('Sync done.')

}
