package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.Community;

import java.util.List;

public class CommunityDynamoDBService {

    DynamoDBMapper mapper;

    @Inject
    public CommunityDynamoDBService(DynamoDBMapperBuilder builder) {
        this.mapper = builder.buildMapper("Community");
    }

    public void save(Object obj) {
        this.mapper.save(obj);
    }

    public Community load(String communityId) {
        return this.mapper.load(Community.class, communityId);
    }

    public void delete(String communityId) {
        Community community = load(communityId);
        if (community != null) {
            this.mapper.delete(community);
        }
    }

    public void deleteAll() {
        mapper.generateDeleteTableRequest(Community.class);
    }

    public PaginatedScanList<Community> scan() {
        return this.mapper.scan(Community.class, new DynamoDBScanExpression());
    }

    public PaginatedScanList<Community> scan(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Community.class, dynamoDBScanExpression);
    }

    public List<Community> query(DynamoDBQueryExpression<Community> queryExpression) {
        return this.mapper.query(Community.class, queryExpression);
    }
}
