package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type Address to a string and vice-versa.
 */

public class AddressTypeConverter implements DynamoDBTypeConverter<String, Address> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(Address address) {
        return DynamoDBTypeConverterHelper.getInstance().convert(address, "address", log);
    }

    @Override
    public Address unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        Address address;

        try {
            address = objectMapper.readValue(s, Address.class);
            log.debug("unmarshalled address to " + address);
        } catch (IOException e) {
            log.error("error de-marshalling address: " + s, e);
            return null;
        }

        return address;
    }
}
