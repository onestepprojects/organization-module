import { render, screen } from '@testing-library/react'
import type { Project, Task } from '../../store'
import { wrapper } from '../../tests'
import { ProjectTasksCard } from './ProjectTasksCard'

describe('ProjectTasksCard', () => {
  it('renders no tasks if tasks are empty', () => {
    render(<ProjectTasksCard project={{} as Project} tasks={[]} />, { wrapper })

    expect(screen.getByText('No tasks')).toBeInTheDocument()
  })

  it('renders tasks table', () => {
    render(
      <ProjectTasksCard
        project={{} as Project}
        tasks={[{ id: '1', description: 'task description', status: 'task status' }] as Task[]}
      />,
      {
        wrapper,
      }
    )

    expect(screen.getByText('task description')).toBeInTheDocument()
    expect(screen.getByText('task status')).toBeInTheDocument()
  })

  it('renders a create button if admin', () => {
    render(<ProjectTasksCard project={{} as Project} tasks={[]} isAdmin />, { wrapper })

    expect(screen.getByRole('button', { name: 'Create' })).toBeInTheDocument()
  })
})
