import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import { type FC } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { PageTitle, ProjectForm } from '../../components'
import {
  skipToken,
  useAppSelector,
  useGetProjectQuery,
  useUpdateProjectMutation,
  type Project,
} from '../../store'

export const ProjectUpdate: FC = () => {
  const navigate = useNavigate()
  const person = useAppSelector((state) => state.auth.person)
  const { projectId } = useParams<{ projectId: string }>()

  const { isLoading: isLoadingProject, data: project } = useGetProjectQuery(projectId || skipToken)
  const [updateProject] = useUpdateProjectMutation()

  const handleSubmit = useMemoizedFn(async (values: Project) => {
    const newProject: Project = {
      ...project,
      ...values,
    }

    console.log('Update project', newProject)

    try {
      await updateProject(newProject).unwrap()
      message.success('Update project success')
      navigate(`/projects/${projectId}`)
    } catch (err) {
      console.log('Update project failed', err)
      message.error('Update project failed')
      throw err
    }
  })

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Update Project</PageTitle>

      <Card className='max-w-screen-lg rounded-lg' loading={isLoadingProject}>
        {!!project && (
          <ProjectForm
            initialValues={project}
            onFinish={handleSubmit}
            onFinishFailed={console.log}
            // onValuesChange={console.log}
          />
        )}
      </Card>
    </div>
  )
}
