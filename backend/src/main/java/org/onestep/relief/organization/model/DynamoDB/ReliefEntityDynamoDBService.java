package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.PersonRole;
import org.onestep.relief.organization.model.ReliefComposite;
import org.onestep.relief.organization.model.ReliefEntity;

import java.util.List;

public class ReliefEntityDynamoDBService {

    DynamoDBMapper mapper;

    @Inject
    public ReliefEntityDynamoDBService(DynamoDBMapperBuilder builder) {
        this.mapper = builder.buildMapper("ReliefEntity");
    }

    public void save(Object obj) {
        this.mapper.save(obj);
    }

    public ReliefEntity load(String reliefEntityId) {
        return this.mapper.load(ReliefEntity.class, reliefEntityId);
    }

    public void delete(String reliefEntityId) {
        ReliefEntity reliefEntity = load(reliefEntityId);
        if (reliefEntity != null) {
            this.mapper.delete(reliefEntity);
        }
    }

    public PaginatedScanList<ReliefEntity> scan() {
        return this.mapper.scan(ReliefEntity.class, new DynamoDBScanExpression());
    }

    public PaginatedScanList<ReliefEntity> scan(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(ReliefEntity.class, dynamoDBScanExpression);
    }

    public List<ReliefComposite> query(DynamoDBQueryExpression<ReliefComposite> queryExpression) {
        return this.mapper.query(ReliefComposite.class, queryExpression);
    }

    public List<PersonRole> queryPersonRole(DynamoDBQueryExpression<PersonRole> queryExpression) {
        return this.mapper.query(PersonRole.class, queryExpression);
    }
}