import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import type { UploadFile } from 'antd/lib/upload/interface'
import { useState, type FC } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { OrganizationForm, PageTitle } from '../../components'
import {
  skipToken,
  useGetOrganizationQuery,
  useLazyGetGeographicPositionByAddressQuery,
  useUpdateOrganizationMutation,
  type Organization,
} from '../../store'

export const OrganizationUpdate: FC = () => {
  const navigate = useNavigate()
  const { organizationId } = useParams<{ organizationId: string }>()

  const [step, setStep] = useState(1)

  const { isLoading: isLoadingOrganization, data: organization } = useGetOrganizationQuery(
    organizationId || skipToken
  )
  const [getGeographicPositionByAddress] = useLazyGetGeographicPositionByAddressQuery()
  const [updateOrganization] = useUpdateOrganizationMutation()

  const handleSubmit = useMemoizedFn(async (values: Organization) => {
    try {
      const org = { ...values }

      const uploadLogo = org.logo as unknown as UploadFile
      if (uploadLogo?.thumbUrl) {
        const type = uploadLogo.type.split('/').pop().toUpperCase()
        const format = type === 'JPEG' ? 'JPG' : type
        org.logo = {
          name: uploadLogo.name,
          size: uploadLogo.size,
          format,
          body: uploadLogo.thumbUrl,
          caption: '',
        } as Organization['logo']
      }

      org.statusLabel ??= 'ACTIVE'
      org.description ??= {} as Organization['description']
      org.description.projectTypes ??= ['appear', 'religious']

      const mapData = await getGeographicPositionByAddress(
        [
          org.address.address,
          org.address.city,
          org.address.state,
          org.address.postalCode,
          org.address.country,
        ]
          .filter(Boolean)
          .join(',')
      ).unwrap()
      org.description.serviceArea = mapData ? [mapData] : []

      const newOrganization = Object.assign({}, organization, org)

      console.log('OrganizationUpdate submit value', newOrganization)

      const res = await updateOrganization(newOrganization).unwrap()

      console.log('OrganizationUpdate organization updated', res)

      navigate(`/organizations/${organizationId}`)

      message.success('Organization updated')
    } catch (err) {
      console.log('OrganizationUpdate submit error', err)

      message.error('Organization update failed')
    }
  })

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Organization Update</PageTitle>

      <Card className='max-w-screen-lg rounded-xl' loading={isLoadingOrganization}>
        {!!organization && (
          <OrganizationForm
            initialValues={organization}
            step={step}
            onStepChange={setStep}
            onFinish={handleSubmit}
          />
        )}
      </Card>
    </div>
  )
}
