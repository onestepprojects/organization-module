import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import { useMemo, type FC } from 'react'
import { useNavigate } from 'react-router-dom'
import { PageTitle, TaskForm } from '../../components'
import {
  skipToken,
  useCreateTaskMutation,
  useGetPersonRolesByParentQuery,
  type Task,
} from '../../store'

export const TaskCreate: FC = () => {
  const navigate = useNavigate()

  const projectId = useMemo(() => new URLSearchParams(window.location.search).get('projectId'), [])

  const { isLoading: isLoadingPersonRoles, data: personRoles } = useGetPersonRolesByParentQuery(
    projectId || skipToken
  )

  const [createTask] = useCreateTaskMutation()

  const handleSubmit = useMemoizedFn(async (values: Task) => {
    const task = { ...values, creationDate: Date.now().toString(), projectId }
    console.log('Create task', task)

    try {
      await createTask(task)
      message.success('Create task success')
      navigate(`/projects/${projectId}`)
    } catch (err) {
      console.log('Create task failed', err)
      message.error('Create task failed')
    }
  })

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Create Task</PageTitle>

      <Card className='max-w-screen-lg rounded-lg' loading={isLoadingPersonRoles}>
        <TaskForm
          personOptions={personRoles?.map(({ id, name }) => ({ label: name, value: id }))}
          onFinish={handleSubmit}
          // onValuesChange={console.log}
        />
      </Card>
    </div>
  )
}
