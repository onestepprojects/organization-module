package org.onestep.relief.organization.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.onestep.relief.dropwizard.SystemEnvValues;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.model.BindCognitoRoleRequest;
import org.onestep.relief.organization.model.CognitoUser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class CognitoService {
    private SystemEnvValues systemEnvValues;
    private ServiceTokenService serviceTokenService;

    @Inject
    public CognitoService(SystemEnvValues systemEnvValues, ServiceTokenService serviceTokenService) {
        this.systemEnvValues = systemEnvValues;
        this.serviceTokenService = serviceTokenService;
    }

    public CognitoUser fetchUser(String authToken) throws AuthenticationException {
        try {
            String authUrl = systemEnvValues.APP_BASE_URL + systemEnvValues.AUTH_USER_PATH;

            // form request to retrieve Cognito User details with auth token
            URL url = new URL(authUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", authToken);

            // read response
            BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String output;
            StringBuilder response = new StringBuilder();
            while ((output = input.readLine()) != null) {
                response.append(output);
            }
            input.close();

            // map to CognitoUser object and return
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(response.toString(), CognitoUser.class);
        } catch (Exception e) {
            throw new AuthenticationException("Get Cognito user", "Authentication failed");
        }
    }

    public void bindCognitoRole(String cognitoUsername, String role) throws OrganizationServiceException {
        // build and submit issue reward request
        int responseCode;
        String message;
        try {
            // build request
            URL url = new URL(systemEnvValues.APP_BASE_URL + systemEnvValues.BIND_COGNITO_ROLE_PATH);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", serviceTokenService.getServiceToken());
            connection.setDoOutput(true);

            // build request body
            BindCognitoRoleRequest roleRequest = new BindCognitoRoleRequest();
            roleRequest.setUserName(cognitoUsername);
            roleRequest.setRoleName(role);
            ObjectMapper objectMapper = new ObjectMapper();
            String body = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(roleRequest);

            // write request
            OutputStream os = connection.getOutputStream();
            byte[] input = body.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);

            // get response code
            responseCode = connection.getResponseCode();
            message = connection.getResponseMessage();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new OrganizationServiceException("Issue Reward", "Connection failed");
        }

        // confirm whether reward request was successful
        if (responseCode < 200 || responseCode > 299) {
            throw new OrganizationServiceException("Bind Cognito Role",
                    "Role could not be bound: " + responseCode + " " + message);
        }
    }
}
