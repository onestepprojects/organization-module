import { useMemoizedFn } from 'ahooks'
import { Button, Checkbox, Form, Input, Select, Steps, Upload, type FormProps } from 'antd'
import { useState, type FC } from 'react'
import { countryOptions } from '../../options/countryOptions'
import { organizationTypeOptions } from '../../options/organizationTypeOptions'
import { stateCodeOptions } from '../../options/stateCodeOptions'
import { type Organization } from '../../store'
import { type Paths } from '../../utils/TypedPath'

export interface OrganizationFormProps extends FormProps {
  /** 1 ~ 4 */
  step?: number

  onStepChange?: (step: number) => void
}

function typedPath(paths: Paths<Organization>): Paths<Organization> {
  return paths
}

export const OrganizationForm: FC<OrganizationFormProps> = (props) => {
  const { className, step = 1, onStepChange, onFinish, ...formProps } = props

  const [isSubmitting, setIsSubmitting] = useState(false)

  const [form] = Form.useForm<Partial<Organization>>()

  const handleValidate = useMemoizedFn(async () => {
    try {
      return await form.validateFields()
    } catch (err) {
      console.log('OrganizationForm validation error', err)
    }
  })

  const handleStepChange = useMemoizedFn(async (newStep: number) => {
    if (newStep >= 1 && newStep <= 4) {
      const values = await handleValidate()
      if (values) {
        onStepChange?.(newStep)
      }
    }
  })

  const handleSubmit = useMemoizedFn(async (values) => {
    try {
      setIsSubmitting(true)
      await onFinish(values)
    } catch {
    } finally {
      setIsSubmitting(false)
    }
  })

  return (
    <div className={className}>
      <Steps
        className='mb-4'
        type='navigation'
        current={step - 1}
        onChange={(current) => handleStepChange(current + 1)}
      >
        <Steps.Step title='Contact Information' />
        <Steps.Step title='Organization Address' />
        <Steps.Step title='Type & Description' />
        <Steps.Step title='Social Media' />
      </Steps>

      <Form
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16, offset: 6, pull: 5 }}
        colon={false}
        form={form}
        onFinish={handleSubmit}
        {...formProps}
      >
        <div style={{ display: step === 1 ? 'block' : 'none' }}>
          <Form.Item
            label='Organization Name'
            name={typedPath(['name'])}
            rules={step === 1 && [{ required: true, message: 'Name is required' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='Upload Logo Image'
            name={typedPath(['logo'])}
            valuePropName='fileList'
            getValueProps={(e) => ({ fileList: e ? [e] : [] })}
            getValueFromEvent={(e) => (Array.isArray(e) ? e[0] : e?.fileList[0])}
            extra='Please select an image.'
          >
            <Upload
              accept='image/jpeg, image/png'
              listType='picture'
              maxCount={1}
              beforeUpload={() => false}
            >
              <Button icon={<i className='mr-2 fas fa-file-upload' />}>Click to upload</Button>
            </Upload>
          </Form.Item>
          <Form.Item
            label='Organization Email'
            name={typedPath(['email'])}
            rules={
              step === 1 && [
                { required: true, message: 'Email is required' },
                { type: 'email', message: 'Email is invalid' },
              ]
            }
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='Phone Number'
            name={typedPath(['phones', 'phone'])}
            rules={step === 1 && [{ required: true, message: 'Phone is required' }]}
          >
            <Input />
          </Form.Item>
        </div>

        <div style={{ display: step === 2 ? 'block' : 'none' }}>
          <Form.Item
            label='Address'
            name={typedPath(['address', 'address'])}
            rules={
              step === 2 && [
                { required: true, message: 'Address is required' },
                { pattern: /.+(\s+.+)+/, message: 'Address is invalid' },
              ]
            }
          >
            <Input />
          </Form.Item>
          <Form.Item
            label='City'
            name={typedPath(['address', 'city'])}
            rules={
              step === 2 && [
                { required: true, message: 'City is required' },
                { min: 3, message: 'City is too short' },
              ]
            }
          >
            <Input />
          </Form.Item>
          <Form.Item label='District' name={typedPath(['address', 'district'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Select a State' name={typedPath(['address', 'state'])}>
            <Select allowClear showSearch options={stateCodeOptions} />
          </Form.Item>
          <Form.Item
            label='Zip Code'
            name={typedPath(['address', 'postalCode'])}
            rules={
              step === 2 && [
                { required: true, message: 'Zip is required' },
                { min: 2, message: 'Zip is too short' },
              ]
            }
          >
            <Input />
          </Form.Item>
          <Form.Item label='Select a Country' name={typedPath(['address', 'country'])}>
            <Select allowClear showSearch options={countryOptions} />
          </Form.Item>
        </div>

        <div style={{ display: step === 3 ? 'block' : 'none' }}>
          <Form.Item label='Select Org Type' name={typedPath(['type'])}>
            <Select allowClear options={organizationTypeOptions} />
          </Form.Item>
          <Form.Item label='Supplier?' name={typedPath(['supplier'])} valuePropName='checked'>
            <Checkbox>Check if you are a supplier</Checkbox>
          </Form.Item>
          <Form.Item label='Org tagline here' name={typedPath(['tagline'])}>
            <Input />
          </Form.Item>
          <Form.Item
            label='Organization description'
            name={typedPath(['description', 'description'])}
          >
            <Input.TextArea rows={3} />
          </Form.Item>
        </div>

        <div style={{ display: step === 4 ? 'block' : 'none' }}>
          <Form.Item label='Twitter' name={typedPath(['socialMedia', 'twitter'])}>
            <Input />
          </Form.Item>
          <Form.Item label='LinkedIn' name={typedPath(['socialMedia', 'linkedin'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Facebook' name={typedPath(['socialMedia', 'facebook'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Youtube' name={typedPath(['socialMedia', 'youtube'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Website' name={typedPath(['website'])}>
            <Input />
          </Form.Item>
        </div>

        <div className='flex justify-center gap-8'>
          <Button
            type='primary'
            ghost
            disabled={step <= 1}
            onClick={() => handleStepChange(step - 1)}
          >
            <i className='mr-2 fas fa-angle-left' /> Prev
          </Button>
          <Button
            type='primary'
            ghost
            disabled={step >= 4}
            onClick={() => handleStepChange(step + 1)}
          >
            Next <i className='ml-2 fas fa-angle-right' />
          </Button>
          <Button
            style={{ visibility: step === 4 ? 'visible' : 'hidden' }}
            type='primary'
            htmlType='submit'
            loading={isSubmitting}
          >
            Submit
          </Button>
        </div>
      </Form>
    </div>
  )
}
