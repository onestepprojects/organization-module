package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentAccountRepr {
    private String payID;
    private String blockchainAddress;

    @Pattern(regexp = "AWAITING_USER_ACTION|PARTIALLY_PROVISIONED|PROVISIONED|FAILED")
    private String status;

    private Map<String, Boolean> assets;

}
