package org.onestep.relief.dropwizard.customDozer;

import org.onestep.relief.dropwizard.representations.FileRepr;
import org.onestep.relief.dropwizard.representations.PaymentAccountRepr;
import org.onestep.relief.dropwizard.representations.RepAddress;
import org.onestep.relief.dropwizard.representations.RepLocation;
import org.onestep.relief.organization.model.Address;
import org.onestep.relief.organization.model.File;
import org.onestep.relief.organization.model.Location;
import org.onestep.relief.organization.model.PaymentAccount;

import java.sql.Timestamp;
import java.util.Date;

/**
 * CLass stores static methods for all types of DTO conversions.
 */
public class Utils {
    public static Address toAddress(RepAddress repAddress) {
        if (repAddress == null)
            return null;

        Address address = new Address();
        address.setText(repAddress.getAddress());
        address.setCity(repAddress.getCity());
        address.setDistrict(repAddress.getDistrict());
        address.setState(repAddress.getState());
        address.setPostalCode(repAddress.getPostalCode());
        address.setCountry(repAddress.getCountry());

        return address;
    }

    public static RepAddress toAddress(Address address) {
        if (address == null)
            return null;

        RepAddress rAddress = new RepAddress();
        rAddress.setAddress(address.getText());
        rAddress.setCity(address.getCity());
        rAddress.setDistrict(address.getDistrict());
        rAddress.setState(address.getState());
        rAddress.setPostalCode(address.getPostalCode());
        rAddress.setCountry(address.getCountry());

        return rAddress;
    }

    public static RepLocation toLocation(Location loc) {
        if (loc == null)
            return null;

        RepLocation rLoc = new RepLocation();
        rLoc.setLatitude(loc.getLat());
        rLoc.setLongitude(loc.getLong());

        return rLoc;
    }

    public static Location toLocation(RepLocation rLoc) {
        if (rLoc == null)
            return null;

        Location loc = new Location();
        loc.setLat(rLoc.getLatitude());
        loc.setLong(rLoc.getLongitude());

        return loc;
    }

    public static Date timestampToDate(String timestamp) {
        if (timestamp == null)
            return null;

        return new Date(Long.parseLong(timestamp));
    }

    public static String dateToTimestamp(Date date) {
        if (date == null)
            return null;

        return String.valueOf(new Timestamp(date.getTime()).getTime());
    }

    public static File toFromFileObj(FileRepr repr) {
        return (repr != null) ? new File(repr.getName(), repr.getBucketName(), repr.getUrl(), repr.getCaption(),
                (repr.getFormat() != null) ? File.FileFormat.valueOf(repr.getFormat()) : null, repr.getSize(),
                timestampToDate(repr.getCreationDate()), repr.getCreator(), repr.getBody()) : null;
    }

    public static FileRepr toFromFileObj(File file) {
        return (file != null) ? new FileRepr(file.getName(), file.getBucketName(), file.getUrl(), file.getCaption(),
                (file.getFormat() != null) ? file.getFormat().toString() : null, file.getSize(),
                dateToTimestamp(file.getCreationDate()), file.getCreator(), file.getBody()) : null;
    }

    public static PaymentAccountRepr toFromPaymentAccount(PaymentAccount acc) {
        if (acc == null)
            return null;

        return new PaymentAccountRepr(acc.getPayID(), acc.getBlockchainAddress(),
                (acc.getStatus() != null) ? acc.getStatus().toString() : null, acc.getAssets());
    }

    public static PaymentAccount toFromPaymentAccount(PaymentAccountRepr accRepr) {
        if (accRepr == null)
            return null;

        return new PaymentAccount(accRepr.getPayID(), accRepr.getBlockchainAddress(),
                (accRepr.getStatus() != null) ? PaymentAccount.ProvisionStatus.valueOf(accRepr.getStatus()) : null,
                accRepr.getAssets());
    }

}
