export const genderOptions = [
  { label: 'MALE', value: 'MALE' },
  { label: 'FEMALE', value: 'FEMALE' },
  { label: 'OTHER', value: 'OTHER' },
]
