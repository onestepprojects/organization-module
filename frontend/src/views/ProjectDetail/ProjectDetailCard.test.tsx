import { render, screen } from '@testing-library/react'
import type { Project } from '../../store'
import { wrapper } from '../../tests'
import { ProjectDetailCard } from './ProjectDetailCard'

describe('ProjectDetailCard', () => {
  const project = { name: 'project', description: 'description' } as Project

  it('renders correctly', () => {
    render(<ProjectDetailCard project={project} compositeRoleRequests={[]} />, { wrapper })

    expect(screen.getByText('project')).toBeInTheDocument()
    expect(screen.getByText('description')).toBeInTheDocument()
    expect(screen.getByRole('img', { name: 'project picture' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'View All Projects' })).toBeInTheDocument()
  })

  it('renders health tag', () => {
    render(
      <ProjectDetailCard project={{ ...project, health: true }} compositeRoleRequests={[]} />,
      { wrapper }
    )

    expect(screen.getByText('Health Project')).toBeInTheDocument()
  })

  it('renders join button', () => {
    render(<ProjectDetailCard canJoin project={project} compositeRoleRequests={[]} />, { wrapper })

    expect(screen.getByRole('button', { name: 'Join' })).toBeInTheDocument()
  })

  it('renders edit and delete button if admin', () => {
    render(<ProjectDetailCard isAdmin project={project} compositeRoleRequests={[]} />, { wrapper })

    expect(screen.getByRole('button', { name: 'Edit' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Delete' })).toBeInTheDocument()
  })
})
