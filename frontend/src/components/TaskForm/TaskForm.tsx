import { useMemoizedFn, useUnmountedRef } from 'ahooks'
import { Button, DatePicker, Form, Input, Select, type FormProps } from 'antd'
import dayjs from 'dayjs'
import { useState, type FC } from 'react'
import { taskStatusOptions, taskVisibilityOptions } from '../../options'
import { type Task } from '../../store'
import { type Paths } from '../../utils'

interface TaskFormProps extends FormProps<Task> {
  personOptions?: { label: string; value: string }[]
  initialValues?: Task
}

function typedPath(paths: Paths<Task>): Paths<Task> {
  return paths
}

export const TaskForm: FC<TaskFormProps> = (props) => {
  const { personOptions, initialValues, onFinish, ...formProps } = props

  const [isSubmitting, setIsSubmitting] = useState(false)

  const unmountRef = useUnmountedRef()

  const handleSubmit = useMemoizedFn(async (values: Task) => {
    try {
      setIsSubmitting(true)
      await onFinish(values)
    } catch (err) {
      console.log('Submit task error', err)
    } finally {
      if (!unmountRef.current) {
        setIsSubmitting(false)
      }
    }
  })

  return (
    <Form<Task>
      labelCol={{ span: 5 }}
      initialValues={initialValues}
      onFinish={handleSubmit}
      {...formProps}
    >
      <Form.Item
        label='Task name'
        name={typedPath(['name'])}
        rules={[{ required: true, message: 'Name is required' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label='Task description'
        name={typedPath(['description'])}
        rules={[{ required: true, message: 'Description is required' }]}
      >
        <Input.TextArea rows={3} />
      </Form.Item>
      <Form.Item
        label='Start date'
        name='dueDate'
        rules={[{ required: true, message: 'Start date is required' }]}
        getValueProps={(value) => value && { value: dayjs(Number(value)) }}
        normalize={(value) => value?.valueOf()}
      >
        <DatePicker format='YYYY-MM-DD' />
      </Form.Item>
      {!!personOptions && (
        <Form.Item
          label='Assign task'
          name={typedPath(['assignedPerson'])}
          rules={[{ required: true, message: 'Assignee is required' }]}
        >
          <Select options={personOptions} allowClear />
        </Form.Item>
      )}
      <Form.Item
        label='Task status'
        name={typedPath(['status'])}
        rules={[{ required: true, message: 'Status is required' }]}
      >
        <Select options={taskStatusOptions} allowClear />
      </Form.Item>
      <Form.Item
        label='Task visibility'
        name={typedPath(['visibility'])}
        rules={[{ required: true, message: 'Visibility is required' }]}
      >
        <Select options={taskVisibilityOptions} allowClear />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 5 }}>
        <Button type='primary' htmlType='submit' loading={isSubmitting}>
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}
