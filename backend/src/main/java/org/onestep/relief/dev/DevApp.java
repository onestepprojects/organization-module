package org.onestep.relief.dev;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.google.inject.Provides;
import org.onestep.relief.dropwizard.AppWithModule;
import org.onestep.relief.dropwizard.BaseAppModule;
import org.onestep.relief.dropwizard.SystemEnvValues;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;

public class DevApp {
    public static void main(String[] args) throws Exception {
        System.out.println("Dev mode");
        String endpointUrl = System.getenv("DDB_ENDPOINT_URL");
        System.out.println("Use local dynamodb: " + endpointUrl);
        new AppWithModule(new DevModule(endpointUrl)).run("server");
    }

    private static class DevModule extends BaseAppModule {
        private static final String DUMMY_REGION_FOR_LOCAL = "us-east-1";
        private final String endpointUrl;

        public DevModule(String endpointUrl) {
            this.endpointUrl = endpointUrl;
        }

        @Provides
        public AmazonDynamoDB provideAmazonDynamoDB() {
            AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(
                    endpointUrl, DUMMY_REGION_FOR_LOCAL);
            return AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(endpointConfiguration).build();
        }

        @Provides
        public S3Client provideS3Client(SystemEnvValues systemEnvValues) {
            Region region = Region.of(systemEnvValues.S3_REGION_NAME);
            return S3Client.builder().region(region).build();
        }
    }

}
