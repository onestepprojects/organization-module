import { message } from 'antd'
import { type FC } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import './App.css'
import { OrganizationContainer, type OrganizationContainerProps } from './Container'
import { store } from './store'

message.config({ top: 60 })

interface OrganizationModuleProps extends OrganizationContainerProps {
  /** Module router base name. */
  mountPath: string
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  authHelper?: any
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  person?: any
}

const OrganizationModule: FC<OrganizationModuleProps> = ({ mountPath, ...restProps }) => {
  return (
    <BrowserRouter basename={mountPath}>
      <Provider store={store}>
        <OrganizationContainer {...restProps} />
      </Provider>
    </BrowserRouter>
  )
}

export default OrganizationModule
