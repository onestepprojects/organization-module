import { Button, Card, Image, Popconfirm, Tag, message } from 'antd'
import { type FC } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { ProjectDescriptions } from '../../components'
import type { CompositeRoleRequest, Project } from '../../store'

export interface ProjectDetailCardProps {
  loading?: boolean
  project: Project
  compositeRoleRequests: CompositeRoleRequest[]
  canJoin?: boolean
  isAdmin?: boolean
  deleteProject?: (projectId: string) => Promise<void>
}

export const ProjectDetailCard: FC<ProjectDetailCardProps> = ({
  loading,
  project,
  compositeRoleRequests,
  canJoin,
  isAdmin,
  deleteProject,
}) => {
  const navigate = useNavigate()

  const actions = [
    canJoin && (
      <Link to={`/projects/${project?.id}/join`}>
        <Button type='primary'>Join</Button>
      </Link>
    ),

    isAdmin && (
      <Link to={`/projects/${project?.id}/update`}>
        <Button type='primary' icon={<i className='mr-2 fas fa-edit' />}>
          Edit
        </Button>
      </Link>
    ),

    <Link to='/projects'>
      <Button type='primary'>View All Projects</Button>
    </Link>,

    isAdmin && (
      <Popconfirm
        title='Are you sure you wish to delete this project?'
        okText='Delete'
        okButtonProps={{ danger: true }}
        onConfirm={async () => {
          try {
            await deleteProject(project?.id)
            message.success('Project deleted')
            navigate('/', { replace: true })
          } catch (err) {
            console.warn('Project delete failed', project?.id, err)
            message.error('Project delete failed')
          }
        }}
      >
        <Button type='primary' danger icon={<i className='mr-2 fas fa-trash' />}>
          Delete
        </Button>
      </Popconfirm>
    ),
  ].filter(Boolean)

  return (
    <Card
      className='rounded-lg'
      loading={loading}
      cover={
        <Image
          className='!rounded-t-lg object-cover'
          src={project?.profilePicture?.url}
          fallback='https://onestep-project-pics.s3.amazonaws.com/defaultproject.svg'
          alt={`${project?.name} picture`}
          preview={false}
        />
      }
      actions={actions}
    >
      <Card.Meta
        title={project?.name}
        description={
          <div className='flex flex-col gap-4'>
            {!!project?.health && (
              <span>
                <Tag color='green'>Health Project</Tag>
              </span>
            )}
            <div>{project?.description}</div>
            <ProjectDescriptions
              title='Project Details'
              project={project}
              compositeRoleRequests={isAdmin && compositeRoleRequests}
            />
          </div>
        }
      />
    </Card>
  )
}
