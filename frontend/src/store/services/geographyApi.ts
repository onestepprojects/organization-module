import { api } from './api'

/**
 * Nominatim indexes named (or numbered) features within the OpenStreetMap (OSM) dataset and a
 * subset of other unnamed features (pubs, hotels, churches, etc).
 *
 * @see https://nominatim.org/release-docs/develop/api/Overview/
 */
export const geographyApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getGeographicPositionByAddress: builder.query<
      { latitude: number; longitude: number } | undefined,
      string
    >({
      query: (address) => ({
        url: `https://nominatim.openstreetmap.org/search`,
        params: {
          q: address,
          format: 'json',
          limit: 1,
        },
      }),
      transformResponse: (response) => {
        const loc = response[0]
        if (!loc) return
        return { latitude: parseFloat(loc.lat), longitude: parseFloat(loc.lon) }
      },
    }),
  }),
})

export const { useLazyGetGeographicPositionByAddressQuery } = geographyApi
