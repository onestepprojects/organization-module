import { Space } from 'antd'
import { type FC } from 'react'
import {
  skipToken,
  useAppSelector,
  useGetOrganizationRolesByPersonQuery,
  useGetOrganizationsQuery,
  useGetPersonsQuery,
  useGetProjectsByPersonQuery,
  useGetProjectsQuery,
  useGetTasksByPersonQuery,
} from '../../store'
import { DashboardCard } from './DashboardCard'

export const Dashboard: FC = () => {
  const personId = useAppSelector((state) => state.auth.person?.uuid)

  const { isLoading: myProjectsLoading, data: myProjects } = useGetProjectsByPersonQuery(
    personId || skipToken
  )
  const { isLoading: myTasksLoading, data: myTasks } = useGetTasksByPersonQuery(
    personId || skipToken
  )
  const { isLoading: organizationsLoading, data: organizations } = useGetOrganizationsQuery()
  const { isLoading: myOrganizationRolesLoading, data: myOrganizationRoles } =
    useGetOrganizationRolesByPersonQuery(personId || skipToken)
  const { isLoading: projectsLoading, data: projects } = useGetProjectsQuery()
  const { isLoading: personsLoading, data: persons } = useGetPersonsQuery()

  return (
    <div className='mx-8 flex flex-col'>
      <div className='my-8 text-4xl font-normal text-neutral-600'>
        One Step <span className='font-extralight'>Dashboard</span>
      </div>
      <Space className='mb-8' wrap size='large'>
        <DashboardCard
          logo={<i className='fas fa-briefcase w-11 text-4xl text-violet-500' />}
          title='My Projects'
          loading={myProjectsLoading}
          count={myProjects?.length}
          link={`/persons/${personId}/projects`}
          emptyText='no projects'
          description='Project created through org'
        />
        <DashboardCard
          logo={<i className='fas fa-tasks w-11 text-4xl text-blue-500' />}
          title='My Tasks'
          loading={myTasksLoading}
          count={myTasks?.length}
          emptyText='no tasks'
          link={`/persons/${personId}/tasks`}
          description='Tasks created from projects'
        />
        <DashboardCard
          logo={<i className='fas fa-building w-11 text-4xl text-emerald-500' />}
          title='My Organizations'
          loading={myOrganizationRolesLoading}
          count={myOrganizationRoles?.length}
          emptyText='no organizations'
          link={`/persons/${personId}/organizations`}
          action={{
            text: 'Create an Organization',
            link: '/organizations/create',
          }}
        />
        <DashboardCard
          logo={<i className='fas fa-user-friends w-11 text-4xl text-green-500' />}
          title='Organizations'
          loading={organizationsLoading}
          count={organizations?.length}
          emptyText='no organizations'
          link='/organizations'
          action={{
            text: 'Create an Organization',
            link: '/organizations/create',
          }}
        />
        <DashboardCard
          logo={<i className='fas fa-book w-11 text-4xl text-teal-500' />}
          title='Projects'
          loading={projectsLoading}
          count={projects?.length}
          link='/projects'
          emptyText='no projects'
          description='Project created through org'
        />
        <DashboardCard
          logo={<i className='fas fa-user w-11 text-4xl text-yellow-500' />}
          title='All Members'
          loading={personsLoading}
          count={persons?.length}
          emptyText='no members'
          link='/persons'
          action={{
            text: 'View my profile',
            link: `/persons/${personId}`,
          }}
        />
      </Space>
    </div>
  )
}
