package org.onestep.relief.dropwizard.customDozer;

import org.dozer.CustomConverter;
import org.onestep.relief.dropwizard.representations.TaskRepr;
import org.onestep.relief.organization.model.Task;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class TaskToTaskRepr implements CustomConverter {
    @Override
    public Object convert(Object dest, Object source, Class<?> aClass, Class<?> aClass1) {
        if (source == null)
            return null;

        /*
         * ProjectRepr -> Project class
         */
        if (source instanceof TaskRepr) {
            TaskRepr taskRepr = (TaskRepr) source;
            Task newTask = new Task();

            newTask.setId(taskRepr.getId());
            newTask.setName(taskRepr.getName());
            newTask.setDescription(taskRepr.getDescription());
            newTask.setAssignedPerson(taskRepr.getAssignedPerson());

            newTask.setCreationDate(Utils.timestampToDate(taskRepr.getCreationDate()));
            newTask.setDueDate(Utils.timestampToDate(taskRepr.getDueDate()));
            newTask.setCompletionDate(Utils.timestampToDate(taskRepr.getCompletionDate()));

            if (taskRepr.getStatus() != null)
                newTask.setStatus(Task.TaskStatus.valueOf(taskRepr.getStatus()));
            if (taskRepr.getVisibility() != null)
                newTask.setVisibility(Task.Visibility.valueOf(taskRepr.getVisibility()));

            newTask.setAttachments((taskRepr.getAttachments() != null)
                    ? taskRepr.getAttachments().stream().map(Utils::toFromFileObj).collect(Collectors.toList())
                    : new ArrayList<>());

            return newTask;

            /*
             * Project -> ProjectRepr class
             */
        } else if (source instanceof Task) {
            Task task = (Task) source;
            TaskRepr repr = new TaskRepr();

            repr.setId(task.getId());
            repr.setName(task.getName());
            repr.setDescription(task.getDescription());
            repr.setAssignedPerson(task.getAssignedPerson());

            repr.setCreationDate(Utils.dateToTimestamp(task.getCreationDate()));
            repr.setDueDate(Utils.dateToTimestamp(task.getDueDate()));
            repr.setCompletionDate(Utils.dateToTimestamp(task.getCreationDate()));

            if (task.getStatus() != null)
                repr.setStatus(task.getStatus().toString());
            if (task.getVisibility() != null)
                repr.setVisibility(task.getVisibility().toString());

            repr.setAttachments((task.getAttachments() != null)
                    ? task.getAttachments().stream().map(Utils::toFromFileObj).collect(Collectors.toList())
                    : new ArrayList<>());

            return repr;

        } else
            return null;
    }
}
