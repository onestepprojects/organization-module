import { type Project, type ProjectStatusUpdate } from '../types'
import { api } from './api'

export const projectApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getProjects: builder.query<Project[], void>({
      query: () => `/projects`,
      providesTags: ['Project'],
    }),

    getProject: builder.query<Project, string>({
      query: (id) => `/projects/${id}`,
      providesTags: ['Project'],
    }),

    getSubProjects: builder.query<Project[], string>({
      query: (projectId) => `/projects/sub-projects/${projectId}`,
      providesTags: ['Project'],
    }),

    getProjectsByPerson: builder.query<Project[], string>({
      query: (personId) => `/projects/projects-by-person/${personId}`,
      providesTags: ['Project'],
    }),

    getProjectsByOrganization: builder.query<Project[], string>({
      query: (organizationId) => `/projects/sub-projects/${organizationId}`,
      providesTags: ['Project'],
    }),

    createProject: builder.mutation<Project, Project>({
      query: (body) => ({
        url: `/projects/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Project'],
    }),

    updateProject: builder.mutation<Project, Project>({
      query: (body) => ({
        url: `/projects/update`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Project'],
    }),

    deleteProject: builder.mutation<void, string>({
      query: (id) => ({
        url: `/projects/delete/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Project'],
    }),

    createProjectStatusUpdate: builder.mutation<
      ProjectStatusUpdate,
      ProjectStatusUpdate & { projectId: string }
    >({
      query: ({ projectId, ...body }) => ({
        url: `/projects/create-status/${projectId}`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Project'],
    }),

    removeProjectStatusUpdate: builder.mutation<
      ProjectStatusUpdate,
      ProjectStatusUpdate & { projectId: string }
    >({
      query: ({ projectId, ...body }) => ({
        url: `/projects/remove-status/${projectId}`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Project'],
    }),
  }),
})

export const {
  useGetProjectsQuery,
  useGetProjectQuery,
  useGetSubProjectsQuery,
  useLazyGetSubProjectsQuery,
  useGetProjectsByPersonQuery,
  useGetProjectsByOrganizationQuery,
  useLazyGetProjectsByOrganizationQuery,
  useCreateProjectMutation,
  useUpdateProjectMutation,
  useDeleteProjectMutation,

  useCreateProjectStatusUpdateMutation,
  useRemoveProjectStatusUpdateMutation,
} = projectApi
