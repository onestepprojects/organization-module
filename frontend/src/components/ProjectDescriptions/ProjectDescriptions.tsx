import { Descriptions } from 'antd'
import { lowerCase } from 'lodash-es'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import type { CompositeRoleRequest, Project } from '../../store'

export interface ProjectDescriptionsProps {
  title?: string
  project: Project
  compositeRoleRequests: CompositeRoleRequest[]
}

export const ProjectDescriptions: FC<ProjectDescriptionsProps> = ({
  title,
  project,
  compositeRoleRequests,
}) => {
  return (
    <Descriptions
      title={title || project?.name}
      column={1}
      size='small'
      labelStyle={{ width: 128 }}
    >
      <Descriptions.Item label='Tagline'>{project?.tagline}</Descriptions.Item>
      <Descriptions.Item label='Primary type'>{lowerCase(project?.primaryType)}</Descriptions.Item>
      <Descriptions.Item label='Secondary type'>
        {lowerCase(project?.secondaryType)}
      </Descriptions.Item>
      <Descriptions.Item label='Urgency level'>
        {lowerCase(project?.urgencyLevel)}
      </Descriptions.Item>
      <Descriptions.Item label='Stage'>{lowerCase(project?.projectStage)}</Descriptions.Item>
      {compositeRoleRequests && (
        <Descriptions.Item label='Pending Invites'>
          {compositeRoleRequests?.length ? (
            <Link to={`/projects/${project?.id}/requests`}>
              {compositeRoleRequests.length} click to view
            </Link>
          ) : (
            0
          )}
        </Descriptions.Item>
      )}
    </Descriptions>
  )
}
