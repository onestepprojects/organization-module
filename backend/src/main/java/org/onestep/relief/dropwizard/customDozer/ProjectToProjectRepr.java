package org.onestep.relief.dropwizard.customDozer;

import org.dozer.CustomConverter;
import org.onestep.relief.dropwizard.representations.*;
import org.onestep.relief.organization.model.Location;
import org.onestep.relief.organization.model.Role;
import org.onestep.relief.organization.model.Project;
import org.onestep.relief.organization.model.ProjectStatusUpdate;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectToProjectRepr implements CustomConverter {
    @Override
    public Object convert(Object dest, Object source, Class<?> aClass, Class<?> aClass1) {
        if (source == null)
            return null;

        /*
         * ProjectRepr -> Project class
         */
        if (source instanceof ProjectRepr) {
            ProjectRepr repr = (ProjectRepr) source;
            Project newProject = new Project();

            newProject.setId(repr.getId());
            newProject.setName(repr.getName());
            newProject.setDescription(repr.getDescription());
            newProject.setTargetArea(mapToLocations(repr.getTargetArea()));
            newProject.setHealth(repr.isHealth());

            newProject.setPrimaryType(toFromProjectType(repr.getPrimaryType()));
            newProject.setSecondaryType(toFromProjectType(repr.getSecondaryType()));
            newProject.setTagline(repr.getTagline());

            newProject.setProfilePicture(Utils.toFromFileObj(repr.getProfilePicture()));
            newProject.setCreationDate(Utils.timestampToDate(repr.getCreationDate()));
            newProject.setStartDate(Utils.timestampToDate(repr.getStartDate()));
            newProject.setCloseDate(Utils.timestampToDate(repr.getCloseDate()));

            newProject.setCurrentStatusUpdate(toProjectStatusUpdate(repr.getCurrentStatusUpdate()));

            newProject.setStatusUpdatesHistory((repr.getStatusUpdatesHistory() != null) ? repr.getStatusUpdatesHistory()
                    .stream().map(this::toProjectStatusUpdate).collect(Collectors.toList()) : new ArrayList<>());

            newProject.setPartnerOrganizations(repr.getPartnerOrganizations());
            newProject.setUrgencyLevel((repr.getUrgencyLevel() != null)
                    ? Project.UrgencyLevel.valueOf(repr.getUrgencyLevel().toUpperCase()) : Project.UrgencyLevel.LOW);

            newProject.setProjectStage((repr.getProjectStage() != null)
                    ? Project.ProjectStage.valueOf(repr.getProjectStage()) : Project.ProjectStage.INITIATED);

            newProject.setRequesterRoles((repr.getRequesterRoles() != null) ? repr.getRequesterRoles().stream()
                    .map(r -> Role.valueOf(r.toUpperCase())).collect(Collectors.toList()) : new ArrayList<>());

            newProject.setSocialMedia(repr.getSocialMedia());

            return newProject;

            /*
             * Project -> ProjectRepr class
             */
        } else if (source instanceof Project) {
            Project p = (Project) source;
            ProjectRepr projectRepr = new ProjectRepr();

            Map<String, RepLocation> locations = new HashMap<>();
            if (p.getTargetArea() != null)
                p.getTargetArea().forEach((k, v) -> locations.put(k, Utils.toLocation(v)));

            projectRepr.setId(p.getId());
            projectRepr.setName(p.getName());
            projectRepr.setDescription(p.getDescription());
            projectRepr.setTargetArea(locations);
            projectRepr.setHealth(p.isHealth());

            projectRepr.setPrimaryType(toFromProjectType(p.getPrimaryType()));
            projectRepr.setSecondaryType(toFromProjectType(p.getSecondaryType()));
            projectRepr.setTagline(p.getTagline());

            projectRepr.setProfilePicture(Utils.toFromFileObj(p.getProfilePicture()));
            projectRepr.setCreationDate(Utils.dateToTimestamp(p.getCreationDate()));
            projectRepr.setStartDate(Utils.dateToTimestamp(p.getStartDate()));
            projectRepr.setCloseDate(Utils.dateToTimestamp(p.getCloseDate()));

            projectRepr.setCurrentStatusUpdate(toProjectStatusUpdate(p.getCurrentStatusUpdate()));
            projectRepr.setStatusUpdatesHistory((p.getStatusUpdatesHistory() != null)
                    ? p.getStatusUpdatesHistory().stream().map(this::toProjectStatusUpdate).collect(Collectors.toList())
                    : new ArrayList<>());

            projectRepr.setPartnerOrganizations(p.getPartnerOrganizations());

            if (p.getUrgencyLevel() != null)
                projectRepr.setUrgencyLevel(p.getUrgencyLevel().toString());
            if (p.getProjectStage() != null)
                projectRepr.setProjectStage(p.getProjectStage().toString());

            projectRepr.setRequesterRoles((p.getRequesterRoles() != null)
                    ? p.getRequesterRoles().stream().map(Enum::toString).collect(Collectors.toList())
                    : new ArrayList<>());

            projectRepr.setSocialMedia(p.getSocialMedia());

            return projectRepr;

        } else
            return null;
    }

    private Map<String, Location> mapToLocations(Map<String, RepLocation> repLocations) {
        Map<String, Location> locations = new HashMap<>();

        if (repLocations != null)
            repLocations.forEach((key, value) -> locations.put(key, Utils.toLocation(value)));

        return locations;
    }

    public ProjectStatusUpdate toProjectStatusUpdate(ProjectStatusUpdateRepr update) {
        if (update == null)
            return null;

        ProjectStatusUpdate projectUpdate = new ProjectStatusUpdate();

        projectUpdate.setAuthorID(update.getAuthorID());
        projectUpdate.setDate(Utils.timestampToDate(update.getDate()));
        projectUpdate.setTitle(update.getTitle());
        projectUpdate.setDescription(update.getDescription());

        return projectUpdate;
    }

    public ProjectStatusUpdateRepr toProjectStatusUpdate(ProjectStatusUpdate update) {
        if (update == null)
            return null;

        ProjectStatusUpdateRepr uRepr = new ProjectStatusUpdateRepr();

        uRepr.setAuthorID(update.getAuthorID());
        uRepr.setDate(Utils.dateToTimestamp(update.getDate()));
        uRepr.setTitle(update.getTitle());
        uRepr.setDescription(update.getDescription());

        return uRepr;
    }

    private Project.ProjectType toFromProjectType(String type) {
        return (type != null) ? Project.ProjectType.valueOf(type) : null;
    }

    private String toFromProjectType(Project.ProjectType type) {
        return (type != null) ? type.toString() : null;
    }
}
