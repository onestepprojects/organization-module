export interface AccreditationEntity {
  accreditationId: string
  accreditationState: 'Pending' | 'Approved' | string
  date: string
  documents: any[]
  entityId: string
  entityType: 'ORG' | 'PER' | string
  role: 'Expert' | string
}
