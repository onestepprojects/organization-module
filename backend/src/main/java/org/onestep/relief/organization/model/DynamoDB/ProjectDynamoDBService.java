package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.Project;

import java.util.List;

public class ProjectDynamoDBService {

    DynamoDBMapper mapper;

    @Inject
    public ProjectDynamoDBService(DynamoDBMapperBuilder builder) {
        this.mapper = builder.buildMapper("Project");
    }

    public void save(Object obj) {
        this.mapper.save(obj);
    }

    public Project load(String projectId) {
        return this.mapper.load(Project.class, projectId);
    }

    public void delete(String projectId) {
        Project project = load(projectId);
        if (project != null) {
            this.mapper.delete(project);
        }
    }

    public void deleteAll() {
        mapper.generateDeleteTableRequest(Project.class);
    }

    public PaginatedScanList<Project> scan() {
        return this.mapper.scan(Project.class, new DynamoDBScanExpression());
    }

    public PaginatedScanList<Project> scan(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Project.class, dynamoDBScanExpression);
    }

    public List<Project> query(DynamoDBQueryExpression<Project> queryExpression) {
        return this.mapper.query(Project.class, queryExpression);
    }
}