import { Icon } from 'leaflet'
import markerIconPng from 'leaflet/dist/images/marker-icon.png'
import 'leaflet/dist/leaflet.css'
import { type FC } from 'react'
import { MapContainer, Marker, Popup, TileLayer, type MapContainerProps } from 'react-leaflet'

export interface MapsProps extends MapContainerProps {
  className?: string
  address?: string
}

export const Maps: FC<MapsProps> = ({ center, address = '', ...restProps }) => {
  return (
    <MapContainer center={center} {...restProps}>
      <TileLayer url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' />
      <Marker
        position={center}
        icon={
          new Icon({
            iconUrl: markerIconPng,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
          })
        }
      >
        {address && <Popup>{address}</Popup>}
      </Marker>
    </MapContainer>
  )
}
