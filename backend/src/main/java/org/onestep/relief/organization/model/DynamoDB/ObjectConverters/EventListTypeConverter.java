package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class EventListTypeConverter implements DynamoDBTypeConverter<String, List<Event>> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(List<Event> events) {
        return DynamoDBTypeConverterHelper.getInstance().convert(events, "events", log);
    }

    @Override
    public List<Event> unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Event> statusUpdates;
        try {
            statusUpdates = objectMapper.readValue(s, new TypeReference<List<Event>>() {
            });
            log.debug("unmarshalled address to " + statusUpdates);
        } catch (IOException e) {
            log.error("error de-marshalling address: " + s, e);
            return null;
        }
        return statusUpdates;
    }
}