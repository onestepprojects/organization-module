package org.onestep.relief.dropwizard.resources;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.jersey.validation.Validators;
import io.findify.s3mock.S3Mock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.onestep.relief.dropwizard.SystemEnvValues;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.model.Person;
import org.onestep.relief.organization.service.*;
import software.amazon.awssdk.auth.credentials.AnonymousCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;

import javax.validation.Validator;
import java.net.URI;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class BaseTest {

    protected AmazonDynamoDB dbClient;

    private S3Mock s3Server;
    protected S3Client s3Client;
    protected RewardService rewardService = mock(RewardService.class);
    protected ServiceTokenService serviceTokenService = mock(ServiceTokenService.class);
    protected CognitoService cognitoUserFetcher = mock(CognitoService.class);
    protected HealthProjectService healthProjectService = mock(HealthProjectService.class);
    protected PaymentService paymentService = mock(PaymentService.class);
    protected OrgServiceInterface organizationService;

    protected Injector injector;
    protected Person person;

    private SystemEnvValues createTestingSystemEnvValues() {
        SystemEnvValues values = new SystemEnvValues();
        values.ORG_LOGO_BUCKET = "onestep-org-pics";
        values.PERSON_PIC_BUCKET = "onestep-person-pics";
        values.PROJECT_PIC_BUCKET = "onestep-project-pics";
        return values;
    }

    private SystemEnvValues testingSystemEnvValues;

    protected void setupUserFetcher(Person person) throws AuthenticationException {
        this.person = person;
        reset(cognitoUserFetcher);
        when(cognitoUserFetcher.fetchUser(anyString())).thenReturn(EntityBuilder.newCognitoUser(person));
    }

    @BeforeEach
    public void beforeEach() throws Throwable {
        this.testingSystemEnvValues = this.createTestingSystemEnvValues();
        AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(
                "http://localhost:8000", Regions.US_EAST_1.getName());
        dbClient = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(endpointConfiguration).build();

        s3Server = new S3Mock.Builder().withPort(8001).withInMemoryBackend().build();
        s3Server.start();

        s3Client = provideS3Client();
        String[] buckets = { testingSystemEnvValues.ORG_LOGO_BUCKET, testingSystemEnvValues.PERSON_PIC_BUCKET,
                testingSystemEnvValues.PROJECT_PIC_BUCKET };
        for (String bucket : buckets) {
            s3Client.createBucket(CreateBucketRequest.builder().bucket(bucket).build());
        }

        injector = Guice.createInjector(new AbstractModule() {

            @Override
            protected void configure() {
                bind(AmazonDynamoDB.class).toInstance(dbClient);
                bind(Validator.class).toInstance(Validators.newValidator());
                bind(S3Client.class).toInstance(s3Client);
                bind(CognitoService.class).toInstance(cognitoUserFetcher);
                bind(RewardService.class).toInstance(rewardService);
                bind(ServiceTokenService.class).toInstance(serviceTokenService);
                bind(SystemEnvValues.class).toInstance(createTestingSystemEnvValues());
                bind(HealthProjectService.class).toInstance(healthProjectService);
                bind(PaymentService.class).toInstance(paymentService);
            }
        });
        this.organizationService = this.injector.getInstance(OrgServiceInterface.class);
        this.setupUserFetcher(this.organizationService.getAllPersons().get(0));
    }

    private S3Client provideS3Client() {
        return S3Client.builder()
                .endpointOverride(URI.create("http://localhost:8001"))
                .credentialsProvider(AnonymousCredentialsProvider.create())
                .region(Region.US_EAST_1)
                .build();
    }

    @AfterEach
    public void afterEach() {
        if (s3Server != null)
            s3Server.shutdown();
    }

}
