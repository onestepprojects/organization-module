package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.DynamoDB.ObjectConverters.*;

import java.util.*;

/**
 * A relief organization participating on the OneStep platform.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "Organization")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Organization", description = "Organizations")
@Builder
public class Organization {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    private String name;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private OrganizationType type;

    @DynamoDBAttribute
    private String tagline;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = FileTypeConverter.class)
    private File logo = new File();

    @DynamoDBAttribute
    private String email;

    @DynamoDBAttribute
    private Map<String, String> phones = new HashMap<>();

    @DynamoDBAttribute
    private String website;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = AddressTypeConverter.class)
    private Address address;

    @DynamoDBAttribute
    private Map<String, String> socialMedia;

    @DynamoDBAttribute
    private String creator;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = OrganizationStatusTypeConverter.class)
    private OrganizationStatus status;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = DescriptionTypeConverter.class)
    private Description description;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = PaymentAccountTypeConverter.class)
    private PaymentAccount paymentAccount = new PaymentAccount();

    @DynamoDBAttribute
    private boolean isSupplier;

    @DynamoDBAttribute
    private boolean isActive = true;

    @DynamoDBIgnore
    private List<Role> requesterRoles;

    public enum OrganizationType {
        FOR_PROFIT("For-Profit"), NON_PROFIT("Non-Profit / Non-Governmental Organization"), FOUNDATION("Foundation"),
        GOVERNMENTAL("Governmental Organization"), EDUCATIONAL("Educational Institution"), HYBRID("Hybrid");

        private final String label;
        private static final Map<String, OrganizationType> BY_LABEL = new HashMap<>();

        static {
            for (OrganizationType e : values()) {
                BY_LABEL.put(e.label, e);
            }
        }

        OrganizationType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public static OrganizationType valueOfLabel(String label) {
            return BY_LABEL.get(label);
        }
    }

    public Organization(String name, File logo, String email, String website, Map<String, String> phones) {
        this.name = name;
        this.email = email;
        this.phones = phones;

        // nullable properties
        if (website != null) {
            this.website = website;
        }

        if (logo != null) {
            this.logo = logo;
        }
    }
}