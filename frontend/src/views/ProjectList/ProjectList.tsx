import { Space, Spin } from 'antd'
import { lowerCase } from 'lodash-es'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { ListItemCard, PageTitle, ServerError } from '../../components'
import {
  useAppSelector,
  useGetProjectsByOrganizationQuery,
  useGetProjectsByPersonQuery,
  useGetProjectsQuery,
  useGetSubProjectsQuery,
} from '../../store'

export const ProjectList: FC = () => {
  const myPersonId = useAppSelector((state) => state.auth.person?.uuid)
  const { organizationId, personId, projectId } = useParams<{
    organizationId?: string
    personId?: string
    projectId?: string
  }>()
  const { isLoading, data, error } = personId
    ? // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetProjectsByPersonQuery(personId)
    : organizationId
    ? // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetProjectsByOrganizationQuery(organizationId)
    : projectId
    ? // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetSubProjectsQuery(projectId)
    : // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetProjectsQuery()

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>
        {personId === myPersonId
          ? 'My'
          : personId
          ? 'User'
          : organizationId
          ? 'Org'
          : projectId
          ? 'Sub'
          : 'All'}
        {` Projects`}
      </PageTitle>

      {error ? (
        <ServerError />
      ) : (
        <Spin spinning={isLoading} delay={500}>
          <Space className='mb-8' wrap size={['large', 48]}>
            {data?.map((project) => (
              <ListItemCard
                key={project.id}
                cover={project.profilePicture?.url}
                coverFallback='https://onestep-project-pics.s3.amazonaws.com/defaultproject.svg'
                title={project.name}
                description={`Type: ${lowerCase(project.primaryType)}`}
                link={`/projects/${project.id}`}
              />
            ))}
          </Space>
        </Spin>
      )}
    </div>
  )
}
