package org.onestep.relief.organization.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.onestep.relief.dropwizard.SystemEnvValues;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.model.CognitoService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

public class ServiceTokenService {

    private SystemEnvValues systemEnvValues;

    @Inject
    public ServiceTokenService(SystemEnvValues systemEnvValues) {
        this.systemEnvValues = systemEnvValues;
    }

    /**
     * Retrieves a service account access token and prepends with "Bearer ".
     *
     * @return - the service auth token for the Organization, Person and Project
     *         Module
     *
     * @throws AuthenticationException - thrown due to an authentication error
     */
    public String getServiceToken() throws AuthenticationException {
        CognitoService service;
        try {
            // build request
            URL url = new URL(systemEnvValues.APP_BASE_URL + systemEnvValues.AUTH_SERVICE_LOGIN_PATH);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            // build request Basic auth header
            String credentials = systemEnvValues.SERVICE_CLIENT_ID + ":" + systemEnvValues.SERVICE_CLIENT_SECRET;
            String encodedCredentials = Base64.getEncoder().encodeToString(credentials.getBytes());
            String authToken = "Basic " + encodedCredentials;
            connection.setRequestProperty("Authorization", authToken);

            // read response
            BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String output;
            StringBuilder response = new StringBuilder();
            while ((output = input.readLine()) != null) {
                response.append(output);
            }
            input.close();

            // map to CognitoService object
            ObjectMapper objectMapper = new ObjectMapper();
            service = objectMapper.readValue(response.toString(), CognitoService.class);
        } catch (Exception e) {
            throw new AuthenticationException("Get Cognito service token", "Authentication failed");
        }

        // prepend token with Bearer
        return "Bearer " + service.getAccess_token();
    }
}
