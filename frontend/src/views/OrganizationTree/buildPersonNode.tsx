import { UserOutlined } from '@ant-design/icons'
import { Space } from 'antd'
import { type Person } from '../../store'
import { type PersonNode } from './typings'

export function buildPersonNode(person: Person): PersonNode {
  return {
    type: 'PersonNode',
    person,
    key: person.email,
    title: (
      <Space>
        <UserOutlined className='text-yellow-500' />
        <span>{person.name}</span>
      </Space>
    ),
    isLeaf: true,
  }
}
