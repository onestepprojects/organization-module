package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.google.inject.Inject;
import org.onestep.relief.dropwizard.SystemEnvValues;

public class DynamoDBMapperBuilder {
    private final String prefix;
    private final AmazonDynamoDB client;

    @Inject
    public DynamoDBMapperBuilder(AmazonDynamoDB client, SystemEnvValues systemEnvValues) {
        this.prefix = systemEnvValues.DDB_PREFIX != null ? systemEnvValues.DDB_PREFIX : "";
        this.client = client;
    }

    public DynamoDBMapper buildMapper(String tableName) {
        DynamoDBMapperConfig config = new DynamoDBMapperConfig.Builder().withTableNameOverride(
                DynamoDBMapperConfig.TableNameOverride.withTableNameReplacement(prefix + tableName)).build();

        return new DynamoDBMapper(this.client, config);
    }
}
