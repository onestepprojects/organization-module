package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.PaymentAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type PaymentAccount to a string and vice-versa.
 */

public class PaymentAccountTypeConverter implements DynamoDBTypeConverter<String, PaymentAccount> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(PaymentAccount paymentAccount) {
        return DynamoDBTypeConverterHelper.getInstance().convert(paymentAccount, "payment account", log);
    }

    @Override
    public PaymentAccount unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        PaymentAccount paymentAccount;

        try {
            paymentAccount = objectMapper.readValue(s, PaymentAccount.class);
            log.debug("unmarshalled payment account to " + paymentAccount);
        } catch (IOException e) {
            log.error("error de-marshalling payment account: " + s, e);
            return null;
        }
        return paymentAccount;
    }
}
