import { useMemoizedFn, useUnmountedRef } from 'ahooks'
import { Button, DatePicker, Form, Input, Select, Switch, type FormProps } from 'antd'
import dayjs from 'dayjs'
import { useMemo, useState, type FC } from 'react'
import { projectStageOptions, projectTypeOptions, projectUrgencyLevelOptions } from '../../options'
import { useLazyGetGeographicPositionByAddressQuery, type Project } from '../../store'
import { type Paths } from '../../utils'
import { SingleImageUpload } from '../SingleImageUpload'

interface ProjectFormData extends Project {
  address?: string
  dates?: dayjs.Dayjs[]
}

interface ProjectFormProps extends FormProps<Project> {
  initialValues?: Project
}

function typedPath(paths: Paths<Project>): Paths<Project> {
  return paths
}

export const ProjectForm: FC<ProjectFormProps> = (props) => {
  const { initialValues, onFinish, ...formProps } = props

  const initialProjectFormData = useMemo<ProjectFormData>(
    () =>
      initialValues && {
        ...initialValues,
        dates: [
          initialValues.startDate && dayjs(Number(initialValues.startDate)),
          initialValues.closeDate && dayjs(Number(initialValues.closeDate)),
        ],
      },
    [initialValues]
  )

  const [isSubmitting, setIsSubmitting] = useState(false)

  const unmountRef = useUnmountedRef()

  const [getGeographicPositionByAddress] = useLazyGetGeographicPositionByAddressQuery()

  const handleSubmit = useMemoizedFn(async (values: ProjectFormData) => {
    try {
      setIsSubmitting(true)
      const { address, dates, ...project } = values
      if (address) {
        const loc = await getGeographicPositionByAddress(address).unwrap()
        project.targetArea = {
          area_1: loc,
        }
      }
      if (dates) {
        project.startDate = dates[0]?.valueOf()
        project.closeDate = dates[1]?.valueOf()
      }
      await onFinish(project)
    } catch (err) {
      console.log('Submit project error', err)
    } finally {
      if (!unmountRef.current) {
        setIsSubmitting(false)
      }
    }
  })

  return (
    <Form<ProjectFormData>
      labelCol={{ span: 5 }}
      initialValues={initialProjectFormData}
      onFinish={handleSubmit}
      {...formProps}
    >
      <Form.Item
        label='Project Name'
        name={typedPath(['name'])}
        rules={[
          { required: true, message: 'Name is required' },
          { min: 4, message: 'Name is too short' },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label='Upload Project Image'
        name={typedPath(['profilePicture'])}
        extra='Please select an image.'
      >
        <SingleImageUpload />
      </Form.Item>
      <Form.Item label='Project Tagline' name={typedPath(['tagline'])}>
        <Input />
      </Form.Item>
      <Form.Item label='Health Project?' name={typedPath(['health'])} valuePropName='checked'>
        <Switch />
      </Form.Item>
      <Form.Item
        label='Full address'
        name='address'
        // rules={
        //   initialValues?.targetArea?.area_1
        //     ? undefined
        //     : [
        //         { required: true, message: 'Address is required' },
        //         { pattern: /.+(\s+.+)+/, message: 'Address is invalid' },
        //       ]
        // }
      >
        <Input placeholder='e.g. 123 Some Street, City, ST Zip, Country' />
      </Form.Item>
      <Form.Item
        label='Project description'
        name={typedPath(['description'])}
        rules={[
          { required: true, message: 'Description is required' },
          { min: 10, message: 'Description is too short' },
        ]}
      >
        <Input.TextArea rows={3} />
      </Form.Item>
      <Form.Item shouldUpdate noStyle>
        {(form) => (
          <Form.Item
            label='Primary Type'
            name={typedPath(['primaryType'])}
            rules={[{ required: true }]}
          >
            <Select
              options={projectTypeOptions.map((option) =>
                option.value === form.getFieldValue('secondaryType')
                  ? { ...option, disabled: true }
                  : option
              )}
              allowClear
            />
          </Form.Item>
        )}
      </Form.Item>
      <Form.Item shouldUpdate noStyle>
        {(form) => (
          <Form.Item
            label='Secondary Type'
            name={typedPath(['secondaryType'])}
            rules={[{ required: true }]}
          >
            <Select
              options={projectTypeOptions.map((option) =>
                option.value === form.getFieldValue('primaryType')
                  ? { ...option, disabled: true }
                  : option
              )}
              allowClear
            />
          </Form.Item>
        )}
      </Form.Item>
      <Form.Item
        label='Project Dates'
        name='dates'
        rules={[{ required: true, message: 'Project dates is required' }]}
      >
        <DatePicker.RangePicker format='YYYY-MM-DD' />
      </Form.Item>
      <Form.Item
        label='Urgency Level'
        name={typedPath(['urgencyLevel'])}
        rules={[{ required: true, message: 'Urgency level is required' }]}
      >
        <Select options={projectUrgencyLevelOptions} allowClear />
      </Form.Item>
      <Form.Item
        label='Stage'
        name={typedPath(['projectStage'])}
        rules={[{ required: true, message: 'Stage is required' }]}
      >
        <Select options={projectStageOptions} allowClear />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 5 }}>
        <Button type='primary' htmlType='submit' loading={isSubmitting}>
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}
