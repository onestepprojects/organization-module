import type { Person, PersonRole } from '../types'
import { api } from './api'

export const personApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getPersons: builder.query<Person[], void>({
      query: () => `/persons`,
      providesTags: ['Person'],
    }),

    getPersonsOfParent: builder.query<Person[], string>({
      query: (parentId) => `/persons/get-by-parent/${parentId}`,
      providesTags: ['Person'],
    }),

    getPerson: builder.query<Person, string>({
      query: (id) => `/persons/${id}`,
      providesTags: ['Person'],
    }),

    createPerson: builder.mutation<Person, Person>({
      query: (body) => ({
        url: `persons/create`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Person'],
    }),

    updatePerson: builder.mutation<Person, Person>({
      query: (body) => ({
        url: `/persons/update`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Person'],
    }),

    deletePerson: builder.mutation<void, string>({
      query: (id) => ({
        url: `/persons/delete/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Person'],
    }),

    /** Possible parent: organization, project. */
    getPersonRolesByParent: builder.query<PersonRole[], string>({
      query: (orgId) => `/persons/get-by-parent/${orgId}`,
      providesTags: ['PersonRole'],
    }),
  }),
})

export const {
  useGetPersonsQuery,
  useGetPersonQuery,
  useGetPersonsOfParentQuery,
  useLazyGetPersonsOfParentQuery,

  useCreatePersonMutation,
  useUpdatePersonMutation,

  useGetPersonRolesByParentQuery,
} = personApi
