package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.Organization;
import org.onestep.relief.organization.model.Person;
import org.onestep.relief.organization.model.Role;
import org.onestep.relief.organization.model.Project;

import static java.util.stream.Collectors.toList;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesTripleResponse {
    /**
     * Utility methods to map a pair of (Class, Set[Roles]) to a Triple.
     */
    private String id;
    private String name;
    private List<String> roles;

    public static List<RolesTripleResponse> personTriples(Map<Person, Set<Role>> map) {
        return map.entrySet().stream().map(e -> new RolesTripleResponse(e.getKey().getId(), e.getKey().getName(),
                e.getValue().stream().map(Enum::toString).collect(toList()))).collect(toList());
    }

    public static List<RolesTripleResponse> projectTriples(Map<Project, Set<Role>> map) {
        return map.entrySet().stream().map(e -> new RolesTripleResponse(e.getKey().getId(), e.getKey().getName(),
                e.getValue().stream().map(Enum::toString).collect(toList()))).collect(toList());
    }

    public static List<RolesTripleResponse> organizationTriples(Map<Organization, Set<Role>> map) {
        return map.entrySet().stream().map(e -> new RolesTripleResponse(e.getKey().getId(), e.getKey().getName(),
                e.getValue().stream().map(Enum::toString).collect(toList()))).collect(toList());
    }

}
