import { Button, Card, Descriptions, Image, Popconfirm, message } from 'antd'
import { lowerCase } from 'lodash-es'
import { type FC } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { AccreditationDescriptions, Maps, PageTitle, SocialMediaCard } from '../../components'
import {
  skipToken,
  useAppSelector,
  useDeleteOrganizationMutation,
  useGetAccreditationEntitiesByIdQuery,
  useGetCompositeRoleRequestsQuery,
  useGetOrganizationQuery,
  useGetParentOrganizationQuery,
  useGetPersonRolesByParentQuery,
  useGetProjectsByOrganizationQuery,
  useGetSubOrganizationsQuery,
} from '../../store'

export const OrganizationDetail: FC = () => {
  const navigate = useNavigate()

  const { organizationId } = useParams<{ organizationId: string }>()
  const personId = useAppSelector((state) => state.auth.person?.uuid)

  const { isLoading: isLoadingOrganization, data: organization } = useGetOrganizationQuery(
    organizationId || skipToken
  )
  const { isLoading: isLoadingSubOrganizations, data: subOrganizations } =
    useGetSubOrganizationsQuery(organizationId || skipToken)
  const { isLoading: isLoadingParentOrganization, data: parentOrganization } =
    useGetParentOrganizationQuery(organizationId || skipToken)
  const { isLoading: isLoadingAccreditationEntities, data: accreditationEntities } =
    useGetAccreditationEntitiesByIdQuery(organizationId || skipToken)
  const { isLoading: isLoadingPersonRoles, data: personRoles = [] } =
    useGetPersonRolesByParentQuery(organizationId || skipToken)
  const { isLoading: isLoadingProjects, data: projects } = useGetProjectsByOrganizationQuery(
    organizationId || skipToken
  )
  const { isLoading: isLoadingCompositeRoleRequests, data: compositeRoleRequests } =
    useGetCompositeRoleRequestsQuery(organizationId || skipToken)

  const [deleteOrganization] = useDeleteOrganizationMutation()

  const canJoin = personRoles?.length && personRoles.every(({ id }) => id !== personId)
  const isAdmin = organization?.requesterRoles.some((role) =>
    ['ADMIN', 'GLOBAL_ADMIN'].includes(role)
  )

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Organization</PageTitle>

      <div className='mb-8 grid grid-cols-4 gap-4'>
        <Card
          className='col-span-4 md:col-span-2 row-span-3 h-min rounded-xl'
          loading={isLoadingOrganization}
          cover={
            <Image
              className='!rounded-t-xl object-cover'
              src={organization?.logo?.url}
              fallback='https://onestep-org-pics.s3.amazonaws.com/defaultorg.svg'
              preview={false}
            />
          }
          actions={[
            canJoin && (
              <Link to={`/organizations/${organizationId}/join`}>
                <Button type='primary'>Join</Button>
              </Link>
            ),
            isAdmin && (
              <Link to={`/organizations/${organizationId}/update`}>
                <Button type='primary'>Edit Profile</Button>
              </Link>
            ),
            <Link to='/organizations'>
              <Button type='primary'>View All Orgs</Button>
            </Link>,
            isAdmin && (
              <Popconfirm
                title='Are you sure you wish to delete this organization?'
                okText='Delete'
                okButtonProps={{ danger: true }}
                onConfirm={async () => {
                  try {
                    await deleteOrganization(organizationId).unwrap()
                    message.success('Organization deleted')
                    navigate('/', { replace: true })
                  } catch (err) {
                    console.warn('Organization delete fail', organizationId, err)
                    message.error('Organization delete failed')
                  }
                }}
              >
                <Button type='primary' danger icon={<i className='mr-2 far fa-trash-alt' />}>
                  Delete
                </Button>
              </Popconfirm>
            ),
          ].filter(Boolean)}
        >
          <Card.Meta
            title={organization?.name}
            description={
              <div className='flex flex-col gap-4'>
                <div>{organization?.description?.description}</div>
                <AccreditationDescriptions accreditationEntities={accreditationEntities} />
                <Descriptions column={1} size='small' labelStyle={{ width: 96 }}>
                  <Descriptions.Item label='Type'>
                    {lowerCase(organization?.type)}
                  </Descriptions.Item>
                  <Descriptions.Item label='Tagline'>{organization?.tagline}</Descriptions.Item>
                  <Descriptions.Item label='Location'>
                    {organization?.address.city}
                  </Descriptions.Item>
                  <Descriptions.Item label='Status'>
                    {lowerCase(organization?.statusLabel)}
                  </Descriptions.Item>
                  {!!parentOrganization && (
                    <Descriptions.Item label='Parent Org'>
                      {parentOrganization.name}
                    </Descriptions.Item>
                  )}
                  {isAdmin && (
                    <Descriptions.Item label='Pending Invites'>
                      {compositeRoleRequests?.length ? (
                        <Link to={`/organizations/${organizationId}/requests`}>
                          {compositeRoleRequests?.length} view
                        </Link>
                      ) : (
                        0
                      )}
                    </Descriptions.Item>
                  )}
                </Descriptions>
              </div>
            }
          />
        </Card>

        {organization?.description.serviceArea.length > 0 ? (
          <Card
            className='col-span-4 md:col-span-2 h-min bg-transparent'
            bordered={false}
            bodyStyle={{ padding: 0 }}
            cover={
              <Maps
                className='w-full h-96 rounded-xl'
                center={[
                  organization.description.serviceArea[0].latitude,
                  organization.description.serviceArea[0].longitude,
                ]}
                zoom={8}
                address={organization.address.address}
              />
            }
          />
        ) : (
          <Card
            className='col-span-4 md:col-span-2 h-min rounded-xl'
            title='Location'
            loading={isLoadingOrganization}
          >
            <div className='h-full flex flex-col justify-center items-center'>
              <i className='fas fa-map-marker-alt fa-4x' />
              <div>No location</div>
            </div>
          </Card>
        )}

        <SocialMediaCard
          className='col-span-4 md:col-span-2 h-min'
          loading={isLoadingOrganization}
          socialMedia={organization?.socialMedia}
        />

        <Card
          className='col-span-4 md:col-span-2 h-min rounded-xl'
          title='Contact'
          loading={isLoadingOrganization}
        >
          <div className='flex flex-col gap-2'>
            {organization?.website && (
              <div className='flex items-center'>
                <i className='mr-4 fas fa-globe fa-lg' />
                <a
                  href={
                    organization.website.startsWith('http')
                      ? organization.website
                      : `http://${organization.website}`
                  }
                  target='_blank'
                >
                  {organization.website}
                </a>
              </div>
            )}
            {organization?.phones.phone && (
              <div className='flex items-center'>
                <i className='mr-4 fas fa-phone-alt fa-lg' />
                <a href={`tel:${organization.phones.phone}`} target='_blank'>
                  {organization.phones.phone}
                </a>
              </div>
            )}
            {organization?.email && (
              <div className='flex items-center'>
                <i className='mr-4 fas fa-envelope fa-lg' />
                <a href={`mailto:${organization.email}`} target='_blank'>
                  {organization.email}
                </a>
              </div>
            )}
          </div>
        </Card>

        <Card
          className='col-span-4 md:col-span-2 h-min rounded-xl'
          loading={isLoadingSubOrganizations}
          title='Sub Organizations'
          extra={
            isAdmin && (
              <Link to={`/organizations/${organizationId}/organizations/create`}>
                <Button type='link'>Create</Button>
              </Link>
            )
          }
        >
          {subOrganizations?.length > 0 ? (
            <div>
              {subOrganizations.length} Total
              <Link to={`/organizations/${organizationId}/organizations`}>
                <Button type='link'>view</Button>
              </Link>
            </div>
          ) : (
            <div>No sub organization</div>
          )}
        </Card>

        <Card
          className='col-span-2 md:col-span-1 h-min rounded-xl'
          loading={isLoadingPersonRoles}
          title={
            <div className='flex items-center gap-2'>
              <i className='fas fa-user fa-lg text-green-700' />
              <div>Members</div>
            </div>
          }
          extra={
            isAdmin && (
              <Link to={`/invite/organization/${organizationId}`}>
                <Button type='link'>Invite</Button>
              </Link>
            )
          }
        >
          {personRoles?.length > 0 ? (
            <div className='flex items-end text-2xl text-slate-600'>
              <div className='mx-2 font-medium text-green-700'>{personRoles.length}</div>
              <div>Total</div>
              <Link to={`/organizations/${organizationId}/persons`}>
                <Button type='link' size='small'>
                  view
                </Button>
              </Link>
            </div>
          ) : (
            <div>No members</div>
          )}
        </Card>

        <Card
          className='col-span-2 md:col-span-1 h-min rounded-xl'
          loading={isLoadingProjects}
          title={
            <div className='flex items-center gap-2'>
              <i className='fas fa-project-diagram fa-lg text-green-700' />
              <div>Projects</div>
            </div>
          }
          extra={
            isAdmin && (
              <Link to={`/projects/create?parentType=ORGANIZATION&parentId=${organizationId}`}>
                <Button type='link'>Create</Button>
              </Link>
            )
          }
        >
          {projects?.length > 0 ? (
            <div className='flex items-end text-2xl text-slate-600'>
              <div className='mx-2 font-medium text-green-700'>{projects.length}</div>
              <div>Total</div>
              <Link to={`/organizations/${organizationId}/projects`}>
                <Button type='link' size='small'>
                  view
                </Button>
              </Link>
            </div>
          ) : (
            <div>No projects</div>
          )}
        </Card>
      </div>
    </div>
  )
}
