import { render, screen } from '@testing-library/react'
import type { Project } from '../../store'
import { wrapper } from '../../tests'
import { ProjectSubProjectsCard } from './ProjectSubProjectsCard'

describe('ProjectSubProjectsCard', () => {
  it('renders no projects if sub projects are empty', () => {
    render(<ProjectSubProjectsCard project={{} as Project} subProjects={[]} />, { wrapper })

    expect(screen.getByText('No projects')).toBeInTheDocument()
  })

  it('renders tasks total number and view link', () => {
    render(<ProjectSubProjectsCard project={{} as Project} subProjects={[{}, {}] as Project[]} />, {
      wrapper,
    })

    expect(screen.getByText('2')).toBeInTheDocument()
    expect(screen.getByText('Total')).toBeInTheDocument()
    expect(screen.getByRole('link', { name: 'view' })).toBeInTheDocument()
  })

  it('renders a create button if admin', () => {
    render(<ProjectSubProjectsCard project={{} as Project} subProjects={[]} isAdmin />, { wrapper })

    expect(screen.getByRole('button', { name: 'Create' })).toBeInTheDocument()
  })
})
