package org.onestep.relief.organization.model.S3;

import com.amazonaws.util.Base64;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.File;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.IOException;

public class S3Service {
    private final S3Client client;

    @Inject
    public S3Service(S3Client client) {
        this.client = client;
    }

    public String put(File image) throws IOException {
        // build the request
        PutObjectRequest putRequest = PutObjectRequest.builder().bucket(image.getBucketName()).key(image.getName())
                .contentType(image.getFormat().getContentType()).acl(ObjectCannedACL.PUBLIC_READ).build();

        // strip the leading contentType text to get just the Base64 data
        String data = image.getBody().split(",")[1];

        // convert to a byte array and upload
        byte[] bytes = Base64.decode(data);
        RequestBody body = RequestBody.fromBytes(bytes);
        client.putObject(putRequest, body);

        // retrieve and return the URL of the new file
        GetUrlRequest urlRequest = GetUrlRequest.builder().bucket(image.getBucketName()).key(image.getName()).build();
        return client.utilities().getUrl(urlRequest).toExternalForm();
    }

    public void delete(String bucket, String key) {
        DeleteObjectRequest request = DeleteObjectRequest.builder().bucket(bucket).key(key).build();
        client.deleteObject(request);
    }
}
