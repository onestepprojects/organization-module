import dayjs from 'dayjs'

export const formatUnixTimestamp = (timestamp: string | number) =>
  timestamp ? dayjs(Number(timestamp)).format('YYYY-MM-DD hh:mm') : ''
