import { render, screen } from '@testing-library/react'
import type { Organization, Person, Project, Task } from '../../store'
import { wrapper } from '../../tests'
import { PersonSummaryCardMeta } from './PersonSummaryCardMeta'

describe('PersonSummaryCardMeta', () => {
  it('renders correctly', () => {
    render(
      <PersonSummaryCardMeta
        person={{} as Person}
        organizations={[{ id: 'o1', name: 'Organization 1' }] as Organization[]}
        projects={
          [
            { id: 'p1', name: 'Project 1' },
            { id: 'p2', name: 'Project 2' },
          ] as Project[]
        }
        tasks={
          [
            { id: 't1', name: 'Task 1' },
            { id: 't2', name: 'Task 2' },
            { id: 't3', name: 'Task 3' },
          ] as Task[]
        }
      />,
      { wrapper }
    )

    expect(screen.getByText('Organizations')).toBeInTheDocument()
    expect(screen.getByRole('link', { name: '1' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Organization 1' })).toBeInTheDocument()

    expect(screen.getByText('Projects')).toBeInTheDocument()
    expect(screen.getByRole('link', { name: '2' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Project 1' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Project 2' })).toBeInTheDocument()

    expect(screen.getByText('Tasks')).toBeInTheDocument()
    expect(screen.getByRole('link', { name: '3' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Task 1' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Task 2' })).toBeInTheDocument()
    expect(screen.getByRole('button', { name: 'Task 3' })).toBeInTheDocument()
  })
})
