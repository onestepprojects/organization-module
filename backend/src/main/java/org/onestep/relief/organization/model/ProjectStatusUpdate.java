package org.onestep.relief.organization.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProjectStatusUpdate {
    private String authorID;
    private Date date;
    private String title;
    private String description;
}
