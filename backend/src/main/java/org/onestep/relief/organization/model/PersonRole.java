package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Indicates the role a person has within a specific organization.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "ReliefEntity")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "ReliefEntity", description = "ReliefEntity")
public class PersonRole extends ReliefEntity {

    @DynamoDBAttribute
    private Date origin;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private Role reliefRole;

    @DynamoDBIgnore
    private String personId;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private RequestStatus requestStatus;

    public enum RequestStatus {
        INVITED, REQUESTED, ACTIVE, INACTIVE
    }
}