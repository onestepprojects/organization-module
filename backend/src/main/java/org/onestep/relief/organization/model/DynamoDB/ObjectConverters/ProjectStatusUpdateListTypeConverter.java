package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.ProjectStatusUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

import java.io.IOException;

public class ProjectStatusUpdateListTypeConverter implements DynamoDBTypeConverter<String, List<ProjectStatusUpdate>> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(List<ProjectStatusUpdate> statusUpdates) {
        return DynamoDBTypeConverterHelper.getInstance().convert(statusUpdates, "status updates", log);
    }

    @Override
    public List<ProjectStatusUpdate> unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        List<ProjectStatusUpdate> statusUpdates;

        try {
            statusUpdates = objectMapper.readValue(s, new TypeReference<List<ProjectStatusUpdate>>() {
            });
            log.debug("unmarshalled address to " + statusUpdates);
        } catch (IOException e) {
            log.error("error de-marshalling address: " + s, e);
            return null;
        }
        return statusUpdates;
    }
}
