package org.onestep.relief.dropwizard.resources;

import org.onestep.relief.organization.model.*;

import java.util.*;

import static org.onestep.relief.organization.model.File.FileFormat.JPG;

public class EntityBuilder {

    public static Organization newOrganization(String seed) {
        return Organization.builder()
            .name("test-org-" + seed)
            .logo(File.builder()
                .name("head-" + seed + ".jpeg")
                .size(120)
                .format(JPG)
                .body("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7")
                .caption("test-caption-" + seed)
                .build())
            .email("email-" + seed + "@onestep.com")
            .phones(new HashMap<>() {
                {
                    this.put("phone", "12345678901");
                }
            })
            .address(Address.builder()
                .text("address address " + seed)
                .city("test-city-" + seed)
                .district("District")
                .state("AK")
                .postalCode("231232")
                .country("United States of America")
                .build())
            .type(Organization.OrganizationType.FOR_PROFIT)
            .isSupplier(true)
            .tagline("Org tagline here " + seed)
            .description(Description.builder()
                .description("Organization description " + seed)
                .projectTypes(List.of("appear", "religious"))
                .serviceArea(List.of(Location.builder().Long(111).Lat(222).build()))
                .build())
            .socialMedia(new HashMap<>() {
                {
                    this.put("twitter", "Twitter " + seed);
                    this.put("linkedin", "LinkedIn " + seed);
                    this.put("facebook", "Facebook " + seed);
                    this.put("youtube", "Youtube " + seed);
                }
            })
            .website("Website " + seed)
            .isActive(true)
            .requesterRoles(List.of(Role.DONOR, Role.EXPERT))
            .status(OrganizationStatus.builder()
                .status(OrganizationStatus.ActivityStatus.ACTIVE)
                .lastActive(new Date(1231231231231L))
                .build())
            .paymentAccount(PaymentAccount.builder()
                .assets(Map.of("asset-" + seed, false))
                .build())
            .build();
    }

    public static Person newPerson(String seed) {
        return Person.builder().cognitoID("cognitoID-" + seed).isActive(true).build();
    }

    public static CognitoUser newCognitoUser(Person person) {
        return CognitoUser.builder()
            .personId(person.getId())
            .data(CognitoUser.CognitoUserData.builder()
                .userId(person.getCognitoID())
                .username(person.getName())
                .roles(new ArrayList<>())
                .build())
            .build();
    }
}
