package org.onestep.relief.organization.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.onestep.relief.dropwizard.SystemEnvValues;
import org.onestep.relief.organization.model.PaymentAccount;
import org.onestep.relief.organization.model.ProvisionPayRequest;
import org.onestep.relief.organization.model.ProvisionPayResponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class PaymentService {

    private SystemEnvValues systemEnvValues;

    @Inject
    public PaymentService(SystemEnvValues systemEnvValues) {
        this.systemEnvValues = systemEnvValues;
    }

    /**
     * Initiates the payment account provisioning process with the provided Pago pay
     * ID and populates a PaymentAccount object.
     *
     * @param payID        - the Pago payID/paystring to provision on the OneStep
     *                     Platform
     * @param callbackPath - the path Pago should call when the user has confirmed
     *                     their transactions
     * @param entityID     - the ID of the person or organization being provisioned
     * @return - the newly provisioned payment account with its current status
     * @throws OrganizationServiceException - thrown if the connection failed
     */
    public PaymentAccount provisionPaymentAccount(String payID, String callbackPath, String entityID)
            throws OrganizationServiceException {
        ProvisionPayResponse payResponse;

        // build and submit payment account provisioning request
        try {
            // build request
            URL url = new URL(systemEnvValues.APP_BASE_URL + systemEnvValues.PROVISION_PAY_USER_PATH);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            // build request body
            ProvisionPayRequest payRequest = new ProvisionPayRequest();
            payRequest.setSender(new ProvisionPayRequest.RequestSender(payID));
            payRequest.setCallbackURL(systemEnvValues.APP_BASE_URL + callbackPath + entityID + "/");
            ObjectMapper objectMapper = new ObjectMapper();
            String body = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(payRequest);

            // write request
            OutputStream os = connection.getOutputStream();
            byte[] input = body.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);

            // read response
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String output;
            StringBuilder response = new StringBuilder();
            while ((output = reader.readLine()) != null) {
                response.append(output);
            }
            reader.close();

            // map to ProvisionPayResponse object
            payResponse = objectMapper.readValue(response.toString(), ProvisionPayResponse.class);
        } catch (Exception e) {
            throw new OrganizationServiceException("Provision Payment Account", "Connection failed");
        }

        // build and return PaymentAccount
        try {
            PaymentAccount account = new PaymentAccount();
            account.setPayID(payID);
            account.setStatus(PaymentAccount.ProvisionStatus.AWAITING_USER_ACTION);
            account.setBlockchainAddress(payResponse.getBlockchainAddress());

            // set PaymentAccount assets
            Map<String, Boolean> assets = new HashMap<>();
            for (ProvisionPayResponse.Asset asset : payResponse.getAssets()) {
                assets.put(asset.getAssetId(), false);
            }
            account.setAssets(assets);

            return account;
        } catch (Exception e) {
            throw new OrganizationServiceException("Provision Payment Account", "Badly formatted response");
        }
    }

}
