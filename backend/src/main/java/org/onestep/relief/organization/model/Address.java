package org.onestep.relief.organization.model;

import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the address of a Person or an Organization. Properties follow the FHIR Address standard. See
 * https://www.hl7.org/FHIR/datatypes.html#address for more details.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Address {

    private String text;
    private List<String> inline = new ArrayList<>();
    private String city;
    private String district;
    private String state;
    private String postalCode;
    private String country;
}
