import { render, screen } from '@testing-library/react'
import App from './App'

describe('App', () => {
  it('shows "One Step" on page', () => {
    render(<App auth={{}} mountPath={undefined} />)
    const oneStepText = screen.getByText(/One Step/i)
    expect(oneStepText).toBeInTheDocument()
  })
})
