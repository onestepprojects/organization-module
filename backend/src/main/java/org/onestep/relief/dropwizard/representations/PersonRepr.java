package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonRepr {
    /*
     * Fields for BasePerson
     */

    // null for creation
    @Size(min = 4)
    private String uuid;

    @NotNull
    private String name;

    @Valid
    private RepAddress currentAddress;

    @Email
    private String email;

    @Size(min = 1)
    private Map<String, String> phones;

    /*
     * Fields for Person
     */

    private String birthdate;
    private FileRepr profilePicture;
    private String lastActivity;
    private List<EventRepr> history;
    private RepAddress homeAddress;
    private String ecUUID;
    private String ecName;
    private Map<String, String> ecPhones;
    private RepAddress ecAddress;
    private PaymentAccountRepr paymentAccount;
    private Map<String, RepLocation> location;
    private String gender;
    private Map<String, String> socialMedia;
}
