package org.onestep.relief.organization.model.DynamoDB;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.google.inject.Inject;
import org.onestep.relief.organization.model.Person;

import java.util.List;

public class PersonDynamoDBService {

    DynamoDBMapper mapper;

    @Inject
    public PersonDynamoDBService(DynamoDBMapperBuilder builder) {
        this.mapper = builder.buildMapper("Person");
    }

    public void save(Object obj) {
        this.mapper.save(obj);
    }

    public void update(Object obj) {
        this.mapper.save(obj);
    }

    public Person load(String personId) {
        return this.mapper.load(Person.class, personId);
    }

    public void delete(String personId) {
        Person person = load(personId);
        if (person != null) {
            this.mapper.delete(person);
        }
    }

    public PaginatedScanList<Person> scan() {
        return this.mapper.scan(Person.class, new DynamoDBScanExpression());
    }

    public PaginatedScanList<Person> scan(DynamoDBScanExpression dynamoDBScanExpression) {
        return this.mapper.scan(Person.class, dynamoDBScanExpression);
    }

    public List<Person> query(DynamoDBQueryExpression<Person> queryExpression) {
        return this.mapper.query(Person.class, queryExpression);
    }
}