import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import type { UploadFile } from 'antd/lib/upload/interface'
import { useState, type FC } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { OrganizationForm, PageTitle } from '../../components'
import {
  useAddSubOrganizationMutation,
  useCreateOrganizationMutation,
  useLazyGetGeographicPositionByAddressQuery,
  type Organization,
} from '../../store'

const initialOrganization = {
  address: { country: 'United States of America' },
} as Organization

export const OrganizationCreate: FC = () => {
  const navigate = useNavigate()
  const { organizationId: parentOrganizationId } = useParams<{ organizationId: string }>()

  const [step, setStep] = useState(1)

  const [getGeographicPositionByAddress] = useLazyGetGeographicPositionByAddressQuery()
  const [createOrganization] = useCreateOrganizationMutation()
  const [addSubOrganization] = useAddSubOrganizationMutation()

  const handleSubmit = useMemoizedFn(async (values: Organization) => {
    try {
      const org = { ...values }

      const uploadLogo = org.logo as unknown as UploadFile
      if (uploadLogo?.thumbUrl) {
        const type = uploadLogo.type.split('/').pop().toUpperCase()
        const format = type === 'JPEG' ? 'JPG' : type
        org.logo = {
          name: uploadLogo.name,
          size: uploadLogo.size,
          format,
          body: uploadLogo.thumbUrl,
          caption: '',
        } as Organization['logo']
      }

      org.statusLabel ??= 'ACTIVE'
      org.description ??= {} as Organization['description']
      org.description.projectTypes ??= ['appear', 'religious']

      const mapData = await getGeographicPositionByAddress(
        [
          org.address.address,
          org.address.city,
          org.address.state,
          org.address.postalCode,
          org.address.country,
        ]
          .filter(Boolean)
          .join(',')
      ).unwrap()
      org.description.serviceArea = mapData ? [mapData] : []

      console.log('OrganizationCreate submit value', org)

      const createOrganizationResponse = await createOrganization(org).unwrap()

      console.log('OrganizationCreate organization created', createOrganizationResponse)

      if (parentOrganizationId) {
        try {
          await addSubOrganization({
            id: parentOrganizationId,
            subOrganizationId: createOrganizationResponse.id,
          }).unwrap()
        } catch (err) {
          // FIXME: The backend return a string response, not json.
          if (err.data === 'DONE') {
            console.log('OrganizationCreate add sub organization')
          }
        }
      }

      navigate(`/organizations/${createOrganizationResponse.id}`)

      message.success('Organization created')
    } catch (err) {
      console.log('OrganizationCreate submit error', err)

      message.error('Organization create failed')
    }
  })

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Organization Creation</PageTitle>

      <Card className='max-w-screen-lg rounded-xl'>
        <OrganizationForm
          initialValues={initialOrganization}
          step={step}
          onStepChange={setStep}
          onFinish={handleSubmit}
        />
      </Card>
    </div>
  )
}
