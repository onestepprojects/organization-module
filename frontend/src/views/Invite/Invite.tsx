import { useMemoizedFn } from 'ahooks'
import { Button, Card, Select, Spin, message } from 'antd'
import { useState, type FC } from 'react'
import { Link, useParams } from 'react-router-dom'
import { PageTitle } from '../../components'
import {
  skipToken,
  useGetAccreditationEntitiesByIdQuery,
  useGetOrganizationQuery,
  useGetPersonsQuery,
  useGetProjectQuery,
  useLazyAddCompositeRoleToEntityQuery,
} from '../../store'

export const Invite: FC = () => {
  const { organizationId, projectId } = useParams<{ organizationId?: string; projectId?: string }>()

  const [personId, setPersonId] = useState('')
  const [role, setRole] = useState('')

  const [isInviting, setIsInviting] = useState(false)

  const { isLoadingPersonOptions, personOptions } = useGetPersonsQuery(undefined, {
    selectFromResult: ({ isLoading, data }) => ({
      isLoadingPersonOptions: isLoading,
      personOptions: data
        ?.map(({ uuid, name }) => ({ label: name, value: uuid }))
        ?.sort((a, b) => a.label?.localeCompare(b.label, undefined, { numeric: true })),
    }),
  })
  const { isLoadingAccreditationOptions, accreditationOptions } =
    useGetAccreditationEntitiesByIdQuery(personId || skipToken, {
      selectFromResult: ({ isLoading, data }) => ({
        isLoadingAccreditationOptions: isLoading,
        accreditationOptions: data?.map(({ role }) => ({
          label: role,
          value: role.toUpperCase(),
        })),
      }),
    })
  const { isLoading: isLoadingOrganization, data: organization } = useGetOrganizationQuery(
    organizationId || skipToken
  )
  const { isLoading: isLoadingProject, data: project } = useGetProjectQuery(projectId || skipToken)

  const [addCompositeRoleToEntity] = useLazyAddCompositeRoleToEntityQuery()

  const handleSubmit = useMemoizedFn(async () => {
    try {
      setIsInviting(true)

      const entityType = (organizationId && 'organization') || 'project'
      const entityId = organizationId || projectId

      await addCompositeRoleToEntity({
        personId,
        role,
        entityType,
        entityId,
      }).unwrap()

      message.success('Invite success')
    } catch (err) {
      console.log('Invite failed', err)
      message.error('Invite failed')
    } finally {
      setIsInviting(false)
    }
  })

  if (isLoadingPersonOptions || isLoadingOrganization || isLoadingProject) {
    return <Spin className='flex justify-center items-center h-96' size='large' />
  }

  return (
    <div>
      <PageTitle>Invite Request</PageTitle>
      <Card className='rounded-xl' title={organization?.name || project?.name}>
        <div className='flex flex-col items-center space-y-8'>
          <div>
            <div>Select a user</div>
            <Select
              style={{ minWidth: 384 }}
              showSearch
              optionFilterProp='label'
              options={personOptions}
              value={personId}
              onChange={(value) => {
                setPersonId(value)
                setRole('')
              }}
            />
          </div>
          <div>
            <div>Select an accreditation</div>
            <Select
              style={{ minWidth: 384 }}
              loading={isLoadingAccreditationOptions}
              options={accreditationOptions}
              value={role}
              onChange={setRole}
            />
          </div>
          <div className='space-x-8'>
            <Link
              to={`/${(organizationId && 'organizations') || 'projects'}/${
                organizationId || projectId
              }`}
            >
              <Button size='large'>Go Back</Button>
            </Link>
            <Button
              type='primary'
              size='large'
              disabled={!personId || !role}
              loading={isInviting}
              onClick={handleSubmit}
            >
              Invite An User
            </Button>
          </div>
        </div>
      </Card>
    </div>
  )
}
