package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Represents the composite relationship between a child object and a parent object. For example: A PersonRole can be
 * the child of an Organization, indicating the role that person has in that organization. Likewise, an organization can
 * be a sub-organization of a parent organization.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "ReliefEntity")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "ReliefEntity", description = "ReliefEntity")
public abstract class ReliefEntity {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    private String parentType; // organization or a project

    @DynamoDBAttribute
    private String parentId; // orgID or projectID

    @DynamoDBAttribute
    private String childType; // org, project, person

    @DynamoDBAttribute
    private String childId; // orgID, projectID, personID
}