package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileRepr {
    private String name;
    private String bucketName;
    private String url;
    private String caption;

    @NotNull
    @Pattern(regexp = "image/jpeg|image/png")
    private String format;

    int size;
    public String creationDate;
    public String creator;
    private String body;
}
