import { Button, Card, Empty } from 'antd'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import { TaskDescriptions, type TaskDescriptionsProps } from '../../components'
import { useGetPersonQuery, type Project, type Task } from '../../store'

export interface ProjectTasksCardProps {
  loading?: boolean
  project: Project
  tasks: Task[]
  isAdmin?: boolean
}

export const ProjectTasksCard: FC<ProjectTasksCardProps> = ({
  loading,
  project,
  tasks,
  isAdmin,
}) => {
  return (
    <Card
      className='rounded-lg'
      title='Project Tasks'
      loading={loading}
      extra={
        isAdmin && (
          <Link to={`/tasks/create?projectId=${project?.id}`}>
            <Button type='primary' ghost icon={<i className='mr-2 fas fa-plus-circle' />}>
              Create
            </Button>
          </Link>
        )
      }
    >
      <div className='space-y-8'>
        {tasks?.length ? (
          tasks?.map((task) => <ProjectTaskDescriptions key={task.id} task={task} />)
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='No tasks' />
        )}
      </div>
    </Card>
  )
}

interface ProjectTaskDescriptionsProps extends Partial<TaskDescriptionsProps> {
  task: Task
}

const ProjectTaskDescriptions: FC<ProjectTaskDescriptionsProps> = ({ task, ...restProps }) => {
  const { data: person } = useGetPersonQuery(task.assignedPerson)

  return <TaskDescriptions task={task} assignedPerson={person} {...restProps} />
}
