import { Button, Result } from 'antd'
import { type FC } from 'react'
import { Link } from 'react-router-dom'

export const NotFound: FC = () => {
  return (
    <Result
      status='404'
      title='Not Found'
      subTitle='Sorry, the page you visited does not exist.'
      extra={
        <Link to='/'>
          <Button type='primary' size='large'>
            Back Home
          </Button>
        </Link>
      }
    />
  )
}
