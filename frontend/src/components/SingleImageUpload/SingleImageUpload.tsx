import { Button, Upload, message } from 'antd'
import { type RcFile, type UploadFile } from 'antd/lib/upload/interface'
import { type FC } from 'react'
import { type Picture } from '../../store/types/common'

interface Props {
  value?: Picture
  onChange?: (value: Picture | undefined) => void
  maxSizeInMb?: number
}

function convertPictureToAntdImage(picture: Picture | undefined): UploadFile | undefined {
  if (!picture) return undefined

  function convertImageFormat(format: Picture['format']): string {
    switch (format) {
      case 'JPG':
        return 'image/jpeg'
      case 'PNG':
        return 'image/png'
      default:
        return format
    }
  }

  return {
    uid: picture.name,
    name: picture.name,
    size: picture.size,
    type: convertImageFormat(picture.format),
    thumbUrl: picture.body || picture.url,
  }
}

async function readAsBase64(originFileObj: RcFile): Promise<string> {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.readAsDataURL(originFileObj)
    reader.onloadend = () => {
      resolve(reader.result?.toString())
    }
  })
}

async function convertAntdImageToPicture(
  uploadFile: UploadFile | undefined
): Promise<Picture | undefined> {
  if (!uploadFile) return undefined

  function convertImageFormat(type: string): Picture['format'] {
    switch (type) {
      case 'image/jpeg':
        return 'JPG'
      case 'image/png':
        return 'PNG'
      default:
        throw new Error(`Invalid image type: "${type}"`)
    }
  }

  return {
    name: uploadFile.name,
    size: uploadFile.size,
    format: convertImageFormat(uploadFile.type),
    body: await readAsBase64(uploadFile.originFileObj),
  }
}

export const SingleImageUpload: FC<Props> = ({ value, onChange, maxSizeInMb = 1 }) => {
  function validateImageSize(file: RcFile) {
    const sizeInMb = file.size / 1024 / 1024
    if (sizeInMb > maxSizeInMb) {
      message.error(`Image size must be <= ${maxSizeInMb}M`)()
    }
    return false
  }

  async function uploadImage(uploadFile: UploadFile) {
    const picture = await convertAntdImageToPicture(uploadFile)
    onChange?.(picture)
  }

  return (
    <Upload
      fileList={[convertPictureToAntdImage(value)].filter(Boolean)}
      accept='image/jpeg, image/png'
      listType='picture'
      maxCount={1}
      beforeUpload={(file) => validateImageSize(file)}
      onChange={(e) => uploadImage(e?.fileList[0])}
    >
      <Button>Click to upload</Button>
    </Upload>
  )
}
