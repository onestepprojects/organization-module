import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import { type FC } from 'react'

import { PageTitle, PersonForm } from '../../components'
import { useCreatePersonMutation } from '../../store'

export const PersonCreate: FC = () => {
  const [createPerson] = useCreatePersonMutation()

  const handleSubmit = useMemoizedFn(async (values) => {
    try {
      console.log('Create person', values)
      await createPerson(values).unwrap()
      location.href = '/'
    } catch (err) {
      console.log('Create person failed', err)
      message.error('Create profile failed')
    }
  })

  return (
    <div>
      <PageTitle caption='CREATE ONE STEP PROFILE'>Member</PageTitle>

      <Card className='rounded-xl' title='Create'>
        <PersonForm submitButtonText='Create Profile' onFinish={handleSubmit} />
      </Card>
    </div>
  )
}
