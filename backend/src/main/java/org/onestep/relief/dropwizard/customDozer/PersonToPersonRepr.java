package org.onestep.relief.dropwizard.customDozer;

import org.onestep.relief.dropwizard.representations.PersonRepr;

import org.dozer.CustomConverter;
import org.onestep.relief.organization.model.NonMemberPerson;
import org.onestep.relief.organization.model.Person;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class PersonToPersonRepr implements CustomConverter {
    @Override
    public Object convert(Object dest, Object source, Class<?> aClass, Class<?> aClass1) {
        if (source == null)
            return null;

        /*
         * PersonRepr -> Person
         */
        if (source instanceof PersonRepr) {
            PersonRepr rep = (PersonRepr) source;
            Person person = new Person();

            // BASE PERSON
            person.setId(rep.getUuid());
            person.setName(rep.getName());
            person.setCurrentAddress(Utils.toAddress(rep.getCurrentAddress()));
            person.setEmail(rep.getEmail());
            person.setPhones(rep.getPhones());

            person.setBirthdate(parseBirthdate(rep.getBirthdate()));
            person.setProfilePicture(Utils.toFromFileObj(rep.getProfilePicture()));

            person.setLastActivity(Utils.timestampToDate(rep.getLastActivity()));
            person.setHomeAddress(Utils.toAddress(rep.getHomeAddress()));

            person.setEmergencyContact(toEmergencyContact(rep));
            person.setPaymentAccount(Utils.toFromPaymentAccount(rep.getPaymentAccount()));

            person.setLocation((rep.getLocation() != null)
                    ? rep.getLocation().entrySet().stream()
                            .collect(Collectors.toMap(Map.Entry::getKey, e -> Utils.toLocation(e.getValue())))
                    : new HashMap<>());
            person.setGender(Person.Gender.valueOf(rep.getGender().toUpperCase()));
            person.setSocialMedia(rep.getSocialMedia());

            return person;

            /*
             * Person -> PersonRepr
             */
        } else if (source instanceof Person) {
            Person p = (Person) source;
            PersonRepr personRepr = new PersonRepr();

            personRepr.setUuid(p.getId());
            personRepr.setName(p.getName());
            personRepr.setCurrentAddress(Utils.toAddress(p.getCurrentAddress()));

            personRepr.setEmail(p.getEmail());
            personRepr.setPhones(p.getPhones());

            personRepr.setBirthdate(parseBirthdate(p.getBirthdate()));
            personRepr.setProfilePicture(Utils.toFromFileObj(p.getProfilePicture()));

            personRepr.setLastActivity(getTimeStamp(p.getLastActivity()));
            personRepr.setHistory(
                    p.getHistory().stream().map(EventToEventRepr::toEventRepresentation).collect(Collectors.toList()));
            personRepr.setHomeAddress(Utils.toAddress(p.getHomeAddress()));
            personRepr.setPaymentAccount(Utils.toFromPaymentAccount(p.getPaymentAccount()));

            if (p.getEmergencyContact() != null) {
                personRepr.setEcUUID(p.getEmergencyContact().getId());
                personRepr.setEcName(p.getEmergencyContact().getName());
                personRepr.setEcPhones(p.getEmergencyContact().getPhones());
                personRepr.setEcAddress(Utils.toAddress(p.getEmergencyContact().getCurrentAddress()));
            }

            // personRepr.setPaymentAccount(p.getPaymentAccount());
            personRepr.setLocation((p.getLocation() != null)
                    ? p.getLocation().entrySet().stream()
                            .collect(Collectors.toMap(Map.Entry::getKey, e -> Utils.toLocation(e.getValue())))
                    : new HashMap<>());
            personRepr.setGender(p.getGender().toString());
            personRepr.setSocialMedia(p.getSocialMedia());

            return personRepr;
        } else
            return null;
    }

    private Date parseBirthdate(String birthdate) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            return format.parse(birthdate);

        } catch (ParseException e) {
            return new Date();
        }
    }

    private String parseBirthdate(Date birthdate) {
        if (birthdate == null) {
            return null;
        } else {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            return format.format(birthdate);
        }
    }

    private NonMemberPerson toEmergencyContact(PersonRepr rep) {
        NonMemberPerson person = new NonMemberPerson();

        String uuid = UUID.randomUUID().toString();
        if (rep.getEcUUID() != null)
            uuid = rep.getEcUUID();

        person.setId(uuid);
        person.setName(rep.getEcName());
        person.setCurrentAddress(Utils.toAddress(rep.getEcAddress()));
        person.setPhones(rep.getEcPhones());

        return person;
    }

    private String getTimeStamp(Date date) {
        if (date == null)
            return null;

        Timestamp ts = new Timestamp(date.getTime());
        return String.valueOf(ts.getTime());
    }
}
