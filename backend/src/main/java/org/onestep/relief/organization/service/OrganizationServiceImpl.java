package org.onestep.relief.organization.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import org.onestep.relief.dropwizard.SystemEnvValues;
import org.onestep.relief.organization.model.*;
import org.onestep.relief.organization.model.DynamoDB.*;
import org.onestep.relief.organization.model.S3.S3Service;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service API for the organization module.
 */
public class OrganizationServiceImpl implements OrgServiceInterface {
    private final SystemEnvValues systemEnvValues;

    // testing configuration
    private final String AUTH_TOKEN_TESTING_URL = "http://localhost:8080/tests/cognito-id";
    public boolean testingMode = false;

    // DynamoDB services for each main object
    private final OrganizationDynamoDBService orgDynamoDBService;
    private final PersonDynamoDBService personDynamoDBService;
    private final ProjectDynamoDBService projectDynamoDBService;
    private final CommunityDynamoDBService communityDynamoDBService;
    private final TaskDynamoDBService taskDynamoDBService;
    private final ReliefEntityDynamoDBService reliefEntityDynamoDBService;
    private CognitoService cognitoService;
    private RewardService rewardService;
    private HealthProjectService healthProjectService;
    private PaymentService paymentService;

    // S3 service
    private final S3Service s3Service;

    @Inject
    public OrganizationServiceImpl(SystemEnvValues systemEnvValues,
            OrganizationDynamoDBService organizationDynamoDBService, PersonDynamoDBService personDynamoDBService,
            ProjectDynamoDBService projectDynamoDBService, CommunityDynamoDBService communityDynamoDBService,
            TaskDynamoDBService taskDynamoDBService, ReliefEntityDynamoDBService reliefEntityDynamoDBService,
            S3Service s3Service, CognitoService cognitoService, RewardService rewardService,
            ServiceTokenService serviceTokenService, HealthProjectService healthProjectService,
            PaymentService paymentService) {
        this.systemEnvValues = systemEnvValues;
        this.orgDynamoDBService = organizationDynamoDBService;
        this.personDynamoDBService = personDynamoDBService;
        this.projectDynamoDBService = projectDynamoDBService;
        this.communityDynamoDBService = communityDynamoDBService;
        this.taskDynamoDBService = taskDynamoDBService;
        this.reliefEntityDynamoDBService = reliefEntityDynamoDBService;
        this.s3Service = s3Service;
        this.cognitoService = cognitoService;
        this.rewardService = rewardService;
        this.healthProjectService = healthProjectService;
        this.paymentService = paymentService;
    }

    /**
     * Adds organization to the platform
     *
     * @param org - organization to be added
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Organization createOrganization(String authToken, Organization org)
            throws OrganizationServiceException, AuthenticationException {
        if (org.getId() != null && orgExists(org.getId())) {
            throw new OrganizationServiceException("Create Organization", "Organization already exists");
        }

        CognitoUser user = identifyAuthToken(authToken);

        // create the organization
        org.setCreator(user.getPersonId());

        // provision the organization's payment account if applicable
        PaymentAccount account = org.getPaymentAccount();
        if (account != null && account.getPayID() != null) {
            String payID = account.getPayID();
            try {
                account = paymentService.provisionPaymentAccount(payID, systemEnvValues.PAGO_ORG_CALLBACK_PATH,
                        org.getId());
                org.setPaymentAccount(account);
            } catch (OrganizationServiceException e) {
                org.getPaymentAccount().setStatus(PaymentAccount.ProvisionStatus.FAILED);
            }
        }

        // store image in S3 if applicable; otherwise use default
        File image = org.getLogo();
        if (image != null && image.getBody() != null) {
            uploadImage(image, systemEnvValues.ORG_LOGO_BUCKET, org.getId());
        } else {
            image = new File();
            image.setUrl(systemEnvValues.ORG_PIC_DEFAULT);
        }
        org.setLogo(image);

        // save organization changes
        orgDynamoDBService.save(org);

        // grant the requester an admin role in the organization
        Date creationDate = Calendar.getInstance().getTime();
        PersonRole personRole = new PersonRole(creationDate, Role.ADMIN, user.getPersonId(),
                PersonRole.RequestStatus.ACTIVE);
        addReliefEntity(personRole, "Organization", org.getId(), "Person", personRole.getPersonId());

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.ORG_CREATED, user.getPersonId());

        // return the created organization
        return org;
    }

    /**
     * Updates the organization information
     *
     * @param org - new updated organization object to replace the old one
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Organization updateOrganization(String authToken, Organization org)
            throws OrganizationServiceException, AuthenticationException {
        if (!orgExists(org.getId())) {
            throw new OrganizationServiceException("Update Organization", "Such organization does not exist");
        }

        String personID = verifyPrivileges(org.getId(), authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Update Organization", "Authentication Failed");
        }

        // retrieve the Organization's existing logo from DynamoDB so that it is not
        // overwritten
        Organization oldOrg = orgDynamoDBService.load(org.getId());

        // store image in S3 if applicable; otherwise use default
        File image = org.getLogo();
        File oldLogo = oldOrg.getLogo();
        if (image != null) {
            if (image.getBody() != null) {
                uploadImage(image, systemEnvValues.ORG_LOGO_BUCKET, org.getId());
                org.setLogo(image);
            } else {
                org.setLogo(oldOrg.getLogo());
                oldLogo = null;
            }
        } else {
            image = new File();
            image.setUrl(systemEnvValues.ORG_PIC_DEFAULT);
            org.setLogo(image);
        }

        // check for a new payID
        String newPayID = null;
        if (org.getPaymentAccount() != null) {
            newPayID = org.getPaymentAccount().getPayID();
        }

        // check for an old payID
        String oldPayID = null;
        if (oldOrg.getPaymentAccount() != null) {
            oldPayID = oldOrg.getPaymentAccount().getPayID();
        }

        // re-provision payment account if the payID has changed
        if (newPayID != null && !newPayID.equals(oldPayID)) {
            PaymentAccount payAccount;
            try {
                payAccount = paymentService.provisionPaymentAccount(newPayID, systemEnvValues.PAGO_ORG_CALLBACK_PATH,
                        org.getId());
                org.setPaymentAccount(payAccount);
            } catch (OrganizationServiceException e) {
                org.getPaymentAccount().setStatus(PaymentAccount.ProvisionStatus.FAILED);
            }
        }

        orgDynamoDBService.save(org);

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.ORG_UPDATED, personID);

        try {
            if (oldLogo.getUrl() != systemEnvValues.ORG_PIC_DEFAULT) {
                // delete the old logo from S3 except it is the default picture.
                s3Service.delete(oldLogo.getBucketName(), oldLogo.getName());
            }
        } catch (Exception e) {
            System.out.println("Error deleting organization old logo");
        }

        return org;
    }

    /**
     * Overwrites the organization's current logo with the provided image
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param orgID     - DynamoDB organization ID
     * @param image     - new organization logo
     * @return - the updated organization
     * @throws OrganizationServiceException - thrown due to a data or authorization
     *                                      error
     * @throws AuthenticationException      - thrown due to an authentication error
     */
    @Override
    public Organization updateOrganizationImage(String authToken, String orgID, File image)
            throws OrganizationServiceException, AuthenticationException {
        Organization org = orgDynamoDBService.load(orgID);
        if (org == null) {
            throw new OrganizationServiceException("Update Organization Image", "Such organization does not exist");
        }

        String creator = verifyPrivileges(orgID, authToken, Role.ADMIN);
        if (creator == null) {
            throw new OrganizationServiceException("Update Organization Image", "Authorization Failed");
        }

        // store image in S3
        uploadImage(image, systemEnvValues.ORG_LOGO_BUCKET, creator);
        org.setLogo(image);

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.ORG_UPDATED, creator);

        orgDynamoDBService.save(org);
        return org;
    }

    /**
     * Callback method for Pago to confirm that the user has confirmed the
     * transaction for the specified asset.
     *
     * @param orgID   - DynamoDB organization ID
     * @param assetID - reward service asset ID
     * @throws OrganizationServiceException - thrown due to a data error
     */
    @Override
    public void provisionOrganizationAsset(String orgID, String assetID) throws OrganizationServiceException {
        Organization org = orgDynamoDBService.load(orgID);
        if (org == null) {
            throw new OrganizationServiceException("Provision Organization Payment Asset",
                    "Such organization does not exist");
        }

        try {
            org.getPaymentAccount().provisionAsset(assetID);
        } catch (NoSuchElementException e) {
            throw new OrganizationServiceException("Provision Organization Payment Asset",
                    "Payment account does not have such asset");
        }

        orgDynamoDBService.save(org);
    }

    /**
     * Deletes the organization from the platform
     *
     * @param orgId - ID of the organization to be deleted
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public void deleteOrganization(String authToken, String orgId)
            throws OrganizationServiceException, AuthenticationException {
        if (!orgExists(orgId)) {
            throw new OrganizationServiceException("Delete Organization", "Such organization does not exist");
        }
        if (verifyPrivileges(orgId, authToken, Role.ADMIN) == null) {
            throw new OrganizationServiceException("Delete Organization", "Authentication Failed");
        }
        Organization org = orgDynamoDBService.load(orgId);
        org.setActive(false);
        orgDynamoDBService.save(org);
    }

    /**
     * Returns specific organization
     *
     * @param organizationID - Organization id to be retrieved
     * @return - Requested organization object
     * @throws OrganizationServiceException - Organization Service Exception
     */
    @Override
    public Organization getOrganizationById(String authToken, String organizationID)
            throws OrganizationServiceException {
        Organization org = orgDynamoDBService.load(organizationID);
        if (org == null) {
            throw new OrganizationServiceException("Get Organization by ID", "Such organization does not exist");
        } else {
            org.setRequesterRoles(getRequesterRoles(organizationID, authToken));
            return org;
        }
    }

    /**
     * @return List of all organizations
     */
    @Override
    public List<Organization> getAllOrganizations() {
        List<Organization> organizations = orgDynamoDBService.scan();
        return organizations.stream().filter(Organization::isActive).collect(Collectors.toList());
    }

    /**
     * Retrieves the root level of organizations, used for build a tree.
     */
    @Override
    public List<Organization> getRootOrganizations() {
        List<Organization> organizations = orgDynamoDBService.scan();
        return organizations.stream()
                .filter(Organization::isActive)
                .filter(org -> this.queryByChildIdParentType(org.getId(), "Organization").isEmpty())
                .collect(Collectors.toList());
    }

    /**
     * Retrieves the parent organization of the project or a sub-organization
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param childId   - child entity id (sub org or project)
     * @return - parent organization
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Organization getParentOrganization(String authToken, String childId) throws OrganizationServiceException {
        List<ReliefComposite> queryResult = queryByChildIdParentType(childId, "Organization");
        if (queryResult.isEmpty()) {
            throw new OrganizationServiceException("getParentOrganization", "No parent organization found");
        }
        // retrieve the organization
        Organization org = orgDynamoDBService.load(queryResult.get(0).getParentId());

        // FIXME should not throw exception since it's possible for a organization has
        // no parent
        if (!orgExists(org.getId())) {
            throw new OrganizationServiceException("getParentOrganization", "No parent organization found");
        }
        org.setRequesterRoles(getRequesterRoles(org.getId(), authToken));
        return org;
    }

    /**
     * List of sub-organizations under the parent organization
     *
     * @param parentId - parent organization id
     * @return - list of sub organizations
     */
    @Override
    public List<Organization> getSubOrganizations(String parentId) {
        List<ReliefComposite> subOrgIDs = queryByParentIdChildType(parentId, "Organization");
        List<Organization> subOrgs = new ArrayList<>();
        if (!subOrgIDs.isEmpty()) {
            // given retrieved organization ids, find organizations and load the subOrg list
            for (ReliefComposite r : subOrgIDs) {
                if (orgExists(r.getChildId())) {
                    subOrgs.add(orgDynamoDBService.load(r.getChildId()));
                }
            }
        }
        return subOrgs;
    }

    /**
     * Retrieves the list of organizations person works with
     *
     * @param personID - person id
     * @return - list of organizations
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public List<Organization> getOrganizationsByPerson(String personID) throws OrganizationServiceException {
        if (!personExists(personID)) {
            throw new OrganizationServiceException("Get Organizations by Person", "Such Person does not exist");
        }
        List<ReliefComposite> queryResult = queryByChildIdParentType(personID, "Organization");
        List<Organization> orgs = new ArrayList<>();

        if (!queryResult.isEmpty()) {
            // given retrieved ids, find organization objects and load them in the list
            for (ReliefComposite r : queryResult) {
                if (orgExists(r.getParentId())) {
                    orgs.add(orgDynamoDBService.load(r.getParentId()));
                }
            }
        }
        return orgs;
    }

    /**
     * Provides the list of organizations and person's roles within those
     * organizations
     *
     * @param personID - person id
     * @return - map < Organization, List of Person's roles >
     * @throws OrganizationServiceException - thrown due to a data error
     */
    @Override
    public Map<Organization, Set<Role>> getOrganizationRolesByPerson(String personID)
            throws OrganizationServiceException {
        if (!personExists(personID)) {
            throw new OrganizationServiceException("Get Organization Roles by Person", "Such Person does not exist");
        }

        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":childId", new AttributeValue().withS(personID));
        eav.put(":parentType", new AttributeValue().withS("Organization"));
        eav.put(":requestStatus", new AttributeValue().withS("ACTIVE"));
        DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                .withIndexName("childId-index")
                .withKeyConditionExpression("childId = :childId")
                .withFilterExpression("parentType = :parentType and requestStatus = :requestStatus")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        List<PersonRole> queryResult = reliefEntityDynamoDBService.queryPersonRole(queryExpression);

        // if no roles found then return empty map of Organizations
        Map<Organization, Set<Role>> orgMap = new HashMap<>();
        if (queryResult.isEmpty()) {
            return orgMap;
        }

        // otherwise fill map with Organizations and the Person's Roles
        for (PersonRole role : queryResult) {
            Organization org = orgDynamoDBService.load(role.getParentId());
            // skip Organization if it has been deactivated or does not exist
            if (org == null || !org.isActive()) {
                continue;
            }

            // otherwise capture their Role in the map
            if (orgMap.containsKey(org)) {
                Set<Role> personRoles = orgMap.get(org);
                personRoles.add(role.getReliefRole());
            } else {
                Set<Role> roles = new HashSet<>();
                roles.add(role.getReliefRole());
                orgMap.put(org, roles);
            }
        }
        return orgMap;
    }

    /**
     * Adds person to the platform
     *
     * @param person - a person to be added
     */
    @Override
    public Person createPerson(String authToken, Person person)
            throws OrganizationServiceException, AuthenticationException {
        if (person.getId() != null && personExists(person.getId())) {
            throw new OrganizationServiceException("Create Person", "Person already exists");
        }

        // if the Person is not a NonMemberPerson look up and store their CognitoID
        if (person.getBirthdate() != null) {
            // check that no existing Person already has the same CognitoID
            CognitoUser user = getCognitoUser(authToken);
            String cognitoID = user.getData().getUserId();
            List<Person> queryResult = queryCognitoID(cognitoID);

            // store if Person not found; otherwise throw exception
            if (queryResult.isEmpty()) {
                person.setCognitoID(cognitoID);

                // grant their cognito user a user role
                cognitoService.bindCognitoRole(user.getData().getUsername(), "user");
            } else {
                throw new AuthenticationException("Create Person", "Authentication failed");
            }
        }

        // provision the person's payment account if applicable
        PaymentAccount account = person.getPaymentAccount();
        if (account != null && account.getPayID() != null) {
            String payID = account.getPayID();
            try {
                account = paymentService.provisionPaymentAccount(payID, systemEnvValues.PAGO_PERSON_CALLBACK_PATH,
                        person.getId());
                person.setPaymentAccount(account);
            } catch (OrganizationServiceException e) {
                person.getPaymentAccount().setStatus(PaymentAccount.ProvisionStatus.FAILED);
            }
        }

        // store image in S3 if applicable; otherwise use default
        File image = person.getProfilePicture();
        if (image != null && image.getBody() != null) {
            uploadImage(image, systemEnvValues.PERSON_PIC_BUCKET, person.getId());
        } else {
            image = new File();
            image.setUrl(systemEnvValues.PERSON_PIC_DEFAULT);
        }
        person.setProfilePicture(image);

        // save person changes
        personDynamoDBService.save(person);

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PERSON_CREATED, person.getId());

        return person;
    }

    /**
     * Updates the person details
     *
     * @param person - new person object with updated details to replace the old one
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Person updatePerson(String authToken, Person person)
            throws OrganizationServiceException, AuthenticationException {
        if (person.getId() == null || !personExists(person.getId())) {
            throw new OrganizationServiceException("Update Person", "Such Person does not exist");
        }

        CognitoUser user = identifyAuthToken(authToken);
        if (!user.getPersonId().equals(person.getId()) && !user.isGlobalAdmin()) {
            throw new OrganizationServiceException("Update Person",
                    "Person can only update oneself, permission denied");
        }

        // retrieve and maintain key data from DynamoDB that cannot be updated directly
        // using this method
        Person oldPerson = personDynamoDBService.load(person.getId());
        person.setCognitoID(oldPerson.getCognitoID());
        person.setHistory(oldPerson.getHistory());

        // check for a new payID
        String newPayID = null;
        if (person.getPaymentAccount() != null) {
            newPayID = person.getPaymentAccount().getPayID();
        }

        // check for an old payID
        String oldPayID = null;
        if (oldPerson.getPaymentAccount() != null) {
            oldPayID = oldPerson.getPaymentAccount().getPayID();
        }

        // re-provision payment account if the payID has changed
        if (newPayID != null && !newPayID.equals(oldPayID)) {
            PaymentAccount payAccount;
            try {
                payAccount = paymentService.provisionPaymentAccount(newPayID, systemEnvValues.PAGO_PERSON_CALLBACK_PATH,
                        person.getId());
                person.setPaymentAccount(payAccount);
            } catch (OrganizationServiceException e) {
                person.getPaymentAccount().setStatus(PaymentAccount.ProvisionStatus.FAILED);
            }
        }

        // store image in S3 if applicable; otherwise use default
        File image = person.getProfilePicture();
        File oldProfilePicture = oldPerson.getProfilePicture();
        if (image != null) {
            if (image.getBody() != null) {
                uploadImage(image, systemEnvValues.PERSON_PIC_BUCKET, person.getId());
                person.setProfilePicture(image);
            } else {
                person.setProfilePicture(oldProfilePicture);
                oldProfilePicture = null;
            }
        } else {
            image = new File();
            image.setUrl(systemEnvValues.PERSON_PIC_DEFAULT);
            person.setProfilePicture(image);
        }

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PERSON_UPDATED, user.getPersonId());

        // reset the Person's isRequesterSelf boolean so that it does not get stored in
        // DynamoDB
        person.setRequesterSelf(false);

        personDynamoDBService.save(person);

        try {
            if (oldProfilePicture.getUrl() != systemEnvValues.PERSON_PIC_DEFAULT) {
                // delete the old profile picture from S3 except it is the default picture.
                s3Service.delete(oldProfilePicture.getBucketName(), oldProfilePicture.getName());
            }
        } catch (Exception e) {
            System.out.println("Error deleting person old profile picture");
        }

        return person;
    }

    /**
     * Overwrites the person's current profile picture with the provided image
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param personID  - the person whose profile picture should be changed
     * @param image     - the new profile picture
     * @return - the updated person object
     * @throws OrganizationServiceException - thrown due to a data or authorization
     *                                      error
     * @throws AuthenticationException      - thrown due to an authentication error
     */
    @Override
    public Person updatePersonImage(String authToken, String personID, File image)
            throws OrganizationServiceException, AuthenticationException {
        Person person = personDynamoDBService.load(personID);
        if (person == null) {
            throw new OrganizationServiceException("Update Person Image", "Such person does not exist");
        }

        CognitoUser user = identifyAuthToken(authToken);
        if (!user.getPersonId().equals(personID) && !user.isGlobalAdmin()) {
            throw new OrganizationServiceException("Update Person Image",
                    "Person can only update oneself, permission denied");
        }

        // store image in S3
        uploadImage(image, systemEnvValues.PERSON_PIC_BUCKET, user.getPersonId());
        person.setProfilePicture(image);

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PERSON_UPDATED, user.getPersonId());

        personDynamoDBService.save(person);
        return person;
    }

    /**
     * Callback method for Pago to confirm that the user has confirmed the
     * transaction for the specified asset.
     *
     * @param personID - the person whose asset should be provisioned
     * @param assetID  - the id of the asset to be provisioned
     * @throws OrganizationServiceException - thrown due to a data error
     */
    public void provisionPersonAsset(String personID, String assetID) throws OrganizationServiceException {
        Person person = personDynamoDBService.load(personID);
        if (person == null) {
            throw new OrganizationServiceException("Provision Person Payment Asset", "Such person does not exist");
        }

        try {
            person.getPaymentAccount().provisionAsset(assetID);
        } catch (NoSuchElementException e) {
            throw new OrganizationServiceException("Provision Person Payment Asset",
                    "Payment account does not have such asset");
        }

        personDynamoDBService.save(person);
    }

    /**
     * Deletes person from the platform
     *
     * @param id - id of the person to be deleted
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public void deletePerson(String authToken, String id) throws OrganizationServiceException, AuthenticationException {
        if (!personExists(id)) {
            throw new OrganizationServiceException("Update Person", "Such Person does not exist");
        }

        CognitoUser user = identifyAuthToken(authToken);
        if (!user.getPersonId().equals(id) && !user.isGlobalAdmin()) {
            throw new OrganizationServiceException("Delete Person",
                    "Person can only delete oneself, permission denied");
        }

        Person person = personDynamoDBService.load(id);
        person.setActive(false);
        personDynamoDBService.save(person);
    }

    /**
     * Retrieves a specific person
     *
     * @param personID - Person id to be retrieved
     * @return - Requested person object
     * @throws OrganizationServiceException - thrown due to a data error
     */
    @Override
    public Person getPersonById(String authToken, String personID) throws OrganizationServiceException {
        Person person = personDynamoDBService.load(personID);
        if (person == null || !personExists(personID)) {
            throw new OrganizationServiceException("Get Person by ID", "Such person does not exist");
        }

        // build the person's history of activities on the platform based on rewards
        // they've received
        try {
            List<RewardsResponse.Reward> rewards = rewardService.getRewards(personID);
            List<Event> history = new ArrayList<>();
            for (RewardsResponse.Reward reward : rewards) {
                Event event = new Event();
                event.setDate(reward.getDate());
                event.setMessage(reward.getReason());
                event.setRewardPoints(reward.getAmount());
                history.add(event);
            }

            // save the newly-retrieved reward history
            person.setHistory(history);
            personDynamoDBService.save(person);
        } catch (OrganizationServiceException e) {
            // just use the previously saved activities
        }

        // if requester is not authenticated then just return the person
        CognitoUser user;
        try {
            user = identifyAuthToken(authToken);
        } catch (AuthenticationException e) {
            return person;
        }

        // if the requester is the person then indicate and then return the person
        if (user.getPersonId().equals(personID)) {
            person.setRequesterSelf(true);
        }

        return person;
    }

    /**
     * Retrieves a specific person based on their bound Cognito ID
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param cognitoID - the cognito ID to search for
     * @return - the identified person
     * @throws OrganizationServiceException - thrown due to a data error
     * @throws AuthenticationException      - thrown due to an authentication error
     */
    @Override
    public Person getPersonByCognitoId(String authToken, String cognitoID)
            throws OrganizationServiceException, AuthenticationException {
        // confirm that the requester is a global admin
        CognitoUser user = getCognitoUser(authToken);
        if (!user.isGlobalAdmin()) {
            throw new AuthenticationException("Get Person by Cognito ID", "Authentication failed");
        }

        List<Person> queryResult = queryCognitoID(cognitoID);
        if (queryResult.isEmpty()) {
            throw new OrganizationServiceException("Get Person by Cognito ID", "Such person does not exist");
        } else {
            String personID = queryResult.get(0).getId();
            return personDynamoDBService.load(personID);
        }
    }

    /**
     * @return List of all people in the system
     */
    @Override
    public List<Person> getAllPersons() {
        // return only active people
        List<Person> persons = personDynamoDBService.scan();
        return persons.stream().filter(BasePerson::isActive).collect(Collectors.toList());
    }

    /**
     * List of people and their within the certain parent ReliefEntity
     *
     * @param parentID - Relief entity id (organization or project)
     * @return - List of people
     * @throws OrganizationServiceException - thrown due to a data error
     */
    public Map<Person, Set<Role>> getPersonsByParent(String parentID) throws OrganizationServiceException {
        if (!orgExists(parentID) && !projectExists(parentID) && !communityExists(parentID)) {
            throw new OrganizationServiceException("Get Persons by Parent", "Such parent does not exist");
        }

        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":parentId", new AttributeValue().withS(parentID));
        eav.put(":childType", new AttributeValue().withS("Person"));
        DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                .withIndexName("parentId-index")
                .withKeyConditionExpression("parentId = :parentId")
                .withFilterExpression("childType = :childType")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        List<PersonRole> queryResult = reliefEntityDynamoDBService.queryPersonRole(queryExpression);

        // if no roles found then return empty map of Persons
        Map<Person, Set<Role>> personsMap = new HashMap<>();
        if (queryResult.isEmpty()) {
            return personsMap;
        }

        // otherwise fill map with Persons and their Roles
        for (PersonRole role : queryResult) {
            Person person = personDynamoDBService.load(role.getChildId());
            // skip Person if they have been deactivated or don't exist
            if (person == null || !person.isActive()) {
                continue;
            }

            // otherwise capture their Role in the map
            if (personsMap.containsKey(person)) {
                Set<Role> personRoles = personsMap.get(person);
                personRoles.add(role.getReliefRole());
            } else {
                Set<Role> roles = new HashSet<>();
                roles.add(role.getReliefRole());
                personsMap.put(person, roles);
            }
        }
        return personsMap;
    }

    /**
     * Identifies the person making a request
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @return - Person associated with the request
     * @throws AuthenticationException      - authentication exception
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Person getRequesterPerson(String authToken) throws AuthenticationException, OrganizationServiceException {
        String personID = identifyAuthToken(authToken).getPersonId();
        if (personID == null) {
            throw new AuthenticationException("Get Requester Person", "Authentication failed");
        } else {
            return getPersonById(authToken, personID);
        }
    }

    /**
     * Creates the event and adds to person's history
     *
     * @param authToken  - requester's auth token; includes "Bearer " prefix
     * @param personID   - person id
     * @param activity   - event activity
     * @param message    - event message
     * @param parentType - relief entity type (organization or project)
     * @param parentId   - relief entity id
     * @return - created event
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Event createEvent(String authToken, String personID, Event.Activity activity, String message,
            String parentType, String parentId) throws OrganizationServiceException {
        Date date = Calendar.getInstance().getTime();
        Event event = new Event(date, message, activity, parentId, parentType);
        Person person = getPersonById(authToken, personID);
        person.getHistory().add(event);
        personDynamoDBService.save(person);
        return event;
    }

    /**
     * creates a new project in Project table and captures the Project-Organization
     * relationship in ReliefEntity table
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param parentId  - project's parent org
     * @param project   - project to be created
     * @return - crated project
     * @throws OrganizationServiceException - service exception
     */
    @Override
    public Project createProject(String authToken, String parentId, String parentType, Project project)
            throws OrganizationServiceException, AuthenticationException {
        String personID = verifyPrivileges(parentId, authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Create Project", "Authentication Failed");
        }

        if (!orgExists(parentId) && !projectExists(parentId)) {
            throw new OrganizationServiceException("Create Project", "parent does not exist");
        }

        if (!parentType.equalsIgnoreCase("Organization") && !parentType.equalsIgnoreCase("Project")) {
            throw new OrganizationServiceException("Create Project", "no such type of parent");
        }

        // create the project
        Date creationDate = Calendar.getInstance().getTime();

        // store image in S3 if applicable; otherwise use default
        File image = project.getProfilePicture();
        if (image != null && image.getBody() != null) {
            uploadImage(image, systemEnvValues.PROJECT_PIC_BUCKET, project.getId());
        } else {
            image = new File();
            image.setUrl(systemEnvValues.PROJECT_PIC_DEFAULT);
        }
        project.setProfilePicture(image);

        // save project changes
        projectDynamoDBService.save(project);

        // make project child of the parent in relief entity table
        ReliefEntity reliefEntity = new ReliefComposite();
        if (parentType.equalsIgnoreCase("Organization")) {
            addReliefEntity(reliefEntity, "Organization", parentId, "Project", project.getId());
        } else if (parentType.equalsIgnoreCase("Project")) {
            addReliefEntity(reliefEntity, "Project", parentId, "Project", project.getId());
        }

        // add project creator as an admin of the project
        PersonRole personRole = new PersonRole(creationDate, Role.ADMIN, personID, PersonRole.RequestStatus.ACTIVE);
        addReliefEntity(personRole, "Project", project.getId(), "Person", personRole.getPersonId());

        // notify the Health Module
        if (project.isHealth()) {
            healthProjectService.sendHealthProject(authToken, "POST", "", project.getId());
        }

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PROJECT_CREATED, personID);

        return project;
    }

    /**
     * Updates the already existing project
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param project   - updated project object
     * @return - updated project
     * @throws OrganizationServiceException - service exception
     */
    @Override
    public Project updateProject(String authToken, Project project)
            throws OrganizationServiceException, AuthenticationException {
        if (!projectExists(project.getId())) {
            throw new OrganizationServiceException("Update Project", "Such project does not exist");
        }

        String personID = verifyPrivileges(project.getId(), authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Update Project", "Authentication Failed");
        }

        // retrieve the Project's existing logo from DynamoDB so that it is not
        // overwritten
        Project oldProject = projectDynamoDBService.load(project.getId());

        // store image in S3 if applicable; otherwise use default
        File image = project.getProfilePicture();
        File oldProfilePicture = oldProject.getProfilePicture();
        if (image != null) {
            if (image.getBody() != null) {
                uploadImage(image, systemEnvValues.PROJECT_PIC_BUCKET, project.getId());
                project.setProfilePicture(image);
            } else {
                project.setProfilePicture(oldProject.getProfilePicture());
                oldProfilePicture = null;
            }
        } else {
            image = new File();
            image.setUrl(systemEnvValues.PROJECT_PIC_DEFAULT);
            project.setProfilePicture(image);
        }

        projectDynamoDBService.save(project);

        // if a project became a health project - create a health project in health
        // module
        if (!oldProject.isHealth() && project.isHealth()) {
            healthProjectService.sendHealthProject(authToken, "POST", "", project.getId());
        } else if (oldProject.isHealth() && project.isHealth()) {
            healthProjectService.sendHealthProject(authToken, "PUT", "/" + project.getId(), project.getId());
        }

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PROJECT_UPDATED, personID);

        try {
            if (oldProfilePicture.getUrl() != systemEnvValues.PROJECT_PIC_DEFAULT) {
                // delete the old profile picture from S3 except it is the default picture.
                s3Service.delete(oldProfilePicture.getBucketName(), oldProfilePicture.getName());
            }
        } catch (Exception e) {
            System.out.println("Error deleting project old profile picture");
        }

        return project;
    }

    /**
     * Overwrites the project's current logo with the provided image
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param projectID - the project whose profile picture should be updated
     * @param image     - the new profile picture
     * @return - the updated project object
     * @throws OrganizationServiceException - thrown due to a data or authorization
     *                                      error
     * @throws AuthenticationException      - thrown due to an authentication error
     */
    @Override
    public Project updateProjectImage(String authToken, String projectID, File image)
            throws OrganizationServiceException, AuthenticationException {
        Project project = projectDynamoDBService.load(projectID);
        if (project == null) {
            throw new OrganizationServiceException("Update Project Image", "Such project does not exist");
        }

        String creator = verifyPrivileges(projectID, authToken, Role.ADMIN);
        if (creator == null) {
            throw new OrganizationServiceException("Update Project Image", "Authorization Failed");
        }

        // store image in S3
        uploadImage(image, systemEnvValues.PROJECT_PIC_BUCKET, creator);
        project.setProfilePicture(image);

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PROJECT_UPDATED, creator);

        projectDynamoDBService.save(project);
        return project;
    }

    /**
     * Deletes the project
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param projectID - project to be deleted
     * @throws OrganizationServiceException - service exception
     */
    @Override
    public void deleteProject(String authToken, String projectID)
            throws OrganizationServiceException, AuthenticationException {
        if (!projectExists(projectID)) {
            throw new OrganizationServiceException("Delete Project", "Such project does not exist");
        }

        if (verifyPrivileges(projectID, authToken, Role.ADMIN) == null) {
            throw new OrganizationServiceException("Delete Project", "Authentication Failed");
        }
        Project project = projectDynamoDBService.load(projectID);
        project.setActive(false);
        projectDynamoDBService.save(project);

        if (project.isHealth()) {
            healthProjectService.deleteHealthProject(authToken, projectID);
        }
    }

    /**
     * Returns a specific project
     *
     * @param projectID - project id
     * @return - project object
     * @throws OrganizationServiceException - service exception
     */
    @Override
    public Project getProjectByID(String authToken, String projectID) throws OrganizationServiceException {
        Project project = projectDynamoDBService.load(projectID);
        if (!projectExists(projectID)) {
            throw new OrganizationServiceException("Get Project by ID", "Such project does not exist");
        } else {
            project.setRequesterRoles(getRequesterRoles(projectID, authToken));
            return project;
        }
    }

    /**
     * @return - list of all active projects
     */
    @Override
    public List<Project> getAllProjects() {
        List<Project> projects = projectDynamoDBService.scan();
        return projects.stream().filter(Project::isActive).collect(Collectors.toList());
    }

    /**
     * Get all projects within the specific organization
     *
     * @param parentId - organization ID
     * @return - List of projects
     * @throws OrganizationServiceException - service exception
     */
    @Override
    public List<Project> getProjectsByParent(String parentId) throws OrganizationServiceException {
        if (!orgExists(parentId) && !projectExists(parentId)) {
            throw new OrganizationServiceException("Get Projects by Parent", "Such parent does not exist");
        }
        List<ReliefComposite> queryResult = queryByParentIdChildType(parentId, "Project");
        List<Project> allProjects = new ArrayList<>();

        if (!queryResult.isEmpty()) {
            // find and load projects to allProjects list
            for (ReliefComposite r : queryResult) {
                if (projectExists(r.getChildId())) {
                    allProjects.add(projectDynamoDBService.load(r.getChildId()));
                }
            }
        }
        // return only active projects
        return allProjects;
    }

    /**
     * Retrieves list of projects person is involved with
     *
     * @param personID - person id
     * @return - the list of projects
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public List<Project> getProjectsByPerson(String personID) throws OrganizationServiceException {
        if (!personExists(personID)) {
            throw new OrganizationServiceException("Get Projects by Person", "Such Person does not exist");
        }
        List<ReliefComposite> queryResult = queryByChildIdParentType(personID, "Project");
        List<Project> projects = new ArrayList<>();

        if (!queryResult.isEmpty()) {
            for (ReliefComposite r : queryResult) {
                if (projectExists(r.getParentId())) {
                    projects.add(projectDynamoDBService.load(r.getParentId()));
                }
            }
        }
        return projects;
    }

    /**
     * Creates project status update and add to history
     *
     * @param authToken    - requester's auth token; includes "Bearer " prefix
     * @param projectID    - project id
     * @param statusUpdate - status updated to be added
     * @return - status update
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception (must be the
     *                                      admin)
     */
    @Override
    public ProjectStatusUpdate createProjectStatusUpdate(String authToken, String projectID,
            ProjectStatusUpdate statusUpdate) throws OrganizationServiceException, AuthenticationException {
        if (!projectExists(projectID)) {
            throw new OrganizationServiceException("Create Project Status Update", "Such project does not exist");
        }

        String personID = verifyPrivileges(projectID, authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Create Project Status Update", "Authentication Failed");
        }

        // push old update to update history
        Project project = projectDynamoDBService.load(projectID);

        project.getStatusUpdatesHistory().add(statusUpdate);

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PROJECT_UPDATED, personID);

        projectDynamoDBService.save(project);
        return statusUpdate;
    }

    /**
     * Delete a project status update from history.
     *
     * @param authToken    - requester's auth token; includes "Bearer " prefix
     * @param projectID    - project id
     * @param statusUpdate - status updated to be added
     * @return - status update
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception (must be the
     *                                      admin)
     */
    @Override
    public ProjectStatusUpdate removeProjectStatusUpdate(String authToken, String projectID,
            ProjectStatusUpdate statusUpdate) throws OrganizationServiceException, AuthenticationException {
        if (!projectExists(projectID)) {
            throw new OrganizationServiceException("Delete Project Status Update", "Such project does not exist");
        }

        String personID = verifyPrivileges(projectID, authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Delete Project Status Update", "Authentication Failed");
        }

        // push old update to update history
        Project project = projectDynamoDBService.load(projectID);

        project.getStatusUpdatesHistory()
                .removeIf(
                        s -> s.getAuthorID().equals(statusUpdate.getAuthorID())
                                && s.getDate().equals(statusUpdate.getDate())
                                && s.getTitle().equals(statusUpdate.getTitle())
                                && s.getDescription().equals(statusUpdate.getDescription()));

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.PROJECT_UPDATED, personID);

        projectDynamoDBService.save(project);
        return statusUpdate;
    }

    /**
     * Creates project tasks
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param projectID - project id
     * @param task      - task to be added
     * @return - added task
     * @throws OrganizationServiceException- api exception
     * @throws AuthenticationException       - authentication exception
     */
    @Override
    public Task createTask(String authToken, String projectID, Task task)
            throws OrganizationServiceException, AuthenticationException {
        if (task.getId() != null && taskExists(task.getId())) {
            throw new OrganizationServiceException("Create Task", "Task already exists");
        }

        if (!projectExists(projectID)) {
            throw new OrganizationServiceException("Create Task", "Such project does not exist");
        }

        String personID = verifyPrivileges(projectID, authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Create Task", "Authentication Failed");
        }

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.TASK_CREATED, personID);

        // save task and its child relationship to the project
        taskDynamoDBService.save(task);
        ReliefEntity taskRelationship = new ReliefComposite();
        addReliefEntity(taskRelationship, "Project", projectID, "Task", task.getId());
        return task;
    }

    /**
     * Updates a specific task under project
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param task      - task to be updated
     * @return - updated task
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    @Override
    public Task updateTask(String authToken, Task task) throws OrganizationServiceException, AuthenticationException {
        if (!taskExists(task.getId())) {
            throw new OrganizationServiceException("Update Task", "Such task does not exist");
        }

        // retrieve task from the database
        List<ReliefComposite> queryResult = queryByChildIdParentType(task.getId(), "Project");
        if (queryResult.isEmpty()) {
            throw new OrganizationServiceException("Update Task", "Such task does not exist");
        }

        Project project = projectDynamoDBService.load(queryResult.get(0).getParentId());
        if (!projectExists(project.getId())) {
            throw new OrganizationServiceException("Update Task", "Such task does not exist");
        }

        String personID = verifyPrivileges(project.getId(), authToken, Role.ADMIN);
        if (personID == null) {
            throw new OrganizationServiceException("Update Task", "Authentication Failed");
        }

        // attempt to reward the requester
        if (task.getStatus() == Task.TaskStatus.COMPLETED) {
            rewardService.issueActivityReward(Event.Activity.TASK_COMPLETED, personID);
        } else {
            rewardService.issueActivityReward(Event.Activity.TASK_UPDATED, personID);
        }

        taskDynamoDBService.save(task);
        return task;
    }

    /**
     * Deletes a specific task under a project
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param taskId    - id of the task to be deleted
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    @Override
    public void deleteTask(String authToken, String taskId)
            throws OrganizationServiceException, AuthenticationException {
        if (!taskExists(taskId)) {
            throw new OrganizationServiceException("Delete Task", "Such task does not exist");
        }

        List<ReliefComposite> queryResult = queryByChildIdParentType(taskId, "Project");
        if (queryResult.isEmpty()) {
            throw new OrganizationServiceException("Delete Task", "Such task does not exist");
        }

        Project project = projectDynamoDBService.load(queryResult.get(0).getParentId());
        if (!projectExists(project.getId())) {
            throw new OrganizationServiceException("Delete Task", "Such task does not exist");
        }
        if (verifyPrivileges(project.getId(), authToken, Role.ADMIN) == null) {
            throw new OrganizationServiceException("Delete Task", "Authentication Failed");
        }
        Task task = taskDynamoDBService.load(taskId);
        task.setActive(false);
        taskDynamoDBService.save(task);
    }

    /**
     * Retrieves a specific task
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param taskID    - task id
     * @return - task
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public Task getTaskByID(String authToken, String taskID) throws OrganizationServiceException {
        // todo: why are we requiring a token here if we are never using it?
        if (!taskExists(taskID)) {
            throw new OrganizationServiceException("Get Task by ID", "Such task does not exist");
        } else {
            return taskDynamoDBService.load(taskID);
        }
    }

    /**
     * Retrieves all tasks by parent (project)
     *
     * @param parentID - parent id
     * @return - list of tasks
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public List<Task> getTasksByParent(String parentID) throws OrganizationServiceException {
        if (!projectExists(parentID)) {
            throw new OrganizationServiceException("Get Tasks by Parent", "Such project does not exist");
        }
        List<ReliefComposite> queryResult = queryByParentIdChildType(parentID, "Task");
        List<Task> allTasks = new ArrayList<>();

        if (!queryResult.isEmpty()) {
            for (ReliefComposite r : queryResult) {
                if (taskExists(r.getChildId())) {
                    allTasks.add(taskDynamoDBService.load(r.getChildId()));
                }
            }
        }
        return allTasks;
    }

    /**
     * Retrieves all tasks assigned to a specific person
     *
     * @param personID - person id
     * @return - list of tasks
     * @throws OrganizationServiceException - api exception
     */
    @Override
    public List<Task> getTasksByPerson(String personID) throws OrganizationServiceException {
        if (!personExists(personID)) {
            throw new OrganizationServiceException("Get Tasks by Person", "Such Person does not exist");
        }

        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":assignedPerson", new AttributeValue().withS(personID));
        eav.put(":active", new AttributeValue().withN("1"));
        DynamoDBQueryExpression<Task> queryExpression = new DynamoDBQueryExpression<Task>()
                .withIndexName("assignedPerson-index")
                .withKeyConditionExpression("assignedPerson = :assignedPerson")
                .withFilterExpression("active = :active")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);

        return taskDynamoDBService.query(queryExpression);
    }

    public Community createCommunity(String cognitoID, Community community, Project projectId) {
        return community;
    }

    public Community updateCommunity(String cognitoID, Community community) {
        return community;
    }

    public void deleteCommunity(String cognitoID, String communityId) {
    }

    public List<Community> getCommunitiesByProject(String projectID) {
        // todo : query ReliefEntity table where parentID == projectID && childType ==
        // "Community"
        List<Community> communities = new ArrayList<>();
        return communities;
    }

    /**
     * Adds sub-organization to parent organization
     *
     * @param authToken   - requester's auth token; includes "Bearer " prefix
     * @param parentOrgID - parent organization id
     * @param orgID       - sub organization id
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    public void addSubOrganization(String authToken, String parentOrgID, String orgID)
            throws OrganizationServiceException, AuthenticationException {
        if (verifyPrivileges(parentOrgID, authToken, Role.ADMIN) == null) {
            throw new OrganizationServiceException("addOrganizationToOrganization", "Authorization failed");
        }
        ReliefEntity reliefEntity = new ReliefComposite();
        addReliefEntity(reliefEntity, "Organization", parentOrgID, "Organization", orgID);
    }

    /**
     * Add person role to organization
     *
     * @param authToken         - requester's auth token; includes "Bearer " prefix
     * @param personToBeAddedId - id of the person to be added
     * @param role              - role of the person
     * @param orgID             - organization to which PersonRole is added
     * @throws OrganizationServiceException - service exception
     */
    @Override
    public void addPersonRoleToOrganization(String authToken, String personToBeAddedId, Role role, String orgID)
            throws OrganizationServiceException, AuthenticationException {
        if (!personExists(personToBeAddedId) || !orgExists(orgID)) {
            throw new OrganizationServiceException("Add PersonRole to Organization",
                    "Such Person or Organization does not exist");
        }
        List<PersonRole> duplicatePersonRoles = getPersonRoles(personToBeAddedId, orgID, role);
        if (!duplicatePersonRoles.isEmpty()) {
            throw new OrganizationServiceException("Add PersonRole to Organization",
                    "Person already has specified Role in the Organization");
        }
        PersonRole.RequestStatus newStatus;
        if (verifyPrivileges(orgID, authToken, Role.ADMIN) != null) {
            newStatus = PersonRole.RequestStatus.INVITED;
        } else {
            newStatus = PersonRole.RequestStatus.REQUESTED;
        }
        Date creationDate = Calendar.getInstance().getTime();
        PersonRole personRole = new PersonRole(creationDate, role, personToBeAddedId, newStatus);
        addReliefEntity(personRole, "Organization", orgID, "Person", personRole.getPersonId());
    }

    /**
     * Adds person and its role to the project
     *
     * @param authToken       - requester's auth token; includes "Bearer " prefix
     * @param personToBeAdded - person's id that needs to be added
     * @param role            - person's role
     * @param projectId       - project id
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    @Override
    public void addPersonRoleToProject(String authToken, String personToBeAdded, Role role, String projectId)
            throws OrganizationServiceException, AuthenticationException {
        if (!personExists(personToBeAdded) || !projectExists(projectId)) {
            throw new OrganizationServiceException("Add PersonRole to Organization",
                    "Such Person or Project does not exist");
        }
        List<PersonRole> duplicatePersonRoles = getPersonRoles(personToBeAdded, projectId, role);
        if (!duplicatePersonRoles.isEmpty()) {
            throw new OrganizationServiceException("Add PersonRole to Project",
                    "Person already has specified Role in the Project");
        }
        PersonRole.RequestStatus newStatus;
        if (verifyPrivileges(projectId, authToken, Role.ADMIN) != null) {
            newStatus = PersonRole.RequestStatus.INVITED;
        } else {
            newStatus = PersonRole.RequestStatus.REQUESTED;
        }
        Date creationDate = Calendar.getInstance().getTime();
        PersonRole personRole = new PersonRole(creationDate, role, personToBeAdded, newStatus);
        addReliefEntity(personRole, "Project", projectId, "Person", personRole.getPersonId());
    }

    public void addPersonRoleToCommunity(String authToken, String personID, Role role, String communityID)
            throws OrganizationServiceException, AuthenticationException {
        // todo : should authToken holder be the admin of the project to execute this?
        // if so, we need a project id in this function
    }

    /**
     * Updates person's role within the parent relief entity
     *
     * @param authToken      - requester's auth token; includes "Bearer " prefix
     * @param oldRole        - person's old role
     * @param role           - person's new role
     * @param parentEntityId - parent relief entity id
     * @param personId       - person's id
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    public void updatePersonRole(String authToken, Role oldRole, Role role, String parentEntityId, String personId)
            throws OrganizationServiceException, AuthenticationException {
        if (!orgExists(parentEntityId) && !projectExists(parentEntityId)) {
            throw new OrganizationServiceException("updatePersonRole", "Organization or Project does not exist");
        }

        String requesterID = verifyPrivileges(parentEntityId, authToken, Role.ADMIN);
        if (requesterID == null) {
            throw new OrganizationServiceException("updatePersonRole", "Authorization failed");
        }

        List<PersonRole> queryResult = getPersonRoles(personId, parentEntityId, oldRole);
        if (queryResult.isEmpty()) {
            throw new OrganizationServiceException("updatePersonRole", "No such person-role pair");
        }
        PersonRole personRole = queryResult.get(0);
        personRole.setReliefRole(role);
        reliefEntityDynamoDBService.save(personRole);

        // determine the parent type
        String parentType = personRole.getParentType();
        if (parentType.equals("Organization")) {
            String organization = getOrganizationById("", parentEntityId).getName();
            String message = "In Organization: " + organization + " current role has been updated from"
                    + oldRole.toString() + " to " + role.toString();
            createEvent(authToken, personId, Event.Activity.ROLE_CHANGED, message, parentType, parentEntityId);
        } else if (parentType.equals("Project")) {
            String project = getProjectByID("", parentEntityId).getName();
            String message = "In Project: " + project + " current role has been updated from" + oldRole.toString()
                    + " to " + role.toString();
            createEvent(authToken, personId, Event.Activity.ROLE_CHANGED, message, parentType, parentEntityId);
        }

        // attempt to reward the requester
        rewardService.issueActivityReward(Event.Activity.ROLE_CHANGED, requesterID);
    }

    /**
     * Updates person's role status
     *
     * @param authToken      - requester's auth token; includes "Bearer " prefix
     * @param role           - role status of which to be updated
     * @param parentEntityId - parent relief entity id
     * @param personId       - person id
     * @param updatedStatus  - new status on that role
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authnetication exception
     */
    public void updatePersonRoleStatus(String authToken, Role role, String parentEntityId, String personId,
            PersonRole.RequestStatus updatedStatus) throws OrganizationServiceException, AuthenticationException {
        // confirm the specified PersonRole exists
        List<PersonRole> queryResult = getPersonRoles(personId, parentEntityId, role);
        if (queryResult.isEmpty()) {
            throw new OrganizationServiceException("changePersonRoleStatus", "No such person-role pair");
        }

        // only the Person can update their own INVITED status
        PersonRole personRole = queryResult.get(0);
        PersonRole.RequestStatus oldStatus = personRole.getRequestStatus();
        if (oldStatus == PersonRole.RequestStatus.INVITED) {

            if (!identifyAuthToken(authToken).getPersonId().equals(personId)) {
                throw new OrganizationServiceException("changePersonRoleStatus", "Authorization failed");
            }
            // otherwise only an admin can update other statuses
        } else {
            if (verifyPrivileges(parentEntityId, authToken, Role.ADMIN) == null) {
                throw new OrganizationServiceException("changePersonRoleStatus", "Authorization failed");
            }
        }
        // determine the parent type
        String parentType = personRole.getParentType();
        if (parentType.equals("Organization")) {
            String organization = getOrganizationById("", parentEntityId).getName();

            if (updatedStatus == PersonRole.RequestStatus.ACTIVE) {
                String message = "Has joined " + organization + " as a" + role.toString();
                createEvent(authToken, personId, Event.Activity.ORG_JOINED, message, parentType, parentEntityId);

                // attempt to reward the new member
                rewardService.issueActivityReward(Event.Activity.ORG_JOINED, personId);
            } else if (oldStatus == PersonRole.RequestStatus.ACTIVE
                    && updatedStatus == PersonRole.RequestStatus.INACTIVE) {
                String message = "No longer " + role.toString() + "in organization " + organization;
                createEvent(authToken, personId, Event.Activity.ORG_LEFT, message, parentType, parentEntityId);

                // attempt to reward the departed member
                rewardService.issueActivityReward(Event.Activity.ORG_LEFT, personId);
            }
        }

        if (parentType.equals("Project")) {
            String project = getProjectByID("", parentEntityId).getName();
            if (updatedStatus == PersonRole.RequestStatus.ACTIVE) {
                String message = "Has joined " + project + " as a" + role.toString();
                createEvent(authToken, personId, Event.Activity.PROJECT_JOINED, message, parentType, parentEntityId);

                // attempt to reward the new member
                rewardService.issueActivityReward(Event.Activity.PROJECT_JOINED, personId);
            } else if (oldStatus == PersonRole.RequestStatus.ACTIVE
                    && updatedStatus == PersonRole.RequestStatus.INACTIVE) {
                String message = "No longer " + role.toString() + "in organization " + project;
                createEvent(authToken, personId, Event.Activity.PROJECT_LEFT, message, parentType, parentEntityId);

                // attempt to reward the departed member
                rewardService.issueActivityReward(Event.Activity.PROJECT_LEFT, personId);
            }
        }

        personRole.setRequestStatus(updatedStatus);
        reliefEntityDynamoDBService.save(personRole);
    }

    /**
     * Retrieves list of people and the roles they requested to be within the parent
     * relief entity
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param parentID  - parent relief entity id
     * @return - persons mapped to list of roles they requested
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    public Map<Person, Set<Role>> getRoleRequestsByParent(String authToken, String parentID)
            throws OrganizationServiceException, AuthenticationException {
        if (!orgExists(parentID) && !projectExists(parentID)) {
            throw new OrganizationServiceException("Get Role Requests by Parent", "Such parent does not exist");
        }

        if (verifyPrivileges(parentID, authToken, Role.ADMIN) == null) {
            throw new OrganizationServiceException("Get Role Requests by Parent", "Authorization failed");
        }

        // retrieve Person Roles requests for specified parent
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":parentId", new AttributeValue().withS(parentID));
        eav.put(":childType", new AttributeValue().withS("Person"));
        eav.put(":requestStatus", new AttributeValue().withS(String.valueOf(PersonRole.RequestStatus.REQUESTED)));
        DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                .withIndexName("parentId-index")
                .withKeyConditionExpression("parentId = :parentId")
                .withFilterExpression("childType = :childType and requestStatus = :requestStatus")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        List<PersonRole> queryResult = reliefEntityDynamoDBService.queryPersonRole(queryExpression);

        // if no roles found then return empty map of Persons
        Map<Person, Set<Role>> roleRequests = new HashMap<>();
        if (queryResult.isEmpty()) {
            return roleRequests;
        }

        // otherwise fill map with Persons and their Roles
        for (PersonRole role : queryResult) {
            Person person = personDynamoDBService.load(role.getChildId());
            // skip Person if they have been deactivated
            if (!person.isActive()) {
                continue;
            }

            // otherwise capture their Role in the map
            if (roleRequests.containsKey(person)) {
                Set<Role> personRoles = roleRequests.get(person);
                personRoles.add(role.getReliefRole());
            } else {
                Set<Role> roles = new HashSet<>();
                roles.add(role.getReliefRole());
                roleRequests.put(person, roles);
            }
        }
        return roleRequests;
    }

    /**
     * Retrieves the list of projects and roles person has been invited as
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @return - projects mapped to roles a person has been invited for
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    public Map<Project, Set<Role>> getProjectRoleInvites(String authToken)
            throws OrganizationServiceException, AuthenticationException {
        String personID = identifyAuthToken(authToken).getPersonId();
        if (personID == null) {
            throw new OrganizationServiceException("Get Project Role Invites", "Authentication failed");
        }
        List<PersonRole> queryResult = queryInvites(personID, "Person", "Project");

        // if no roles found then return empty map of Projects
        Map<Project, Set<Role>> roleInvites = new HashMap<>();
        if (queryResult.isEmpty()) {
            return roleInvites;
        }

        // otherwise fill map with Projects and their Roles
        for (PersonRole role : queryResult) {
            Project project = projectDynamoDBService.load(role.getParentId());
            // skip Project if it has been deactivated
            if (!project.isActive()) {
                continue;
            }

            // otherwise capture the invite Role in the map
            if (roleInvites.containsKey(project)) {
                Set<Role> personRoles = roleInvites.get(project);
                personRoles.add(role.getReliefRole());
            } else {
                Set<Role> roles = new HashSet<>();
                roles.add(role.getReliefRole());
                roleInvites.put(project, roles);
            }
        }
        return roleInvites;
    }

    /**
     * Retrieves the list of organization and roles person has been invited as
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @return - organizations mapped to roles a person has been invited for
     * @throws OrganizationServiceException - api exception
     * @throws AuthenticationException      - authentication exception
     */
    public Map<Organization, Set<Role>> getOrganizationRoleInvites(String authToken)
            throws OrganizationServiceException, AuthenticationException {
        String personID = identifyAuthToken(authToken).getPersonId();
        if (personID == null) {
            throw new OrganizationServiceException("Get Organization Role Invites", "Authentication failed");
        }

        List<PersonRole> queryResult = queryInvites(personID, "Person", "Organization");
        // if no roles found then return empty map of Organizations
        Map<Organization, Set<Role>> roleInvites = new HashMap<>();
        if (queryResult.isEmpty()) {
            return roleInvites;
        }

        // otherwise fill map with Organizations and their Roles
        for (PersonRole role : queryResult) {
            Organization organization = orgDynamoDBService.load(role.getParentId());
            // skip Organization if it has been deactivated
            if (!organization.isActive()) {
                continue;
            }

            // otherwise capture the invite Role in the map
            if (roleInvites.containsKey(organization)) {
                Set<Role> personRoles = roleInvites.get(organization);
                personRoles.add(role.getReliefRole());
            } else {
                Set<Role> roles = new HashSet<>();
                roles.add(role.getReliefRole());
                roleInvites.put(organization, roles);
            }
        }

        return roleInvites;
    }

    @Override
    public void issueReward(String authToken, String reason, int amount, String receiverID, String receiverType)
            throws OrganizationServiceException, AuthenticationException {
        rewardService.issueReward(authToken, reason, amount, receiverID, receiverType);
    }

    /**
     * Retrieves the person id given cognitoID
     *
     * @param cognitoID - cognito ID
     * @return - person id
     * @throws AuthenticationException - authentication exception
     */
    private String getPersonId(String cognitoID) throws AuthenticationException {
        List<Person> queryResult = queryCognitoID(cognitoID);

        // if Person found return their id; otherwise null
        if (!queryResult.isEmpty()) {
            return queryResult.get(0).getId();
        } else {
            throw new AuthenticationException("Get Person Id", "Authentication failed");
        }
    }

    /**
     * Retrieves a cognito user from authentication module given authToken
     *
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @return - cognito user
     * @throws AuthenticationException - authentication exception
     */
    private CognitoUser getCognitoUser(String authToken) throws AuthenticationException {
        return cognitoService.fetchUser(authToken);
    }

    private CognitoUser identifyAuthToken(String authToken) throws AuthenticationException {
        // retrieve Cognito user by auth token
        CognitoUser user = getCognitoUser(authToken);

        // query Person table with Cognito ID to get matching Person ID
        user.setPersonId(getPersonId(user.getData().getUserId()));

        // return the CognitoUser
        return user;
    }

    /**
     * Verify privileges that auth token holder has within the relief entity
     *
     * @param parentId  - relief entity id (project/organization)
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @param role      - role needed for privileges
     * @return - person id
     * @throws AuthenticationException - authentication exception
     */
    private String verifyPrivileges(String parentId, String authToken, Role role) throws AuthenticationException {
        CognitoUser user = identifyAuthToken(authToken);

        if (user.isGlobalAdmin()) {
            return user.getPersonId();
        } else {
            Map<String, AttributeValue> eav = new HashMap<>();
            eav.put(":childId", new AttributeValue().withS(user.getPersonId()));
            eav.put(":parentId", new AttributeValue().withS(parentId));
            eav.put(":reliefRole", new AttributeValue().withS(String.valueOf(role)));
            DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                    .withIndexName("childId-index")
                    .withKeyConditionExpression("childId = :childId")
                    .withFilterExpression("parentId = :parentId and reliefRole = :reliefRole")
                    .withExpressionAttributeValues(eav)
                    .withConsistentRead(false);
            List<PersonRole> queryResult = reliefEntityDynamoDBService.queryPersonRole(queryExpression);

            if (!queryResult.isEmpty()) {
                return user.getPersonId();
            } else {
                return null;
            }
        }
    }

    /**
     * Retrieves list of roles a person has within the relief entity
     *
     * @param parentID  - relief entity id
     * @param authToken - requester's auth token; includes "Bearer " prefix
     * @return - list of roles
     */
    private List<Role> getRequesterRoles(String parentID, String authToken) {
        // if unable to authenticate then return the empty list of roles
        List<Role> requesterRoles = new ArrayList<>();
        CognitoUser user;
        try {
            user = identifyAuthToken(authToken);
        } catch (AuthenticationException e) {
            return requesterRoles;
        }

        if (user.isGlobalAdmin()) {
            requesterRoles.add(Role.GLOBAL_ADMIN);
        } else {
            // We will get a collection and then populate the list with roles.
            Map<String, AttributeValue> eav = new HashMap<>();
            eav.put(":childId", new AttributeValue().withS(user.getPersonId()));
            eav.put(":parentId", new AttributeValue().withS(parentID));
            DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                    .withIndexName("childId-index")
                    .withKeyConditionExpression("childId = :childId")
                    .withFilterExpression("parentId = :parentId")
                    .withExpressionAttributeValues(eav)
                    .withConsistentRead(false);
            List<PersonRole> queryResult = reliefEntityDynamoDBService.queryPersonRole(queryExpression);

            for (PersonRole personRole : queryResult) {
                requesterRoles.add(personRole.getReliefRole());
            }
        }
        return requesterRoles;
    }

    /**
     * Retrieves list of people with certain roles within the relief entity
     *
     * @param personId - person id
     * @param parentId - relief entity id
     * @param role     - relief role
     * @return - list of persons with the specified role
     * @throws OrganizationServiceException - api exception
     */
    private List<PersonRole> getPersonRoles(String personId, String parentId, Role role)
            throws OrganizationServiceException {
        if (!personExists(personId)) {
            throw new OrganizationServiceException("getPersonRole", "Person does not exist");
        }
        if (!orgExists(parentId) && !projectExists(parentId)) {
            throw new OrganizationServiceException("getPersonRole", "Parent does not exist");
        }

        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":parentId", new AttributeValue().withS(parentId));
        eav.put(":childId", new AttributeValue().withS(personId));
        eav.put(":reliefRole", new AttributeValue().withS(String.valueOf(role)));
        DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                .withIndexName("childId-index")
                .withKeyConditionExpression("childId = :childId")
                .withFilterExpression("reliefRole = :reliefRole and parentId = :parentId")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);

        return reliefEntityDynamoDBService.queryPersonRole(queryExpression);
    }

    /**
     * Checks if person exists within the database. Returns TRUE if person exists.
     *
     * @param id - id of the person to be checked
     */
    private boolean personExists(String id) {
        Person person = personDynamoDBService.load(id);
        if (person == null || !person.isActive()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if organization exists within the database. Returns TRUE if
     * organization exists.
     *
     * @param id - id of the organization to be checked
     */
    private boolean orgExists(String id) {
        Organization org = orgDynamoDBService.load(id);
        if (org == null || !org.isActive()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the project exists within the database. Returns TRUE if event
     * exists.
     *
     * @param id - id of the project to be checked
     */
    private boolean projectExists(String id) {
        Project project = projectDynamoDBService.load(id);
        if (project == null || !project.isActive()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the community exists within the database. Returns TRUE if event
     * exists.
     *
     * @param id - id of the community to be checked
     */
    private boolean communityExists(String id) {
        Community community = communityDynamoDBService.load(id);
        if (community == null || !community.isActive()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the Task exists within the database. Returns TRUE if Task exists.
     *
     * @param id - id of the community to be checked
     */
    private boolean taskExists(String id) {
        Task task = taskDynamoDBService.load(id);
        if (task == null || !task.isActive()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Uploads the specified image to S3 and returns the image File object with
     * updated metadata for storage in DynamoDB.
     *
     * @param image   - the image to upload
     * @param bucket  - the S3 bucket to upload to
     * @param creator - the person uploading this image
     * @throws OrganizationServiceException - thrown due to a data or connection
     *                                      error
     */
    private void uploadImage(File image, String bucket, String creator) throws OrganizationServiceException {
        try {
            // store image itself in S3
            image.setName(System.currentTimeMillis() + image.getName());
            image.setBucketName(bucket);
            String url = s3Service.put(image);

            // prep image metadata for DynamoDB and return
            image.setUrl(url);
            image.setCreator(creator);
            image.setBody(null);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new OrganizationServiceException("Upload Image", "Invalid image body structure");
        } catch (IOException e) {
            throw new OrganizationServiceException("Upload Image", "Image upload failed");
        }
    }

    /**
     * a helper method to add a relationship in RelifEntity table
     *
     * @param reliefEntity - Entity to be added
     * @param parentType   - Parent type of the entity ("Organization" or a
     *                     "Project")
     * @param parentId     - ID of the parent entity
     * @param childType    - Child entity type ("Organization", "Project" or a
     *                     "Person")
     * @param childId      - Id of the child entity
     */
    private void addReliefEntity(ReliefEntity reliefEntity, String parentType, String parentId, String childType,
            String childId) {
        reliefEntity.setParentType(parentType);
        reliefEntity.setParentId(parentId);
        reliefEntity.setChildId(childId);
        reliefEntity.setChildType(childType);
        reliefEntityDynamoDBService.save(reliefEntity);
    }

    // Helper methods for querying the database tables
    private List<ReliefComposite> queryByChildIdParentType(String childId, String parentType) {
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":childId", new AttributeValue().withS(childId));
        eav.put(":parentType", new AttributeValue().withS(parentType));
        DynamoDBQueryExpression<ReliefComposite> queryExpression = new DynamoDBQueryExpression<ReliefComposite>()
                .withIndexName("childId-index")
                .withKeyConditionExpression("childId = :childId")
                .withFilterExpression("parentType = :parentType")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        return reliefEntityDynamoDBService.query(queryExpression);
    }

    private List<ReliefComposite> queryByParentIdChildType(String parentId, String childType) {
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":parentId", new AttributeValue().withS(parentId));
        eav.put(":childType", new AttributeValue().withS(childType));
        DynamoDBQueryExpression<ReliefComposite> queryExpression = new DynamoDBQueryExpression<ReliefComposite>()
                .withIndexName("parentId-index")
                .withKeyConditionExpression("parentId = :parentId")
                .withFilterExpression("childType = :childType")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        return reliefEntityDynamoDBService.query(queryExpression);
    }

    private List<PersonRole> queryInvites(String childId, String childType, String parentType) {
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":childId", new AttributeValue().withS(childId));
        eav.put(":childType", new AttributeValue().withS(childType));
        eav.put(":parentType", new AttributeValue().withS(parentType));
        eav.put(":requestStatus", new AttributeValue().withS(String.valueOf(PersonRole.RequestStatus.INVITED)));
        DynamoDBQueryExpression<PersonRole> queryExpression = new DynamoDBQueryExpression<PersonRole>()
                .withIndexName("childId-index")
                .withKeyConditionExpression("childId = :childId")
                .withFilterExpression(
                        "childType = :childType and parentType = :parentType and requestStatus = :requestStatus")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        return reliefEntityDynamoDBService.queryPersonRole(queryExpression);
    }

    private List<Person> queryCognitoID(String cognitoID) {
        Map<String, AttributeValue> eav = new HashMap<>();
        eav.put(":cognitoID", new AttributeValue().withS(cognitoID));
        eav.put(":active", new AttributeValue().withN("1"));
        DynamoDBQueryExpression<Person> queryExpression = new DynamoDBQueryExpression<Person>()
                .withIndexName("cognitoID-index")
                .withKeyConditionExpression("cognitoID = :cognitoID")
                .withFilterExpression("active = :active")
                .withExpressionAttributeValues(eav)
                .withConsistentRead(false);
        return personDynamoDBService.query(queryExpression);
    }

    private void activateTestingMode() {
        testingMode = true;
    }
}
