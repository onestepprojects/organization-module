package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.ProjectStatusUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ProjectStatusUpdateTypeConverter implements DynamoDBTypeConverter<String, ProjectStatusUpdate> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(ProjectStatusUpdate statusUpdate) {
        return DynamoDBTypeConverterHelper.getInstance().convert(statusUpdate, "status update", log);
    }

    @Override
    public ProjectStatusUpdate unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        ProjectStatusUpdate statusUpdate;

        try {
            statusUpdate = objectMapper.readValue(s, ProjectStatusUpdate.class);
            log.debug("unmarshalled address to " + statusUpdate);
        } catch (IOException e) {
            log.error("error de-marshalling address: " + s, e);
            return null;
        }

        return statusUpdate;
    }
}
