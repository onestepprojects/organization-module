export const projectStageOptions = [
  { label: 'PENDING', value: 'PENDING' },
  { label: 'INITIATED', value: 'INITIATED' },
  { label: 'ASSESSMENT', value: 'ASSESSMENT' },
  { label: 'DESIGN', value: 'DESIGN' },
  { label: 'IMPLEMENTATION', value: 'IMPLEMENTATION' },
  { label: 'FOLLOWUP', value: 'FOLLOWUP' },
  { label: 'COMPLETED', value: 'COMPLETED' },
  { label: 'CLOSED', value: 'CLOSED' },
]
