import { Space, Spin } from 'antd'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { ListItemCard, PageTitle, ServerError } from '../../components'
import { useGetPersonRolesByParentQuery, useGetPersonsQuery } from '../../store'

export const PersonList: FC = () => {
  const { organizationId } = useParams<{ organizationId?: string }>()
  const { isLoading, data, error } = organizationId
    ? // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetPersonRolesByParentQuery(organizationId)
    : // eslint-disable-next-line react-hooks/rules-of-hooks
      useGetPersonsQuery()

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>{organizationId ? '' : 'All '}Members</PageTitle>

      {error ? (
        <ServerError />
      ) : (
        <Spin spinning={isLoading} delay={500}>
          <Space className='mb-8' wrap size={['large', 48]}>
            {data?.map((person) => (
              <ListItemCard
                key={person.uuid || person.id}
                cover={person.profilePicture?.url || ''}
                coverFallback='https://onestep-person-pics.s3.amazonaws.com/defaultperson.svg'
                title={person.name}
                description={person.currentAddress?.country}
                link={`/persons/${person.uuid || person.id}`}
              />
            ))}
          </Space>
        </Spin>
      )}
    </div>
  )
}
