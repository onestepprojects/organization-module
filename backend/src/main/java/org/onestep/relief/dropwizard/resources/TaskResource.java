package org.onestep.relief.dropwizard.resources;

import com.google.inject.Inject;
import org.dozer.DozerBeanMapper;
import org.onestep.relief.dropwizard.representations.TaskRepr;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.model.Task;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;
import org.onestep.relief.organization.service.OrganizationServiceImpl;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Path("/tasks")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
public class TaskResource {
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;
    private final Validator validator;

    /**
     * Constructor initializes a Dozer mapper, a Validator, and the model interface.
     *
     * @param validator
     *            Validator class received from environment
     */
    @Inject
    public TaskResource(OrganizationServiceImpl orgInterface, Validator validator) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        this.dtoMapper = mapper;
        this.validator = validator;
        this.orgInterface = orgInterface;
    }

    @GET
    @Path("/{taskId}")
    public Response getTaskById(@PathParam("taskId") String taskId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            Task task = orgInterface.getTaskByID(authToken.orElse("UNKNOWN"), taskId);
            TaskRepr taskRepr = dtoMapper.map(task, TaskRepr.class);

            return Response.status(Response.Status.OK).entity(taskRepr).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("/by-parent/{parentId}")
    public Response getTasksByParent(@PathParam("parentId") String parentId) {

        try {
            List<Task> tasks = orgInterface.getTasksByParent(parentId);

            return Response.status(Response.Status.OK)
                    .entity(tasks.stream().map(t -> dtoMapper.map(t, TaskRepr.class)).collect(toList())).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("/by-person/{personId}")
    public Response getTasksByPerson(@PathParam("personId") String personId) {

        try {
            List<Task> tasks = orgInterface.getTasksByPerson(personId);

            return Response.status(Response.Status.OK)
                    .entity(tasks.stream().map(t -> dtoMapper.map(t, TaskRepr.class)).collect(toList())).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/create/{projectId}")
    public Response createTask(TaskRepr taskRepr, @PathParam("projectId") String projectId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authentication token required.");

        Set<ConstraintViolation<TaskRepr>> violations = validator.validate(taskRepr);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, Utils.getViolation(violations));

        try {
            Task task = dtoMapper.map(taskRepr, Task.class);

            TaskRepr newTask = dtoMapper.map(orgInterface.createTask(authToken.orElse("UNKNOWN"), projectId, task),
                    TaskRepr.class);

            return Response.status(Response.Status.OK).entity(newTask).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/update/{projectId}")
    public Response updateTask(TaskRepr taskRepr, @PathParam("projectId") String projectId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authentication token required.");

        Set<ConstraintViolation<TaskRepr>> violations = validator.validate(taskRepr);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, Utils.getViolation(violations));

        try {
            Task task = dtoMapper.map(taskRepr, Task.class);
            Task newTask = orgInterface.updateTask(authToken.orElse("UNKNOWN"), task);
            TaskRepr taskRep = dtoMapper.map(newTask, TaskRepr.class);

            return Response.status(Response.Status.OK).entity(taskRep).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @DELETE
    @Path("/delete/{projectId}/{taskId}")
    public Response deleteTask(@PathParam("projectId") String projectId, @PathParam("taskId") String taskId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, "Authorization token is required.");

        try {
            orgInterface.deleteTask(authToken.orElse("UNKNOWN"), taskId);

            return Response.status(Response.Status.OK).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, e.getMessage());
        }

    }

}
