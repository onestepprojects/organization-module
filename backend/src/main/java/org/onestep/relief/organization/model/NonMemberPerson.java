package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.AllArgsConstructor;

/**
 * Represents a persons like emergency contacts, board members
 */

@Data
@AllArgsConstructor
@DynamoDBTable(tableName = "Person")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Person", description = "Persons")
public class NonMemberPerson extends BasePerson {
}