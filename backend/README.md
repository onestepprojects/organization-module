# Backend

For local development with DynamoDB, please follow the instructions.

## Environment variables

Create a `.env` file.

```bash
APP_BASE_URL=https://staging.onestepprojects.org/onestep
AUTH_SERVICE_LOGIN_PATH=/authorization/servicelogin
AUTH_USER_PATH=/authorization/user
AWS_ACCESS_KEY_ID=
AWS_DEFAULT_REGION=us-east-2
AWS_REGION=us-east-2
AWS_SECRET_ACCESS_KEY=
DDB_PREFIX=staging_
DDB_REGION_NAME=us-east-2
GET_REWARD_PATH=/rewards/reward
HEALTH_BASE_URL=https://staging.onestepprojects.org/onestep/health/projects
ISSUE_REWARD_PATH=/rewards/reward/issue
ORG_LOGO_PATH=
ORG_PIC_DEFAULT=https://onestep-org-pics.s3.amazonaws.com/defaultorg.svg
PAGO_ORG_CALLBACK_PATH=/organizations/provision-asset/
PAGO_PERSON_CALLBACK_PATH=/persons/provision-asset/
PERSON_PIC_BUCKET=onestep-person-pics
PERSON_PIC_DEFAULT=https://onestep-person-pics.s3.amazonaws.com/defaultperson.svg
PROJECT_PIC_BUCKET=https://onestep-project-pics.s3.amazonaws.com/defaultproject.svg
PROJECT_PIC_DEFAULT=onestep-project-pics
PROVISION_PAY_USER_PATH=/blockchain/provision/user
S3_REGION_NAME=us-east-1
SERVICE_CLIENT_ID=
SERVICE_CLIENT_SECRET=
```

## Debugging in VSCode

Create a `.vscode/launch.json`.

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "type": "java",
      "name": "Organization Service",
      "cwd": "${workspaceFolder}/backend",
      "envFile": "${workspaceFolder}/backend/.env",
      "request": "launch",
      "mainClass": "org.onestep.relief.dropwizard.App",
      "args": "server",
      "vmArgs": "-DskipTests -Ddw.server.applicationConnectors[0].port=3101 -Ddw.server.adminConnectors[0].port=3201"
    }
  ]
}
```

This will run java backend server under debug mode. The api will be available at `http://127.0.0.1:3101`.

## Local DynamicDB (optional)

### Start local DynamicDB

1. Docker is required
2. Node `npm` is required

```
npm i
npm run db
```

It will use docker to start a local dynamic db, with port `8000`.

When it's successful, you will see:

```
Initializing DynamoDB Local with the following configuration:
Port:	8000
InMemory:	false
DbPath:	null
SharedDb:	true
shouldDelayTransientStatuses:	false
CorsParams:	*
```

### Sync data from test env(https://test.onesteprelief.org/)

If we never synchronized data, or just want to fetch last data from test env to local, we can run:

```
npm run syncDb -- --profile=one-step-test-env
```

It will try to delete the tables from local db, and read all tables and data from remote, and import them to local db.

After several minutes, it should be done, and you will see `Sync done.`

### Start the backend server with local db

Copy the '.envExports.template' to '.envExports', and follow the comments in the file to get the correct values from gitlab CI.

Then, run

```
npm run server
```

It will run the './startLocalServer.sh' and which specifies the local db url:

```
export DDB_ENDPOINT_URL="http://localhost:8000"
```

When you see something like:

```
Dev mode
Use local dynamodb: http://localhost:8000
```

It means the server is running with local dynamodb.

### Local Integration Test

Since we don't maintain dynamodb tables schema locally, it's hard to create and drop tables in unit tests, we have to use
this approach:

1. Run `npm run db` to start the local docker dynamodb server, which listens on `8000`
2. Run `npm run syncDb` as mentioned in above sections, to sync data from test env to local
3. Make sure port `8001` is available, since s3 mock service will use it
4. Run `mvn clean test` to run integration tests locally

## Try the api

Open `http://127.0.0.1:8080/organizations/`, if everything is OK, you will see a long list of organization data.

Otherwise, you will see some errors and need to figure out the cause.

Now start the frontend and do some operations, the changes should be applied to the local DB only.

You can develop the backend with local db now.

## Use the local server

Go to 'one-step-ui' project and edit the `.env.development` file,
find key `REACT_APP_ORG_MODULE_API_URL` and change its value to `http://127.0.0.1:8080`

## Java code format

Run command:

```
mvn formatter:format
```
