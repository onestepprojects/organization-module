package org.onestep.relief.dropwizard.resources;

import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.onestep.relief.dropwizard.representations.FileRepr;
import org.onestep.relief.dropwizard.representations.OrganizationRepr;
import org.onestep.relief.dropwizard.representations.RolesTripleResponse;
import org.onestep.relief.organization.model.Organization;
import org.onestep.relief.organization.model.Person;
import org.onestep.relief.organization.model.PersonRole;
import org.onestep.relief.organization.model.Role;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.onestep.relief.TestUtils.*;
import static org.onestep.relief.dropwizard.resources.EntityBuilder.newOrganization;
import static org.onestep.relief.dropwizard.resources.EntityBuilder.newPerson;

public class OrganizationResourceTest extends BaseTest {

    private ResourceExtension organizationResource;

    private static final String AUTH_TOKEN = "mock-token";

    @BeforeEach
    public void beforeEach() throws Throwable {
        super.beforeEach();
        organizationResource = ResourceExtension.builder()
            .addResource(this.injector.getInstance(OrganizationResource.class))
            .build();
        organizationResource.before();
    }

    private Invocation.Builder request(String path) {
        return organizationResource.target(path)
            .request()
            .header("Authorization", "authorization: Bearer " + AUTH_TOKEN);
    }

    /**
     * {@link OrganizationResource#getOrganizations}
     */
    @Test
    void getOrganizations() throws Exception {
        OrganizationRepr[] organizations = request("/organizations").get(OrganizationRepr[].class);
        assertThat(organizations).isNotEmpty();
        OrganizationRepr org = Arrays.stream(organizations)
            .filter(it -> it.getId().equals("817f942c-807b-4fad-b868-45b718cbc255"))
            .findFirst()
            .get();
        assertThatJson(toJson(org)).isEqualTo(
            readFixture(this.getClass(), "OrganizationResourceTest.817f942c-807b-4fad-b868-45b718cbc255.json"));
    }

    /**
     * {@link OrganizationResource#createOrganization}
     */
    @Test
    void createOrganization() throws Exception {
        String seed = UUID.randomUUID().toString();
        String payload = readFixture(this.getClass(),
            "OrganizationResourceTest.createOrganizationSuccessfully.payload.json").replace("${seed}", seed);
        OrganizationRepr createdOrg = request("/organizations/create").post(Entity.json(payload),
            OrganizationRepr.class);

        // FIXME should be 201
        // assertThat(response.getStatus()).isEqualTo(200);

        assertThatJson(toJson(createdOrg)).isEqualTo(buildExpectedOrgJson(
            "OrganizationResourceTest.createOrganizationSuccessfully.response.json", seed, createdOrg));
    }

    private String buildExpectedOrgJson(String fixtureFileName, String seed, OrganizationRepr createdOrg)
        throws IOException {
        String expectedTemplate = readFixture(this.getClass(), fixtureFileName);
        // TODO can't believe Java still not support expression in multiple line string!
        String generatedId = createdOrg.getId();
        String logoTimestamp = createdOrg.getLogo().getName().substring(0, 13);
        System.out.println("### person:" + toJson(this.person));
        return expectedTemplate.replace("${seed}", seed)
            .replace("${generatedId}", generatedId)
            .replace("${logoCreator}", createdOrg.getId())
            .replace("${creatorUUID}", this.person.getId())
            .replace("${logoTimestamp}", logoTimestamp);
    }

    /**
     * {@link OrganizationResource#getRootOrganizations}
     */
    @Test
    void getRootOrganizations() throws Exception {
        Organization createdOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        OrganizationRepr[] rootOrganizations = request("/organizations/roots").get(OrganizationRepr[].class);
        assertThat(rootOrganizations.length).isGreaterThan(1);
        assertThat(Arrays.stream(rootOrganizations).map(OrganizationRepr::getId).collect(toList())).asList()
            .contains(createdOrg.getId());
        assertThat(rootOrganizations).allSatisfy(it ->
            // FIXME should not throw exception when a org has no parent
            assertThatThrownBy(() -> this.organizationService.getParentOrganization(AUTH_TOKEN, it.getId()))
                .hasMessageContaining("No parent organization found"));
    }

    /**
     * {@link OrganizationResource#getOrganizationById}
     */
    @Test
    void getOrganizationById() throws Exception {
        String seed = seed();
        Organization createdOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed));
        OrganizationRepr organization = request("/organizations/" + createdOrg.getId()).get(OrganizationRepr.class);
        assertThatJson(toJson(organization)).isEqualTo(
            buildExpectedOrgJson("OrganizationResourceTest.getOrganizationById.json", seed, organization));
    }

    /**
     * {@link OrganizationResource#getSubOrganizations}
     */
    @Test
    void getSubOrganizations() throws Exception {
        Organization parentOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        Organization subOrg1 = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        Organization subOrg2 = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));

        this.organizationService.addSubOrganization(AUTH_TOKEN, parentOrg.getId(), subOrg1.getId());
        this.organizationService.addSubOrganization(AUTH_TOKEN, parentOrg.getId(), subOrg2.getId());

        OrganizationRepr[] subOrganizations = request("/organizations/get-sub/" + parentOrg.getId())
            .get(OrganizationRepr[].class);
        assertThat(Arrays.stream(subOrganizations).map(it -> it.getId()).toArray())
            .hasSameElementsAs(List.of(subOrg1.getId(), subOrg2.getId()));
    }

    /**
     * {@link OrganizationResource#getOrganizationsByPerson}
     */
    @Test
    void getOrganizationsByPerson() throws Exception {
        Organization org = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        OrganizationRepr[] rootOrganizations = request("/organizations/get-org-by-person/" + this.person.getId())
            .get(OrganizationRepr[].class);
        assertThat(rootOrganizations).anySatisfy(it -> assertThat(it.getId()).isEqualTo(org.getId()));
    }

    /**
     * {@link OrganizationResource#getOrganizationRolesByPerson }
     */
    @Test
    void getOrganizationRolesByPerson() throws Exception {
        Person person = this.organizationService.createPerson(AUTH_TOKEN, newPerson(seed()));
        setupUserFetcher(person);

        Organization org = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        this.organizationService.addPersonRoleToOrganization(AUTH_TOKEN, person.getId(), Role.EXPERT, org.getId());
        this.organizationService.updatePersonRoleStatus(AUTH_TOKEN, Role.EXPERT, org.getId(), person.getId(),
            PersonRole.RequestStatus.ACTIVE);

        RolesTripleResponse[] orgRoles = request("/organizations/get-org-roles-by-person/" + person.getId())
            .get(RolesTripleResponse[].class);
        assertThat(Arrays.stream(orgRoles).flatMap(it -> it.getRoles().stream()).collect(toList())).asList()
            .hasSameElementsAs(List.of(Role.EXPERT.name(), Role.ADMIN.name()));
    }

    /**
     * {@link OrganizationResource#getParentOrganization }
     */
    @Test
    void getParentOrganization() throws Exception {
        Organization parentOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        Organization subOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));

        this.organizationService.addSubOrganization(AUTH_TOKEN, parentOrg.getId(), subOrg.getId());

        OrganizationRepr parentOrgRepr = request("/organizations/get-parent/" + subOrg.getId())
            .get(OrganizationRepr.class);
        assertThat(parentOrgRepr.getId()).isEqualTo(parentOrg.getId());
    }

    /**
     * {@link OrganizationResource#addSubOrganization }
     */
    @Test
    void addSubOrganization() throws Exception {
        Organization parentOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        Organization subOrg = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        // FIXME should be POST
        String response = request("/organizations/add-sub-organization/" + parentOrg.getId() + "/" + subOrg.getId()).get(String.class);
        assertThat(response).isEqualTo("DONE");
        assertThat(this.organizationService.getParentOrganization(AUTH_TOKEN, subOrg.getId()).getId()).isEqualTo(parentOrg.getId());
    }

    /**
     * {@link OrganizationResource#provision }
     */
    @Test
    void provision() throws Exception {
        String seed = seed();
        Organization org = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed));
        // FIXME should be PUT
        String assetId = "asset-" +seed;
        String result = request("/organizations/provision-asset/" + org.getId() + "/" + assetId).get(String.class);
        assertThat(result).isEqualTo("DONE");
        Organization fetchedOrg = organizationService.getOrganizationById(AUTH_TOKEN, org.getId());
        assertThat(fetchedOrg.getPaymentAccount().getAssets().get(assetId)).isEqualTo(true);
    }

    /**
     * {@link OrganizationResource#updateOrganization }
     */
    @Test
    void updateOrganization() throws Exception {
        Organization org = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        System.out.println("### org" + toJson(org));
        OrganizationRepr orgRepr = request("/organizations/" + org.getId()).get(OrganizationRepr.class);
        System.out.println("### orgRepr" + toJson(orgRepr));

        String oriName = orgRepr.getName();
        orgRepr.setName(oriName + "!!!");

        // FIXME should be PUT
        OrganizationRepr result = request("/organizations/update").post(Entity.json(orgRepr), OrganizationRepr.class);
        assertThat(result.getName()).isEqualTo(oriName + "!!!");
    }

    /**
     * {@link OrganizationResource#deleteOrganization }
     */
    @Test
    void deleteOrganization() throws Exception {
        Organization org = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));
        Response result = request("/organizations/delete/" + org.getId()).delete();
        assertThat(result.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
        assertThat(this.organizationService.getOrganizationById(AUTH_TOKEN, org.getId()).isActive()).isEqualTo(false);
    }

    /**
     * {@link OrganizationResource#updateImage }
     */
    @Test
    void updateImage() throws Exception {
        Organization org = this.organizationService.createOrganization(AUTH_TOKEN, newOrganization(seed()));

        String seed = seed();
        FileRepr logo = FileRepr.builder()
            .name("head-" + seed + ".jpeg")
            .size(120)
            .format("image/jpeg")
            .body("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7")
            .caption("test-caption-" + seed)
            .build();


        System.out.println("### logo" + toJson(logo));
        // // FIXME should be PUT
        OrganizationRepr updated = request("/organizations/update-image/" + org.getId()).post(Entity.json(logo), OrganizationRepr.class);
        assertThat(updated.getLogo().getName()).endsWith("head-" + seed + ".jpeg");
    }
}
