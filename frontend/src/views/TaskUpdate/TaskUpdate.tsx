import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import { type FC } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { PageTitle, TaskForm } from '../../components'
import { skipToken, useGetTaskQuery, useUpdateTaskMutation, type Task } from '../../store'

export const TaskUpdate: FC = () => {
  const navigate = useNavigate()
  const { taskId } = useParams<{ taskId: string }>()

  const { isLoading: isLoadingTask, data: task } = useGetTaskQuery(taskId || skipToken)

  const [updateTask] = useUpdateTaskMutation()

  const handleSubmit = useMemoizedFn(async (values: Task) => {
    const newTask = { ...task, ...values }
    console.log('Update task', newTask)

    try {
      await updateTask(newTask)
      message.success('Update task success')
      navigate(-1)
    } catch (err) {
      console.log('Update task failed', err)
      message.error('Update task failed')
    }
  })

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Update Task</PageTitle>

      <Card className='max-w-screen-lg rounded-lg' loading={isLoadingTask}>
        <TaskForm
          initialValues={task}
          onFinish={handleSubmit}
          // onValuesChange={console.log}
        />
      </Card>
    </div>
  )
}
