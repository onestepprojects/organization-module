package org.onestep.relief.dropwizard.resources;

import com.google.inject.Inject;
import org.dozer.DozerBeanMapper;
import org.onestep.relief.dropwizard.representations.FileRepr;
import org.onestep.relief.dropwizard.representations.OrganizationRepr;
import org.onestep.relief.dropwizard.representations.RolesTripleResponse;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.model.File;
import org.onestep.relief.organization.model.Organization;
import org.onestep.relief.organization.model.Role;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;
import org.onestep.relief.organization.service.OrganizationServiceImpl;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Path("/organizations")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
public class OrganizationResource {
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;
    private final Validator validator;

    /**
     * Constructor initializes a Dozer mapper, a Validator, and the model interface.
     *
     * @param validator
     *            Validator class received from environment
     */
    @Inject
    public OrganizationResource(OrganizationServiceImpl orgInterface, Validator validator) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        this.dtoMapper = mapper;
        this.validator = validator;
        this.orgInterface = orgInterface;
    }

    @GET
    public Response getOrganizations() {
        return Response.status(Status.OK).entity(orgInterface.getAllOrganizations().stream()
                .map(o -> dtoMapper.map(o, OrganizationRepr.class)).collect(toList())).build();
    }

    @GET
    @Path("/roots")
    public Response getRootOrganizations() {
        return Response.status(Status.OK).entity(orgInterface.getRootOrganizations().stream()
                .map(o -> dtoMapper.map(o, OrganizationRepr.class)).collect(toList())).build();
    }

    @GET
    @Path("/{id}")
    public Response getOrganizationById(@PathParam("id") String id,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            Organization org = orgInterface.getOrganizationById(authToken.orElse("UNKNOWN"), id);
            OrganizationRepr orgRepr = dtoMapper.map(org, OrganizationRepr.class);

            return Response.status(Status.OK).entity(orgRepr).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/get-sub/{parentId}")
    public Response getSubOrganizations(@PathParam("parentId") String parentId) {
        List<Organization> orgs = orgInterface.getSubOrganizations(parentId);

        return Response.status(Status.OK)
                .entity(orgs.stream().map(o -> dtoMapper.map(o, OrganizationRepr.class)).collect(toList())).build();
    }

    @GET
    @Path("/get-org-by-person/{personId}")
    public Response getOrganizationsByPerson(@PathParam("personId") String personId) {
        try {
            List<Organization> orgs = orgInterface.getOrganizationsByPerson(personId);

            return Response.status(Status.OK)
                    .entity(orgs.stream().map(o -> dtoMapper.map(o, OrganizationRepr.class)).collect(toList())).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/get-org-roles-by-person/{personId}")
    public Response getOrganizationRolesByPerson(@PathParam("personId") String personId) {
        try {
            Map<Organization, Set<Role>> resp = orgInterface.getOrganizationRolesByPerson(personId);

            return Response.status(Status.OK).entity(RolesTripleResponse.organizationTriples(resp)).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/get-parent/{childId}")
    public Response getParentOrganization(@PathParam("childId") String childId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            Organization org = orgInterface.getParentOrganization(authToken.orElse("UNKNOWN"), childId);
            OrganizationRepr orgRepr = dtoMapper.map(org, OrganizationRepr.class);

            return Response.status(Status.OK).entity(orgRepr).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @POST
    @Path("/create")
    public Response createOrganization(OrganizationRepr orgRequest,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Authorization token is required.");

        if (orgRequest.getId() != null)
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Organization UUID must be null.");

        if (ReliefValidators.invalidPhoneLabels(orgRequest))
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Invalid phone labels.");

        Set<ConstraintViolation<OrganizationRepr>> violations = validator.validate(orgRequest);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Status.BAD_REQUEST, Utils.getViolation(violations));

        try {
            Organization org = dtoMapper.map(orgRequest, Organization.class);

            Organization newOrg = orgInterface.createOrganization(authToken.orElse("UNKNOWN"), org);
            OrganizationRepr oResp = dtoMapper.map(newOrg, OrganizationRepr.class);

            return Response.status(Status.OK).entity(oResp).build();

        } catch (OrganizationServiceException | AuthenticationException exception) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, exception.getMessage());
        }
    }

    @GET
    @Path("/add-sub-organization/{parentOrgId}/{orgId}")
    public Response addSubOrganization(@PathParam("parentOrgId") String parentOrgId, @PathParam("orgId") String orgId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Authorization token is required.");

        try {
            orgInterface.addSubOrganization(authToken.orElse("UNKNOWN"), parentOrgId, orgId);

            return Response.status(Status.OK).entity("DONE").build();

        } catch (OrganizationServiceException | AuthenticationException exception) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, exception.getMessage());
        }
    }

    @GET
    @Path("/provision-asset/{orgId}/{assetId}")
    public Response provision(@PathParam("orgId") String orgId, @PathParam("assetId") String assetId) {

        try {
            orgInterface.provisionOrganizationAsset(orgId, assetId);

            return Response.status(Status.OK).entity("DONE").build();

        } catch (OrganizationServiceException exception) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, exception.getMessage());
        }
    }

    @POST
    @Path("/update")
    public Response updateOrganization(OrganizationRepr orgRequest,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Authorization token is required.");

        Set<ConstraintViolation<OrganizationRepr>> violations = validator.validate(orgRequest);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Status.BAD_REQUEST, Utils.getViolation(violations));

        if (ReliefValidators.invalidPhoneLabels(orgRequest))
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Invalid phone labels.");

        try {
            Organization org = dtoMapper.map(orgRequest, Organization.class);
            Organization newOrg = orgInterface.updateOrganization(authToken.orElse("UNKNOWN"), org);
            OrganizationRepr oRepr = dtoMapper.map(newOrg, OrganizationRepr.class);

            return Response.status(Status.OK).entity(oRepr).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, e.getMessage());
        }
    }

    @DELETE
    @Path("/delete/{orgId}")
    public Response deleteOrganization(@PathParam("orgId") String orgId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Authorization token is required.");

        try {
            orgInterface.deleteOrganization(authToken.orElse("UNKNOWN"), orgId);

            return Response.status(Status.OK).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @POST
    @Path("update-image/{organizationId}")
    public Response updateImage(FileRepr fileRepr, @PathParam("organizationId") String organizationId,
            @HeaderParam("Authorization") String authToken) {
        try {
            File file = dtoMapper.map(fileRepr, File.class);

            Organization org = orgInterface.updateOrganizationImage(authToken, organizationId, file);
            OrganizationRepr orgResp = dtoMapper.map(org, OrganizationRepr.class);

            return Response.status(Status.OK).entity(orgResp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, e.getMessage());
        }
    }
}
