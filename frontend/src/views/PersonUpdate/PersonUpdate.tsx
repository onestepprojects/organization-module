import { useMemoizedFn } from 'ahooks'
import { Card, message } from 'antd'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'

import { PageTitle, PersonForm } from '../../components'
import { skipToken, useGetPersonQuery, useUpdatePersonMutation } from '../../store'

export const PersonUpdate: FC = () => {
  const { personId } = useParams<{ personId: string }>()

  const { isLoading: isLoadingPerson, data: person } = useGetPersonQuery(personId || skipToken)
  const [updatePerson] = useUpdatePersonMutation()

  const handleSubmit = useMemoizedFn(async (values) => {
    try {
      const newPerson = {
        ...person,
        ...values,
      }
      console.log('Update person', newPerson)
      await updatePerson(newPerson).unwrap()
      location.href = '/'
    } catch (err) {
      console.log('Update person failed', err)
      message.error('Update profile failed')
    }
  })

  return (
    <div>
      <PageTitle caption='UPDATE ONE STEP PROFILE'>Member</PageTitle>

      <Card className='rounded-xl' title='Update' loading={isLoadingPerson}>
        <PersonForm
          initialValues={person}
          submitButtonText='Update Profile'
          onFinish={handleSubmit}
        />
      </Card>
    </div>
  )
}
