import { Button, Card, Space } from 'antd'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import type { Organization, Person, Project, Task } from '../../store'

interface PersonSummaryCardMetaProps {
  className?: string
  person?: Person
  organizations?: Organization[]
  projects?: Project[]
  tasks?: Task[]
}

export const PersonSummaryCardMeta: FC<PersonSummaryCardMetaProps> = ({
  className,
  person,
  organizations,
  projects,
  tasks,
}) => {
  return (
    <div className={className}>
      <Card.Meta
        title={
          <div className='flex text-4xl !text-slate-500'>
            <div className='font-light'>Organizations</div>
            {!!organizations?.length && (
              <Link className='ml-2 font-semibold' to={`/persons/${person?.uuid}/organizations`}>
                {organizations.length}
              </Link>
            )}
          </div>
        }
        avatar={<i className='fas fa-building text-4xl text-green-500' />}
        description={
          <Space wrap>
            {organizations?.map(({ id, name }) => (
              <Link key={id} to={`/organizations/${id}`}>
                <Button type='link'>{name}</Button>
              </Link>
            ))}
          </Space>
        }
      />
      <Card.Meta
        title={
          <div className='flex text-4xl !text-slate-500'>
            <div className='font-light'>Projects</div>
            {!!projects?.length && (
              <Link className='ml-2 font-semibold' to={`/persons/${person?.uuid}/projects`}>
                {projects.length}
              </Link>
            )}
          </div>
        }
        avatar={<i className='fas fa-briefcase text-4xl text-violet-600' />}
        description={
          <Space wrap>
            {projects?.map(({ id, name }) => (
              <Link key={id} to={`/projects/${id}`}>
                <Button type='link'>{name}</Button>
              </Link>
            ))}
          </Space>
        }
      />
      <Card.Meta
        title={
          <div className='flex text-4xl !text-slate-500'>
            <div className='font-light'>Tasks</div>
            {!!tasks?.length && (
              <Link className='ml-2 font-semibold' to={`/persons/${person?.uuid}/tasks`}>
                {tasks.length}
              </Link>
            )}
          </div>
        }
        avatar={<i className='fas fa-tasks text-4xl text-blue-500' />}
        description={
          <Space wrap>
            {tasks?.map(({ id, name }) => (
              <Link key={id} to={`/tasks/${id}`}>
                <Button type='link'>{name}</Button>
              </Link>
            ))}
          </Space>
        }
      />
    </div>
  )
}
