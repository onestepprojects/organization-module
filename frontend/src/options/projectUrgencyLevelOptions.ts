export const projectUrgencyLevelOptions = [
  { label: 'LOW', value: 'LOW' },
  { label: 'MEDIUM', value: 'MEDIUM' },
  { label: 'HIGH', value: 'HIGH' },
  { label: 'CRITICAL', value: 'CRITICAL' },
  { label: 'EMERGENCY', value: 'EMERGENCY' },
]
