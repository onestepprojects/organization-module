package org.onestep.relief.dropwizard.test;

import com.github.javafaker.Address;
import com.github.javafaker.Faker;
import org.onestep.relief.dropwizard.representations.*;

import java.util.*;

public class DataGenerator {
    static Faker fakerService = new Faker();
    static Random rand = new Random();

    public static PersonRepr generatePerson() {
        PersonRepr personRepr = new PersonRepr();

        personRepr.setName(fakerService.name().fullName());
        personRepr.setCurrentAddress(getAddress());
        personRepr.setEmail(fakerService.internet().emailAddress());
        personRepr.setPhones(getPhones());

        personRepr.setBirthdate("1980-04-01");
        personRepr.setProfilePicture(null);
        personRepr.setLastActivity(null);
        personRepr.setHomeAddress(getAddress());

        personRepr.setEcUUID(UUID.randomUUID().toString());
        personRepr.setEcName(fakerService.name().fullName());
        personRepr.setEcPhones(getPhones());
        personRepr.setEcAddress(getAddress());

        personRepr.setPaymentAccount(getAccount());
        personRepr.setLocation(getLocations());
        personRepr.setGender((rand.nextBoolean()) ? "FEMALE" : "MALE");
        personRepr.setSocialMedia(getSocialMedia());

        return personRepr;
    }

    public static OrganizationRepr generateOrganization() {
        OrganizationRepr orgRepr = new OrganizationRepr();

        orgRepr.setName(fakerService.company().name());
        orgRepr.setType("FOR_PROFIT");
        orgRepr.setEmail(fakerService.internet().emailAddress());
        orgRepr.setPhones(getPhones());
        orgRepr.setWebsite(fakerService.internet().url());
        orgRepr.setAddress(getAddress());
        orgRepr.setStatusLabel("INITIATING");
        orgRepr.setDescription(getOrgDescription());
        orgRepr.setSupplier(rand.nextBoolean());
        orgRepr.setSocialMedia(getSocialMedia());
        orgRepr.setPaymentAccount(getAccount());

        return orgRepr;
    }

    private static RepAddress getAddress() {
        RepAddress address = new RepAddress();
        Address fake = fakerService.address();

        address.setAddress(fake.streetAddress());
        address.setCity(fake.city());
        address.setDistrict(fake.state());
        address.setState(fake.state());
        address.setPostalCode(fake.zipCode());
        address.setCountry(fake.country());

        return address;
    }

    private static HashMap<String, String> getPhones() {
        HashMap<String, String> phones = new HashMap<>();
        phones.put("phone", fakerService.phoneNumber().cellPhone());
        phones.put("mobile", fakerService.phoneNumber().cellPhone());
        phones.put("sat_phone", fakerService.phoneNumber().cellPhone());

        return phones;
    }

    private static PaymentAccountRepr getAccount() {
        PaymentAccountRepr account = new PaymentAccountRepr();
        Map<String, Boolean> assets = new HashMap<>();
        assets.put(UUID.randomUUID().toString(), fakerService.bool().bool());
        assets.put(UUID.randomUUID().toString(), fakerService.bool().bool());

        account.setPayID(UUID.randomUUID().toString());
        account.setBlockchainAddress(UUID.randomUUID().toString());
        account.setStatus("AWAITING_USER_ACTION");
        account.setAssets(assets);

        return account;
    }

    private static HashMap<String, RepLocation> getLocations() {
        HashMap<String, RepLocation> locations = new HashMap<>();

        locations.put("location-1", getLocation());
        locations.put("location-2", getLocation());
        locations.put("location-3", getLocation());

        return locations;
    }

    private static HashMap<String, String> getSocialMedia() {
        HashMap<String, String> media = new HashMap<>();
        media.put("url1", fakerService.internet().url());
        media.put("url2", fakerService.internet().url());
        media.put("url3", fakerService.internet().url());

        return media;
    }

    private static RepDescription getOrgDescription() {
        RepDescription desc = new RepDescription();

        desc.setDescription("Random text here...");
        desc.setServiceArea(new ArrayList<RepLocation>() {
            {
                add(getLocation());
                add(getLocation());
                add(getLocation());
            }
        });
        desc.setProjectTypes(new ArrayList<String>() {
            {
                add("ZERO_HUNGER");
                add("EDUCATION");
            }
        });

        return desc;
    }

    private static RepLocation getLocation() {
        RepLocation loc = new RepLocation();
        loc.setLongitude(Float.parseFloat(fakerService.address().longitude()));
        loc.setLatitude(Float.parseFloat(fakerService.address().latitude()));

        return loc;
    }
}
