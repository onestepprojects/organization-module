import { type DataNode } from 'antd/lib/tree'
import { type Organization, type Person, type Project, type Task } from '../../store'

type BaseNode = DataNode & {
  children?: TreeNode[]
  calcChildren?: () => TreeNode[]
}
export type RootNode = BaseNode & {
  type: 'root'
  organizations?: Organization[]
}

export type OrganizationNode = BaseNode & {
  type: 'OrganizationNode'
  organization: Organization
  subOrganizations?: Organization[]
  projects?: Project[]
  persons?: Person[]
}

export type ProjectContainerNode = BaseNode & {
  type: 'ProjectContainerNode'
}

export type ProjectNode = BaseNode & {
  type: 'ProjectNode'
  project: Project
  subProjects?: Project[]
  tasks?: Task[]
  persons?: Person[]
}

export type PersonContainerNode = BaseNode & {
  type: 'PersonContainerNode'
}

export type PersonNode = BaseNode & {
  type: 'PersonNode'
  person: Person
}

export type TaskContainerNode = BaseNode & {
  type: 'TaskContainerNode'
}

export type TaskNode = BaseNode & {
  type: 'TaskNode'
  task: Task
}

export type TreeNode =
  | RootNode
  | OrganizationNode
  | ProjectContainerNode
  | ProjectNode
  | PersonContainerNode
  | PersonNode
  | TaskContainerNode
  | TaskNode
