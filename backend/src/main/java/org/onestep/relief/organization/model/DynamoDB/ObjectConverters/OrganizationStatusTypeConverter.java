package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.OrganizationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type Status to a string and vice-versa.
 */

public class OrganizationStatusTypeConverter implements DynamoDBTypeConverter<String, OrganizationStatus> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(OrganizationStatus status) {
        return DynamoDBTypeConverterHelper.getInstance().convert(status, "status", log);
    }

    @Override
    public OrganizationStatus unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        OrganizationStatus status;
        try {
            status = objectMapper.readValue(s, OrganizationStatus.class);
            log.debug("unmarshalled status to " + status);
        } catch (IOException e) {
            log.error("error de-marshalling status: " + s, e);
            return null;
        }

        return status;
    }
}
