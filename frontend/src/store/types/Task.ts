export interface Task {
  id: string
  name: string
  description: string
  status: 'NEW' | string
  visibility: 'PROTECTED' | string
  attachments: any[]
  assignedPerson: string
  creationDate: string
  dueDate: string
  completionDate: string
}
