import { render, screen } from '@testing-library/react'
import { wrapper } from '../../tests'
import { DashboardCard } from './DashboardCard'

describe('DashboardCard', () => {
  it('renders correctly', () => {
    render(<DashboardCard logo='logo' title='title' />)

    expect(screen.getByText('logo')).toBeInTheDocument()
    expect(screen.getByText('title')).toBeInTheDocument()
  })

  it('renders loading', () => {
    render(<DashboardCard loading logo='logo' title='title' />)

    expect(screen.getByText('loading...')).toBeInTheDocument()
  })

  it('renders empty text', () => {
    render(<DashboardCard emptyText='no data' logo='logo' title='title' />)

    expect(screen.getByText('no data')).toBeInTheDocument()
  })

  it('renders count', () => {
    render(<DashboardCard count={123} link='/organizations' logo='logo' title='title' />, {
      wrapper,
    })

    expect(screen.getByText('123')).toBeInTheDocument()
    expect(screen.getByText('Total')).toBeInTheDocument()
    expect(screen.getByRole('link')).toBeInTheDocument()
    expect(screen.getByRole<HTMLAnchorElement>('link', { name: 'view' }).href).toMatch(
      '/organizations'
    )
  })

  it('renders description', () => {
    render(<DashboardCard description='description' logo='logo' title='title' />)

    expect(screen.getByText('description')).toBeInTheDocument()
  })

  it('renders action', () => {
    render(
      <DashboardCard
        action={{ text: 'action', link: '/organizations' }}
        logo='logo'
        title='title'
      />,
      {
        wrapper,
      }
    )

    expect(screen.getByRole<HTMLAnchorElement>('link', { name: 'action' }).href).toMatch(
      '/organizations'
    )
  })
})
