import { Card, type CardProps } from 'antd'
import classNames from 'classnames'
import { type FC } from 'react'
import {
  SocialMediaLinkList,
  type SocialMediaLinkListProps,
} from '../SocialMediaLinkList/SocialMediaLinkList'

export interface SocialMediaCardProps extends CardProps, SocialMediaLinkListProps {}

export const SocialMediaCard: FC<SocialMediaCardProps> = ({
  className,
  socialMedia,
  emptyText = 'No social media',
  ...restProps
}) => {
  return (
    <Card className={classNames('rounded-xl', className)} title='Social Media' {...restProps}>
      <SocialMediaLinkList socialMedia={socialMedia} emptyText={emptyText} />
    </Card>
  )
}
