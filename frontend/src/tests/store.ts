import { configureStore, createSlice } from '@reduxjs/toolkit'

export const store = configureStore({
  reducer: {
    auth: createSlice({
      name: 'auth',
      initialState: {
        person: {
          uuid: 'personId',
        },
      },
      reducers: {},
    }).reducer,

    api: createSlice({
      name: 'api',
      initialState: {},
      reducers: {},
    }).reducer,
  },
})
