package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

public class LocationMapTypeConverter implements DynamoDBTypeConverter<String, Map<String, Location>> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(Map<String, Location> locations) {
        return DynamoDBTypeConverterHelper.getInstance().convert(locations, "locations", log);
    }

    @Override
    public Map<String, Location> unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Location> locationMap;

        try {
            locationMap = objectMapper.readValue(s, new TypeReference<Map<String, Location>>() {
            });
            log.debug("unmarshalled address to " + locationMap);
        } catch (IOException e) {
            log.error("error de-marshalling address: " + s, e);
            return null;
        }
        return locationMap;
    }
}
