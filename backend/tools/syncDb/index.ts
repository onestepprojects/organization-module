import {syncDb} from './syncDb'
import minimist, {ParsedArgs} from 'minimist';

const argv = process.argv.slice(2);

const {profile} = minimist<ParsedArgs & { profile: string, }>(argv, {
  string: ['profile'],
  default: {
    profile: process.env.AWS_PROFILE,
  },
  unknown: () => false,
});

syncDb(profile);


