package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.Description;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type Description to a string and vice-versa.
 */

public class DescriptionTypeConverter implements DynamoDBTypeConverter<String, Description> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(Description description) {
        return DynamoDBTypeConverterHelper.getInstance().convert(description, "description", log);
    }

    @Override
    public Description unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        Description description;

        try {
            description = objectMapper.readValue(s, Description.class);
            log.debug("unmarshalled description to " + description);
        } catch (IOException e) {
            log.error("error de-marshalling description: " + s, e);
            return null;
        }

        return description;
    }
}
