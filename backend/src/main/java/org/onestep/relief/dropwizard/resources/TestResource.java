package org.onestep.relief.dropwizard.resources;

import com.google.inject.Inject;
import org.dozer.DozerBeanMapper;
import org.onestep.relief.dropwizard.representations.OrganizationRepr;
import org.onestep.relief.dropwizard.representations.PersonRepr;
import org.onestep.relief.dropwizard.test.CognitoResponse;
import org.onestep.relief.dropwizard.test.DataGenerator;
import org.onestep.relief.dropwizard.test.TokenService;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.model.Organization;
import org.onestep.relief.organization.model.Person;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;
import org.onestep.relief.organization.service.OrganizationServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("/tests")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
public class TestResource {
    private final TokenService tokenService;
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;

    @Inject
    public TestResource(OrganizationServiceImpl orgInterface, TokenService tokenService) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        dtoMapper = mapper;

        orgInterface.testingMode = true;
        this.orgInterface = orgInterface;
        this.tokenService = tokenService;
    }

    @GET
    @Path("/token")
    public Response getAuthenticationToken() {
        return Response.status(Response.Status.OK).entity(tokenService.getAuthenticationToken()).build();
    }

    @GET
    @Path("/display-tokens")
    public Response getAllTokens() {
        return Response.status(Response.Status.OK).entity(tokenService.getCognitoPairs()).build();
    }

    @GET
    @Path("/cognito-id")
    public Response getCognitoId(@HeaderParam("Authorization") Optional<String> authToken) {
        Optional<String> cognitoId = tokenService.getPermanentId(authToken.orElse("UNKNOWN"));

        if (!cognitoId.isPresent())
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, "Unable to find Cognito ID");

        CognitoResponse response = new CognitoResponse();
        response.getData().setUserId(cognitoId.get());

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @GET
    @Path("/generate-person")
    public Response generatePersons(@QueryParam("number") Integer number) {
        try {
            int number_ = (number != null) ? number : 1;
            List<PersonRepr> persons = new ArrayList<>();

            for (int i = 0; i < number_; i++) {
                String authToken = tokenService.getAuthenticationToken();
                Person newPerson = dtoMapper.map(DataGenerator.generatePerson(), Person.class);

                Person created = orgInterface.createPerson(authToken, newPerson);
                persons.add(dtoMapper.map(created, PersonRepr.class));
            }

            return Response.status(Response.Status.OK).entity(persons).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }

    @GET
    @Path("/generate-organization")
    public Response generateOrganizations(@QueryParam("number") Integer number) {
        try {
            int number_ = (number != null) ? number : 1;
            List<OrganizationRepr> organizations = new ArrayList<>();

            for (int i = 0; i < number_; i++) {
                String token = tokenService.getAnyAuthenticationToken();
                Organization org = dtoMapper.map(DataGenerator.generateOrganization(), Organization.class);

                Organization created = orgInterface.createOrganization(token, org);
                organizations.add(dtoMapper.map(created, OrganizationRepr.class));
            }

            return Response.status(Response.Status.OK).entity(organizations).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.BAD_REQUEST, e.getMessage());
        }
    }
}
