import { Button, Descriptions, Popconfirm } from 'antd'
import dayjs from 'dayjs'
import { lowerCase } from 'lodash-es'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import type { Person, Task } from '../../store'
import { PersonAvatar } from '../PersonAvatar/PersonAvatar'

export interface TaskDescriptionsProps {
  title?: string
  task: Task
  assignedPerson: Person
  editLink?: string
  onDelete?: () => Promise<void>
}

export const TaskDescriptions: FC<TaskDescriptionsProps> = ({
  title,
  task,
  assignedPerson,
  editLink,
  onDelete,
}) => {
  return (
    <Descriptions
      title={title || task.name}
      size='small'
      column={1}
      labelStyle={{ width: 96 }}
      extra={
        <div className='flex justify-center items-center gap-4'>
          {editLink && (
            <Link to={editLink}>
              <Button type='primary' size='small' ghost icon={<i className='mr-2 fas fa-edit' />}>
                Edit
              </Button>
            </Link>
          )}
          {
            // TODO: How to detect if the user is admin of this task?
            onDelete && (
              <Popconfirm
                title='Are you sure you wish to delete this task?'
                okText='Delete'
                okButtonProps={{ danger: true }}
                onConfirm={onDelete}
              >
                <Button
                  type='primary'
                  size='small'
                  ghost
                  danger
                  icon={<i className='mr-2 fas fa-trash' />}
                >
                  Delete
                </Button>
              </Popconfirm>
            )
          }
        </div>
      }
    >
      <Descriptions.Item label='Assigned to'>
        <PersonAvatar person={assignedPerson} />
      </Descriptions.Item>
      <Descriptions.Item label='Create'>
        {task.creationDate ? dayjs(Number(task.creationDate)).format('YYYY-MM-DD hh:mm') : 'N/A'}
      </Descriptions.Item>
      <Descriptions.Item label='Description'>{task.description}</Descriptions.Item>
      <Descriptions.Item label='Status'>{lowerCase(task.status)}</Descriptions.Item>
    </Descriptions>
  )
}
