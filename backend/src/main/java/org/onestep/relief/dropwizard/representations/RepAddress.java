package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RepAddress {
    @Pattern(regexp = ".+ .+")
    private String address;

    @Size(min = 3)
    private String city;

    @Size(min = 1)
    private String district;

    @Size(min = 2)
    private String state;

    @Size(min = 2)
    private String postalCode;

    @Size(min = 2)
    private String country;
}
