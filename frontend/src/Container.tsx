import { useEffect, type FC } from 'react'
import { Route, Routes } from 'react-router-dom'
import { authSlice, useAppDispatch, type Person } from './store'
import {
  Dashboard,
  Invite,
  Join,
  NotFound,
  OrganizationCreate,
  OrganizationDetail,
  OrganizationList,
  OrganizationTree,
  OrganizationUpdate,
  PersonCreate,
  PersonDetail,
  PersonList,
  PersonUpdate,
  ProjectCreate,
  ProjectDetail,
  ProjectList,
  ProjectUpdate,
  RequestList,
  TaskCreate,
  TaskList,
  TaskUpdate,
} from './views'

export interface OrganizationContainerProps {
  auth: {
    token?: string
    person?: Person
    roles?: string[]
  }
}

export const OrganizationContainer: FC<OrganizationContainerProps> = ({ auth }) => {
  const dispatch = useAppDispatch()

  useEffect(() => {
    if (!auth) return
    dispatch(authSlice.actions.setAuth(auth))
  }, [auth, dispatch])

  return (
    <div id='organizationContainer' className='mx-auto container min-h-[calc(100vh-120px)]'>
      <Routes>
        <Route path='/' element={<Dashboard />} />

        <Route path='/organizations' element={<OrganizationList />} />
        <Route path='/organizations/tree' element={<OrganizationTree />} />
        <Route path='/organizations/create' element={<OrganizationCreate />} />
        <Route path='/organizations/:organizationId' element={<OrganizationDetail />} />
        <Route path='/organizations/:organizationId/update' element={<OrganizationUpdate />} />
        <Route path='/organizations/:organizationId/invite' element={<Invite />} />
        <Route path='/organizations/:organizationId/join' element={<Join />} />
        <Route path='/organizations/:organizationId/requests' element={<RequestList />} />
        <Route path='/organizations/:organizationId/persons' element={<PersonList />} />
        <Route path='/organizations/:organizationId/projects' element={<ProjectList />} />
        <Route path='/organizations/:organizationId/organizations' element={<OrganizationList />} />
        <Route
          path='/organizations/:organizationId/organizations/create'
          element={<OrganizationCreate />}
        />

        <Route path='/persons' element={<PersonList />} />
        <Route path='/persons/create' element={<PersonCreate />} />
        <Route path='/registration' element={<PersonCreate />} />
        <Route path='/persons/:personId' element={<PersonDetail />} />
        <Route path='/persons/:personId/update' element={<PersonUpdate />} />
        <Route path='/persons/:personId/organizations' element={<OrganizationList />} />
        <Route path='/persons/:personId/projects' element={<ProjectList />} />
        <Route path='/persons/:personId/tasks' element={<TaskList />} />

        <Route path='/projects' element={<ProjectList />} />
        <Route path='/projects/create' element={<ProjectCreate />} />
        <Route path='/projects/:projectId' element={<ProjectDetail />} />
        <Route path='/projects/:projectId/update' element={<ProjectUpdate />} />
        <Route path='/projects/:projectId/invite' element={<Invite />} />
        <Route path='/projects/:projectId/join' element={<Join />} />
        <Route path='/projects/:projectId/requests' element={<RequestList />} />
        <Route path='/projects/:projectId/projects' element={<ProjectList />} />

        <Route path='/tasks/create' element={<TaskCreate />} />
        <Route path='/tasks/:taskId/update' element={<TaskUpdate />} />

        <Route path='*' element={<NotFound />} />
      </Routes>
    </div>
  )
}
