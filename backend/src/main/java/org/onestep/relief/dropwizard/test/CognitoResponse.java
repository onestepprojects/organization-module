package org.onestep.relief.dropwizard.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CognitoResponse {
    private String message = "Random message here...";
    private CognitoData data = new CognitoData();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CognitoData {
        private String userId;
        private String username = "john";
        private List<String> roles = new ArrayList<>();
    }
}