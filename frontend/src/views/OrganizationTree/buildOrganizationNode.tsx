import { ClusterOutlined, ContainerOutlined, TeamOutlined } from '@ant-design/icons'
import { Space } from 'antd'
import { type Organization } from '../../store'
import { buildPersonNode } from './buildPersonNode'
import { buildProjectNode } from './buildProjectNode'
import { type OrganizationNode, type TreeNode } from './typings'

export function buildOrganizationNode(organization: Organization): OrganizationNode {
  const node: OrganizationNode = {
    type: 'OrganizationNode',
    organization,
    key: organization.id,
    title: (
      <Space>
        <ClusterOutlined className='text-pink-500' />
        <span>{organization.name}</span>
      </Space>
    ),
    calcChildren(): TreeNode[] | undefined {
      if (node.subOrganizations === undefined || node.projects === undefined) {
        return undefined
      }
      return [
        ...node.subOrganizations.map((it) => buildOrganizationNode(it)),
        ...[
          node.projects.length && {
            type: 'ProjectContainerNode',
            title: (
              <Space>
                <ContainerOutlined className='text-blue-500' />
                <span>Projects</span>
              </Space>
            ),
            key: `${organization.id}-projects`,
            children: node.projects.map((it) => buildProjectNode(it)),
          },
        ].filter(Boolean),
        ...[
          node.persons.length && {
            type: 'PersonsContainerNode',
            title: (
              <Space>
                <TeamOutlined className='text-yellow-500' />
                <span>Persons</span>
              </Space>
            ),
            key: `${organization.id}-persons`,
            children: node.persons.map((it) => buildPersonNode(it)),
          },
        ].filter(Boolean),
      ] as TreeNode[]
    },
  }

  return node
}
