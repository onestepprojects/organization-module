import { Card } from 'antd'
import { type FC } from 'react'
import { Maps } from '../../components'
import { type Project } from '../../store'

export interface ProjectMapsCardProps {
  loading?: boolean
  project: Project
}

export const ProjectMapsCard: FC<ProjectMapsCardProps> = ({ loading, project }) => {
  if (project?.targetArea?.area_1) {
    return (
      <Card
        className='bg-transparent'
        bordered={false}
        bodyStyle={{ padding: 0 }}
        cover={
          <Maps
            className='w-full h-96 rounded-lg'
            center={[project.targetArea.area_1.latitude, project.targetArea.area_1.longitude]}
            zoom={8}
          />
        }
      />
    )
  }

  return (
    <Card className='rounded-lg' title='Location' loading={loading}>
      <div className='h-full flex flex-col justify-center items-center'>
        <i className='fas fa-map-marker-alt fa-4x' />
        <div>No location</div>
      </div>
    </Card>
  )
}
