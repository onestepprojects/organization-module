package org.onestep.relief.dropwizard.resources;

import javax.validation.ConstraintViolation;
import java.util.Set;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class Utils {

    /**
     * Assumes there is at least 1 violation in the Set.
     *
     * @param set_
     *            set of violations
     *
     * @return string representation of the violation
     */
    public static <T> String getViolation(Set<ConstraintViolation<T>> set_) {
        ConstraintViolation<T> violation = set_.iterator().next();

        return violation.getPropertyPath().toString() + ": " + violation.getMessage();
    }

    public static Response buildBadResponse(Status status, String msn) {
        return Response.status(status).entity(msn).build();
    }
}
