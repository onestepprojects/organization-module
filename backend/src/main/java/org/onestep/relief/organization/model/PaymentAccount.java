package org.onestep.relief.organization.model;

import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * A PaymentAccount represents a Person or Organization's Pago account details, including which assets they have
 * complete the provisioning process for.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentAccount {
    String payID;
    String blockchainAddress;
    ProvisionStatus status;
    Map<String, Boolean> assets = new HashMap<>();

    public enum ProvisionStatus {
        AWAITING_USER_ACTION, PARTIALLY_PROVISIONED, PROVISIONED, FAILED
    }

    /**
     * Sets the value of the specified asset ID and updates the payment account's status accordingly. Asset ID must
     * already exist within assets Map.
     *
     * @param assetID
     *            - id of the asset to mark as provisioned
     *
     * @throws NoSuchElementException
     *             - thrown if the specified asset does not exist
     */
    public void provisionAsset(String assetID) throws NoSuchElementException {
        if (!assets.containsKey(assetID)) {
            throw new NoSuchElementException("Specified asset does not exist");
        } else {
            // mark the asset as provisioned
            assets.put(assetID, true);

            // check whether any of the assets still need to be provisioned
            boolean isPartiallyProvisioned = false;
            for (Map.Entry<String, Boolean> asset : assets.entrySet()) {
                if (!asset.getValue()) {
                    isPartiallyProvisioned = true;
                    break;
                }
            }

            // set the payment account status
            if (isPartiallyProvisioned) {
                status = ProvisionStatus.PARTIALLY_PROVISIONED;
            } else {
                status = ProvisionStatus.PROVISIONED;
            }
        }
    }
}
