package org.onestep.relief.organization.model;

import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CognitoUser {
    public static final String GLOBAL_ADMIN = "admin";

    private String message;
    private CognitoUserData data;
    private String personId;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class CognitoUserData {
        private String userId;
        private String username;
        private List<String> roles;
    }

    public boolean isGlobalAdmin() {
        return this.getData().getRoles().contains(GLOBAL_ADMIN);
    }
}
