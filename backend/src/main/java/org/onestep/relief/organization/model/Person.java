package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.DynamoDB.ObjectConverters.*;

import java.util.*;

/**
 * A person participating on the OneStep platform.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "Person")
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Person", description = "Persons")
@Builder
public class Person extends BasePerson {

    @DynamoDBAttribute
    private Date birthdate;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = FileTypeConverter.class)
    private File profilePicture = new File();

    @DynamoDBAttribute
    private Map<String, String> socialMedia;

    @DynamoDBAttribute
    private Date lastActivity;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = EventListTypeConverter.class)
    private List<Event> history = new ArrayList<>();

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = AddressTypeConverter.class)
    private Address homeAddress;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = NonMemberPersonConverter.class)
    private NonMemberPerson emergencyContact;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = PaymentAccountTypeConverter.class)
    private PaymentAccount paymentAccount = new PaymentAccount();

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = LocationMapTypeConverter.class)
    private Map<String, Location> location;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private Gender gender;

    @DynamoDBAttribute
    private String cognitoID;

    @DynamoDBAttribute
    private boolean isActive = true;

    @DynamoDBIgnore
    private boolean isRequesterSelf = false;

    public enum Gender {
        MALE, FEMALE, OTHER
    }

    public Person(String id, String name, Address address, String email, Map<String, String> phones, Gender gender) {
        super(id, name, address, email, phones, true);
        this.gender = gender;
    }
}