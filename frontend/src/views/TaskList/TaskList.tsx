import { Badge, Card, Space, Spin } from 'antd'
import { lowerCase } from 'lodash-es'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { PageTitle, ServerError } from '../../components'
import { useGetTasksByPersonQuery } from '../../store'

const getTaskStatusColor = (status: string) => {
  return (
    {
      NEW: 'blue',
      IN_PROGRESS: 'pink',
      COMPLETED: 'green',
    }[status] || '#999'
  )
}

export const TaskList: FC = () => {
  const { personId } = useParams<{ personId?: string }>()
  const { isLoading, data, error } = useGetTasksByPersonQuery(personId)

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>My Tasks</PageTitle>

      {error ? (
        <ServerError />
      ) : (
        <Spin spinning={isLoading}>
          <Space className='mb-8' wrap size={['large', 48]}>
            {data?.map((task) => (
              <Badge.Ribbon
                key={task.id}
                text={lowerCase(task.status)}
                color={getTaskStatusColor(task.status)}
              >
                <Card className='w-64 rounded' size='small' title={task.name}>
                  <Card.Meta description={task.description} />
                </Card>
              </Badge.Ribbon>
            ))}
          </Space>
        </Spin>
      )}
    </div>
  )
}
