package org.onestep.relief.organization.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class File {
    String name;
    String bucketName;
    String url;
    String caption;
    FileFormat format;
    int size;
    Date creationDate = Calendar.getInstance().getTime();
    String creator;
    String body;

    public enum FileFormat {
        JPG("image/jpeg"), PNG("image/png");

        private final String contentType;

        FileFormat(String contentType) {
            this.contentType = contentType;
        }

        public String getContentType() {
            return contentType;
        }

        public static FileFormat valueFromContentType(String contentType) {
            return Arrays.stream(FileFormat.values()).filter(it -> it.contentType.equals(contentType)).findFirst().orElse(null);
        }
    }
}
