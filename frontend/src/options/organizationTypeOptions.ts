export const organizationTypeOptions = [
  { label: 'For Profit', value: 'FOR_PROFIT' },
  { label: 'Non Profit', value: 'NON_PROFIT' },
  { label: 'Foundation', value: 'FOUNDATION' },
  { label: 'Governmental', value: 'GOVERNMENTAL' },
  { label: 'Educational', value: 'EDUCATIONAL' },
  { label: 'Hybrid', value: 'HYBRID' },
]
