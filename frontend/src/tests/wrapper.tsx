import { type FC, type ReactElement, type ReactNode } from 'react'
import { Provider } from 'react-redux'
import { MemoryRouter, type MemoryRouterProps } from 'react-router-dom'
import { store } from './store'

/**
 * Wraps a component in a `MemoryRouter` and `Provider` to provide the `store`.
 *
 * Could be pass to `@testing-library/react` render options. Or call `wrapper` function to pass
 * memory router props.
 *
 * @example
 *   render(<MyComponent />, { wrapper })
 *   render(
 *     <Route path='/users/:id'>
 *       <MyComponent />
 *     </Route>,
 *     { wrapper: wrapper({ initialEntries: ['/users/1'] }) }
 *   )
 */
export function wrapper(options?: { children?: ReactNode }): ReactElement
export function wrapper(options?: MemoryRouterProps): FC
export function wrapper(options?: MemoryRouterProps | { children?: ReactNode }) {
  const Wrapper: FC<MemoryRouterProps> = ({ children, ...routerProps }) => (
    <MemoryRouter {...routerProps}>
      <Provider store={store}>{children}</Provider>
    </MemoryRouter>
  )

  if (options?.children) {
    return <Wrapper {...options} />
  }

  const inner: FC = ({ children }) => <Wrapper {...options}>{children}</Wrapper>

  return inner
}
