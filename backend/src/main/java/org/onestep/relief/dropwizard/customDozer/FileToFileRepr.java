package org.onestep.relief.dropwizard.customDozer;

import org.dozer.CustomConverter;
import org.onestep.relief.dropwizard.representations.FileRepr;
import org.onestep.relief.organization.model.File;

public class FileToFileRepr implements CustomConverter {
    @Override
    public Object convert(Object dest, Object source, Class<?> aClass, Class<?> aClass1) {
        if (source == null)
            return null;

        if (source instanceof FileRepr) {
            FileRepr rep = (FileRepr) source;
            File file = new File();

            file.setName(rep.getName());
            file.setCaption(rep.getCaption());

            if (rep.getFormat() != null)
                file.setFormat(File.FileFormat.valueFromContentType(rep.getFormat()));

            file.setSize(rep.getSize());
            file.setBody(rep.getBody());

            return file;

        } else if (source instanceof File) {
            File file = (File) source;
            FileRepr rep = new FileRepr();

            rep.setName(file.getName());
            rep.setBucketName(file.getBucketName());
            rep.setUrl(file.getUrl());
            rep.setCaption(file.getCaption());

            if (file.getFormat() != null)
                rep.setFormat(file.getFormat().toString());

            rep.setSize(file.getSize());
            rep.setCreationDate(Utils.dateToTimestamp(file.getCreationDate()));
            rep.setCreator(file.getCreator());
            rep.setBody(file.getBody());

            return rep;

        } else
            return null;
    }
}
