package org.onestep.relief.dropwizard;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TestMain {
    public static void main(String[] args) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        System.out.println(ts.getTime());
        System.out.println(String.valueOf(ts.getTime()));

        Date date = new Date(Long.parseLong("1614409888300"));

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        System.out.println(format.format(date));

        long sss = new Timestamp(date.getTime()).getTime();

        System.out.println(String.valueOf(sss));
    }
}
