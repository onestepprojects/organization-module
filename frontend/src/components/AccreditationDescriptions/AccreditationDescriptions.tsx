import { Button } from 'antd'
import classNames from 'classnames'
import { type FC } from 'react'
import { type AccreditationEntity } from '../../store'

export interface AccreditationDescriptionsProps {
  className?: string
  accreditationEntities?: AccreditationEntity[]
}

export const AccreditationDescriptions: FC<AccreditationDescriptionsProps> = ({
  className,
  accreditationEntities,
}) => {
  const approvedAccreditationRoles = accreditationEntities
    ?.filter(({ accreditationState }) => accreditationState === 'Approved')
    .map(({ role }) => role)
  const pendingAccreditationRoles = accreditationEntities
    ?.filter(({ accreditationState }) => accreditationState === 'Pending')
    .map(({ role }) => role)

  return (
    <div className={classNames('text-slate-600', className)}>
      <div>
        {approvedAccreditationRoles?.length ? (
          <span>Accreditations: {approvedAccreditationRoles.join(',')}</span>
        ) : (
          <span>No Accreditations</span>
        )}
      </div>
      {!!pendingAccreditationRoles?.length && (
        <div>
          <span>Pending Accreditations: {pendingAccreditationRoles.join(',')}</span>
        </div>
      )}
      {!!accreditationEntities?.length && (
        <Button
          className='px-0'
          type='link'
          href={`/accreditation?entityTpe=${accreditationEntities[0].entityType}&entityId=${accreditationEntities[0].entityId}`}
        >
          View Accreditations
        </Button>
      )}
    </div>
  )
}
