# **OneStep Organization, Person, and Project Module**

## Table of Contents

- [**OneStep Organization, Person, and Project Module**](#onestep-organization-person-and-project-module)
  - [Table of Contents](#table-of-contents)
  - [Team Members](#team-members)
  - [Product Owner](#product-owner)
  - [Faculty](#faculty)
  - [Technologies](#technologies)
  - [Project Overview](#project-overview)
  - [Organization Module](#organization-module)
    - [Frontend](#frontend)
    - [Backend](#backend)
  - [Backend REST API](#backend-rest-api)

## Team Members

- Ethan Anthony
- Claudio Bustamante
- Alex Friberg
- Antony Gavidia
- Mariam Gogia
- Pawel Nawrocki

## Product Owner

- Nitya Timalsina

## Faculty

- Annie Kamlang
- Eric Gieseke
- Hannah Riggs

## Technologies

- Programming Languages
  - Java
- Data Persistence
  - DynamoDB
  - S3
- RESTful API
  - DropWizard
- UI
  - React
  - AntD
  - tailwindcss
- IDE
  - IntelliJ
  - VSCode
- Server Environment
  - AWS

## Project Overview

The Organization, Project, and Person (OPP) Module acts as an entry point to the OneStep Relief Platform. From initial registration to organization and project setup, the OPP module also serves as a functional communication hub for all relief effort activity. The OPP module provides the infrastructure to represent and allow interaction between organizations, persons, and projects, gives them representation.

## Organization Module

This repository has two primary sections:

### Frontend

This section contains the React user interface.

We use the [React](https://reactjs.org/) to build the frontend. Embrace hooks. Written in TypeScript.

We use the [tailwindcss](https://tailwindcss.com/) as css utilities. Recommended installation [VSCode Tailwind CSS IntelliSense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss). Or [WebStorm Tailwind CSS](https://www.jetbrains.com/help/webstorm/tailwind-css.html).

### Backend

This section contains the Dropwizard REST API called by the frontend, the Java model class files, and the annotations, mappers and services used to access DynamoDB.

## Backend REST API

```
ROOT = https://test.onesteprelief.org/onestep/org/

********
PERSON:
********

GET     -> /persons
GET     -> /persons/{id}                        @header:{'Authorization': 'token'}
GET     -> /persons/requester/token             @header:{'Authorization': 'token'}
GET     -> /persons/get-by-parent/{parentId}
GET     -> /persons/by-cognito/{cognitoId}      @header:{'Authorization': 'token'}
POST    -> /persons/create                      @header:{'Authorization': 'token'}
POST    -> /persons/update                      @header:{'Authorization': 'token'}
POST    -> /persons/create-event/{personId}     @header:{'Authorization': 'token'} *see Event payload
DELETE  -> /persons/delete/{personId}           @header:{'Authorization': 'token'}
POST    -> /persons/update-image/{personId}     @header:{'Authorization': 'token'}
GET     -> /persons/provision-asset/{personId}/{assetId}

  PAYLOAD:

    {
      "uuid":"641d9c4f-452b-495f-b638-66cc935ccbb3",

      @NotNull
      "name":"Master Tester",

      @NotNull
      # All fields must be filled
      "currentAddress":{
          "address":"5021 Taylor Parkways",
          "city":"Robertachester",
          "district":"Brittanyview",
          "state":"Connecticut",
          "postalCode":"42534",
          "country":"Azerbaijan"
      },

      "email":"old_master_tester@testing.com",

      @NotNull
      "phones"{
          "phone":"415-555-5647",
          "mobile":"+1-160-351-7881x379",
          "sat_phone":"(767)592-0693x4324"
      },

      "birthdate":"1945-07-26",

      # see File Json below
      "profilePicture": ...,

      "lastActivity":"1616128724970",

      "homeAddress":null,

      "ecUUID":"0ecf445e-b402-4488-9af2-c2871b95a842",

      "ecName":"Bethany Horton",

      "ecPhones":{},

      "ecAddress":null,

      "paymentAccount": {
          "payID": "123456789",
          "blockchainAddress": "7894561623",

          # options: AWAITING_USER_ACTION|PARTIALLY_PROVISIONED|PROVISIONED|FAILED
          "status": "FAILED"
          "assets": {"asset1": true, "asset2": true}
      },

      "location":{},

      "gender":"MALE",

      "socialMedia": {}
    }


**************
ORGANIZATION:
**************

GET     -> /organizations
GET     -> /organizations/{id}                                        @header:{'Authorization': 'token'}
GET     -> /organizations/get-sub/{parentId}
GET     -> /organizations/get-org-by-person/{parentId}
GET     -> /organizations/get-org-roles-by-person/{parentId}
GET     -> /organizations/get-parent/{childId}                        @header:{'Authorization': 'token'}
GET     -> /organizations/add-sub-organization/{parentOrgId}/{orgId}  @header:{'Authorization': 'token'}
POST    -> /organizations/create                                      @header:{'Authorization': 'token'}
POST    -> /organizations/update                                      @header:{'Authorization': 'token'}
DELETE  -> /organizations/delete/{orgId}                              @header:{'Authorization': 'token'}
POST    -> /organizations/update-image/{organizationId}               @header:{'Authorization': 'token'}
GET     -> /organizations/provision-asset/{orgId}/{assetId}

  PAYLOAD:

    {
      "id":"641d9c4f-452b-495f-b638-66cc935ccbb3",

      @NotNull
      "name":"Johnson-Moore",Healthy

      # Options: "FOR_PROFIT|NON_PROFIT|FOUNDATION|GOVERNMENTAL|EDUCATIONAL|HYBRID"
      "type":"FOR_PROFIT",


      "tagline":"Responsive and tactical firm specializing",

      # see File Json below
      "logo": ...,

      "email":"cthomas@hotmail.com",

      @NotNull
      "phones":{"sat_phone":"294-476-0703x758"},

      "website":"https://www.eaton.net/",

      @NotNull
      "address":{
          "address":"Unit 6704 Box 1339",
          "city":"West Jessica",
          "district":"Davismouth",
          "state":"Virginia",
          "postalCode":"96168",
          "country":"Iceland"
      },

      "creatorUUID":"318b6231-1d63-4133-b482-717h4zd5bbbf",

      # Options: "INITIATING|ACTIVE|INACTIVE|PAUSED|CLOSED"
      "statusLabel":"ACTIVE",

      # timestamp with 13 digits (if any)
      "statusLastActive":null,

      "description":{
          "description":"Character sign bad i",
          "serviceArea":[{"latitude":-45.33845,"longitude":51.706913}],
          "projectTypes":["still","wall"]
      },

      "isSupplier": true,

      "requesterRoles": null,

      "socialMedia": {},

      "paymentAccount": {
          "payID": "123456789",
          "blockchainAddress": "7894561623",

          # options: AWAITING_USER_ACTION|PARTIALLY_PROVISIONED|PROVISIONED|FAILED
          "status": "FAILED"
          "assets": {"asset1": true, "asset2": true}
      }
    }


*********
PROJECT:
*********

GET     -> /projects
POST    -> /projects/create                         @header:{'Authorization': 'token'}
POST    -> /projects/update                         @header:{'Authorization': 'token'}
POST    -> /projects/create-status/{projectId}      @header:{'Authorization': 'token'}
GET     -> /projects/{id}                           @header:{'Authorization': 'token'}
GET     -> /projects/sub-projects/{parentId}
GET     -> /projects/projects-by-person/{personId}
DELETE  -> /projects/delete/{projectId}             @header:{'Authorization': 'token'}
POST    -> /projects/update-image/{projectId}       @header:{'Authorization': 'token'}

  PAYLOAD:

    {
      # Options: "PROJECT|ORGANIZATION"
      "parentType":null,

      "parentId":null,

      "id":"a9356057-5743-4e39-9eb2-24555615056d",

      @NotNull
      "name":"New Project Here",

      @NotNull
      "description":"This is for a new project",

      "targetArea":{"area_1":{"latitude":140.03479,"longitude":-118.23407},"area_2":{"latitude":-166.83241,"longitude":30.819183}},

      # *projectTYpes*
      "primaryType":null,
      "secondaryType":null,

      "tagline":null,

      # see File Json below
      "profilePicture": null,

      @NotNull
      "creationDate":"1616392207607",

      "closeDate":"1616558400000",

      "statusUpdatesHistory":[{
          "authorID":"7dac717d-92a1-4cb0-bba1-c92f1d8850f9",
          "date":"1615191748781",
          "title":"This is title",
          "description":"This is a dummy description"
      }],

      "partnerOrganizations":{},

      @NotNull
      # options: LOW|MEDIUM|HIGH|CRITICAL|EMERGENCY
      "urgencyLevel":"MEDIUM",

      @NotNull
      # options: PENDING|INITIATED|ASSESSMENT|DESIGN|IMPLEMENTATION|FOLLOWUP|COMPLETED|CLOSED
      "projectStage": "PENDING",

      "isHealth": false,

      "requesterRoles":[],

      "socialMedia": {}
    }

projectTYpes:   "POVERTY_ALLEVIATION|"
                "ZERO_HUNGER|"
                "HEALTH_AND_WELL_BEING|"
                "EDUCATION|"
                "GENDER_EQUALITY|"
                "CLEAN_WATER_AND_SANITATION|"
                "AFFORDABLE_AND_CLEAN_ENERGY|"
                "ECONOMIC_GROWTH|"
                "INDUSTRY_INNOVATION_AND_INFRASTRUCTURE|"
                "REDUCING_INEQUALITY|"
                "SUSTAINABLE_CITIES_AND_COMMUNITIES|"
                "WASTE_REDUCTION|"
                "CLIMATE_ACTION|"
                "PROTECTING_MARINE_ECOSYSTEMS|"
                "PROTECTING_TERRESTRIAL_ECOSYSTEMS|"
                "PEACE_JUSTICE_AND_STRONG_INSTITUTIONS|"
                "PROMOTING_UN_SUSTAINABLE_DEVELOPMENT_GOALS"


******
TASK:
******

GET     -> /tasks/{taskId}                        @header:{'Authorization': 'token'}
GET     -> /tasks/by-parent/{parentId}
GET     -> /tasks/by-person/{personId}
POST     -> /tasks/create/{projectId}             @header:{'Authorization': 'token'}
POST     -> /tasks/update/{projectId}             @header:{'Authorization': 'token'}
DELETE   -> /tasks/delete/{projectId}/{taskId}    @header:{'Authorization': 'token'}

  PAYLOAD:

    {
    "id": null,

    @NotNull
    "name": "my-task",

    @NotNull
    "description": "any random description here",

    "assignedPerson": 123456789,

    # must be a 13 digits String IF passed
    "creationDate": null,

    # must be a 13 digits String IF passed
    "dueDate": null,

    # must be a 13 digits String IF passed
    "completionDate": null,

    @NotNull
    # options: NEW|IN_PROGRESS|COMPLETED
    "status": "NEW",

    @NotNull
    # options: PUBLIC|PROTECTED|PRIVATE
    "visibility": "PUBLIC",

    # send empty list when creating
    # see File Json below
    "attachmentList": [File JSON]
  }


**********
COMPOSITE:
**********

GET   -> /composite/health
GET   -> /composite/add-role-to-entity/{personToBeAddedId}/{role}/{entityType}/{entityId}         @header:{'Authorization': 'token'}
GET   -> /composite/update-person-role/{oldRole}/{role}/{parentEntityId}/{personId}               @header:{'Authorization': 'token'}
GET   -> /composite/update-person-role-status/{role}/{parentEntityId}/{personId}/{requestStatus}  @header:{'Authorization': 'token'}
GET   -> /composite/role-requests/{parentEntityId}                                                @header:{'Authorization': 'token'}
GET   -> /composite/entity-role-invites/{entityType}                                              @header:{'Authorization': 'token'}

entityType: organization|project
requestStatus: INVITED|REQUESTED|ACTIVE|INACTIVE
role: COMMUNITYLEADER|COMMUNITYMEMBER|SPONSOR|DONOR|EXPERT|RELIEFORGADMIN|RELIEFWORKER|HEALTHWORKER|SUPPLIER|ADMIN|GLOBAL_ADMIN|VOLUNTEER


**********
EXTERNAL:
**********
GET   -> /external/reward/{reason}/{amount}/{receiverId}/{receiverType}     @header:{'Authorization': 'token'}


***************
Other Payloads
***************

File (for update-image endpoints)
  {
  "name": "picture.jpeg",

  "caption"" "random text here...",

  # options: image/jpeg|image/png
  "format": "image/png",

  # integer
  "size": 1000,

  # base64 encoding
  "body": "TWFuIGlzIGRpc3Rpbmd1aXNoZW..."
  }

Event Payload
  {
    # must be a 13 digits String IF passed
    "date": ...,

    "message": ...

    # options: *eventActivities
    "activity": ...

    "parentID": ...

    "parentType": ...

    # int
    "rewardPoints": 20
  }

eventActivities =
ORG_CREATED|ORG_UPDATED|PERSON_CREATED|PERSON_UPDATED|PROJECT_CREATED|
PROJECT_UPDATED|TASK_CREATED|TASK_UPDATED|TASK_COMPLETED|ROLE_ADDED|
ROLE_CHANGED|ROLE_REMOVED|POST|REWARD
```
