package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskRepr {
    private String id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    private String assignedPerson;

    @Pattern(regexp = "[0-9]{13}")
    private String creationDate;

    @Pattern(regexp = "[0-9]{13}")
    private String dueDate;

    @Pattern(regexp = "[0-9]{13}")
    private String completionDate;

    @NotNull
    @Pattern(regexp = "NEW|IN_PROGRESS|COMPLETED")
    private String status = "NEW";

    @NotNull
    @Pattern(regexp = "PUBLIC|PROTECTED|PRIVATE")
    private String visibility;

    private List<FileRepr> attachments;
}
