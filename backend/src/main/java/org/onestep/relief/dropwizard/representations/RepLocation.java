package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class RepLocation {
    @NotNull
    private float latitude;
    @NotNull
    private float longitude;
}
