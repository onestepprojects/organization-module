package org.onestep.relief.dropwizard.resources;

import org.dozer.DozerBeanMapper;
import org.onestep.relief.organization.model.AuthenticationException;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;

import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Path("/external")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
public class ExternalResource {
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;
    private final Validator validator;

    @Inject
    public ExternalResource(OrgServiceInterface orgInterface, Validator validator) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        this.dtoMapper = mapper;
        this.validator = validator;
        this.orgInterface = orgInterface;
    }

    @GET
    @Path("/reward/{reason}/{amount}/{receiverId}/{receiverType}")
    public Response getPersonById(@PathParam("reason") String reason, @PathParam("amount") String amount,
            @PathParam("receiverId") String receiverId, @PathParam("receiverType") String receiverType,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            orgInterface.issueReward(authToken.orElse("UNKNOWN"), reason, Integer.parseInt(amount), receiverId,
                    receiverType);

            return Response.status(Response.Status.OK).entity("DONE").build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Response.Status.NOT_FOUND, e.getMessage());
        }
    }
}
