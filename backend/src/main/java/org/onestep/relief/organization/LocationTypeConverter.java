package org.onestep.relief.organization;

import org.onestep.relief.organization.model.Location;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type Location to a string and vice-versa.
 */

public class LocationTypeConverter implements DynamoDBTypeConverter<String, Location> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(Location location) {
        ObjectMapper objectMapper = new ObjectMapper();

        String jsonStr;
        try {
            jsonStr = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(location);
            log.debug("marshalled location to " + jsonStr);
        } catch (JsonProcessingException e) {
            log.error("error marshalling description " + location, e);
            throw new RuntimeException("Cannot write item");
        }

        return jsonStr;
    }

    @Override
    public Location unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        Location location;

        try {
            location = objectMapper.readValue(s, Location.class);
            log.debug("unmarshalled location to " + location);
        } catch (IOException e) {
            log.error("error de-marshalling location: " + s, e);
            return null;
        }

        return location;
    }
}
