import { Collapse } from 'antd'
import { type FC } from 'react'
import { useParams } from 'react-router-dom'
import { PageTitle } from '../../components'
import {
  skipToken,
  useAppSelector,
  useDeleteTaskMutation,
  useGetAccreditationEntitiesByIdQuery,
  useGetOrganizationsByPersonQuery,
  useGetPersonQuery,
  useGetProjectsByPersonQuery,
  useGetTasksByPersonQuery,
} from '../../store'
import { PersonHistoryTimeline } from './PersonHistoryTimeline'
import { PersonProfileCard } from './PersonProfileCard'
import { PersonSummaryCardMeta } from './PersonSummaryCardMeta'
import { PersonTasksDescriptions } from './PersonTasksDescriptions'

export const PersonDetail: FC = () => {
  const myPersonId = useAppSelector((state) => state.auth.person?.uuid)
  const { personId } = useParams<{ personId: string }>()

  const { isLoading: isLoadingPerson, data: person } = useGetPersonQuery(personId || skipToken)
  const { data: accreditationEntities } = useGetAccreditationEntitiesByIdQuery(
    personId || skipToken
  )
  const { data: organizations } = useGetOrganizationsByPersonQuery(personId || skipToken)
  const { data: projects } = useGetProjectsByPersonQuery(personId || skipToken)
  const { data: tasks } = useGetTasksByPersonQuery(personId || skipToken)

  const [deleteTask] = useDeleteTaskMutation()

  return (
    <div className='mx-8 flex flex-col'>
      <PageTitle>Member</PageTitle>

      <div className='mb-8 grid grid-cols-3 gap-4'>
        <PersonProfileCard
          className='col-span-3 md:col-span-1 h-min'
          loading={isLoadingPerson}
          person={person}
          accreditationEntities={accreditationEntities}
          editable={personId === myPersonId}
        />

        <Collapse
          className='col-span-3 md:col-span-2 h-min rounded-lg'
          defaultActiveKey={['summary', 'my-tasks', 'history']}
        >
          <Collapse.Panel header='Summary' key='summary'>
            <PersonSummaryCardMeta
              className='flex flex-col gap-8'
              person={person}
              organizations={organizations}
              projects={projects}
              tasks={tasks}
            />
          </Collapse.Panel>

          <Collapse.Panel header='My Tasks' key='my-tasks'>
            <PersonTasksDescriptions person={person} tasks={tasks} />
          </Collapse.Panel>

          <Collapse.Panel header='History' key='history'>
            <PersonHistoryTimeline person={person} />
          </Collapse.Panel>
        </Collapse>
      </div>
    </div>
  )
}
