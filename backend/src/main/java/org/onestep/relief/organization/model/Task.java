package org.onestep.relief.organization.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.onestep.relief.organization.model.DynamoDB.ObjectConverters.FileListTypeConverter;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {

    @DynamoDBHashKey
    @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
    private String id;

    @DynamoDBAttribute
    String name;

    @DynamoDBAttribute
    String description;

    @DynamoDBAttribute
    String assignedPerson;

    @DynamoDBAttribute
    Date creationDate = Calendar.getInstance().getTime();

    @DynamoDBAttribute
    Date dueDate;

    @DynamoDBAttribute
    Date completionDate;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    TaskStatus status;

    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    Visibility visibility;

    @DynamoDBAttribute
    @DynamoDBTypeConverted(converter = FileListTypeConverter.class)
    List<File> attachments = new ArrayList<>();

    @DynamoDBAttribute
    boolean isActive = true;

    public enum TaskStatus {
        NEW, IN_PROGRESS, COMPLETED
    }

    public enum Visibility {
        PUBLIC, PROTECTED, PRIVATE
    }
}
