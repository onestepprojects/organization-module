export const taskVisibilityOptions = [
  { label: 'PUBLIC', value: 'PUBLIC' },
  { label: 'PROTECTED', value: 'PROTECTED' },
  { label: 'PRIVATE', value: 'PRIVATE' },
]
