import { LeftOutlined, ReloadOutlined } from '@ant-design/icons'
import { Button, Result } from 'antd'
import { type FC } from 'react'
import { useNavigate } from 'react-router-dom'

interface ServerErrorProps {
  onReload?: () => any
}

const defaultReload = () => location.reload()

export const ServerError: FC<ServerErrorProps> = ({ onReload = defaultReload }) => {
  const navigate = useNavigate()

  return (
    <Result
      status='500'
      title='Server Error'
      subTitle='Sorry, something went wrong.'
      extra={
        <div className='space-x-8'>
          <Button type='primary' size='large' icon={<LeftOutlined />} onClick={() => navigate(-1)}>
            Go Back
          </Button>
          <Button type='primary' size='large' icon={<ReloadOutlined />} onClick={onReload}>
            Reload Page
          </Button>
        </div>
      }
    />
  )
}
