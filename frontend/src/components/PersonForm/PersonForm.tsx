import { useMemoizedFn } from 'ahooks'
import { Alert, Button, DatePicker, Form, Input, Select, Tabs, type FormProps } from 'antd'
import dayjs from 'dayjs'
import { useState, type FC } from 'react'

import { SingleImageUpload } from '../../components'
import { countryOptions, genderOptions, stateOptions } from '../../options'
import { type Person } from '../../store'
import { type Paths } from '../../utils/TypedPath'

interface PersonFormProps extends FormProps<Person> {
  initialValues?: Person
  submitButtonText?: string
}

export const PersonForm: FC<PersonFormProps> = (props) => {
  const { submitButtonText = 'Submit', onFinish, ...formProps } = props

  const [activeKey, setActiveKey] = useState('basic')
  const [isSubmitting, setIsSubmitting] = useState(false)

  const handleSubmit = useMemoizedFn(async (values) => {
    try {
      setIsSubmitting(true)
      await onFinish(values)
    } finally {
      setIsSubmitting(false)
    }
  })

  const handleFinishFailed = useMemoizedFn((errorInfo) => {
    switch (errorInfo.errorFields[0].name[0]) {
      case 'name':
      case 'gender':
      case 'birthdate':
        setActiveKey('basic')
        break
      case 'email':
      case 'phones':
        setActiveKey('contact')
        break
    }
  })

  return (
    <Form
      {...formProps}
      labelCol={{ span: 5 }}
      labelWrap
      onFinish={handleSubmit}
      onFinishFailed={handleFinishFailed}
    >
      <Tabs activeKey={activeKey} onChange={setActiveKey}>
        <Tabs.TabPane key='basic' tab='Basic'>
          <Form.Item label='Full Name' name={typedPath(['name'])} rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label='Upload Profile Image'
            name={typedPath(['profilePicture'])}
            extra='Please select a picture.'
          >
            <SingleImageUpload />
          </Form.Item>
          <Form.Item label='Gender' name={typedPath(['gender'])} rules={[{ required: true }]}>
            <Select options={genderOptions} allowClear />
          </Form.Item>
          <Form.Item
            label='Date of Birth'
            name={typedPath(['birthdate'])}
            rules={[{ required: true }]}
            // convert date string to dayjs obj which is required by DatePicker
            getValueProps={(v) => ({ value: v ? dayjs(v, 'YYYY-MM-DD') : v })}
            // convert dayjs obj from DatePicker to date string for person
            getValueFromEvent={(e) => (e ? dayjs(e).format('YYYY-MM-DD') : '')}
          >
            <DatePicker />
          </Form.Item>
        </Tabs.TabPane>

        <Tabs.TabPane key='contact' tab='Contact Information' forceRender>
          <Form.Item label='Email' name={typedPath(['email'])} rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label='Phone'
            name={typedPath(['phones', 'phone'])}
            rules={[{ required: true }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label='Mobile' name={typedPath(['phones', 'mobile'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Satellite Phone' name={typedPath(['phones', 'sat_phone'])}>
            <Input />
          </Form.Item>
        </Tabs.TabPane>

        <Tabs.TabPane key='address' tab='Home Address' forceRender>
          <Form.Item label='Address 1' name={typedPath(['currentAddress', 'address'])}>
            <Input />
          </Form.Item>
          <Form.Item label='City' name={typedPath(['currentAddress', 'city'])}>
            <Input />
          </Form.Item>
          <Form.Item label='District' name={typedPath(['currentAddress', 'district'])}>
            <Input />
          </Form.Item>
          <Form.Item label='State' name={typedPath(['currentAddress', 'state'])}>
            <Select allowClear showSearch options={stateOptions} />
          </Form.Item>
          <Form.Item label='Zip Code' name={typedPath(['currentAddress', 'postalCode'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Country' name={typedPath(['currentAddress', 'country'])}>
            <Select allowClear showSearch options={countryOptions} />
          </Form.Item>
        </Tabs.TabPane>

        <Tabs.TabPane key='pay-id' tab='PayID' forceRender>
          <Form.Item label='Payment Account Status' name={typedPath(['paymentAccount', 'status'])}>
            <PaymentStatus />
          </Form.Item>
          <Form.Item label='Pay ID' name={typedPath(['paymentAccount', 'payID'])}>
            <Input />
          </Form.Item>
        </Tabs.TabPane>

        <Tabs.TabPane key='media' tab='Media' forceRender>
          <Form.Item label='Twitter' name={typedPath(['socialMedia', 'twitter'])}>
            <Input />
          </Form.Item>
          <Form.Item label='LinkedIn' name={typedPath(['socialMedia', 'linkedin'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Facebook' name={typedPath(['socialMedia', 'facebook'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Youtube' name={typedPath(['socialMedia', 'youtube'])}>
            <Input />
          </Form.Item>
        </Tabs.TabPane>

        <Tabs.TabPane key='emergency' tab='Emergency Contact' forceRender>
          <Form.Item label='Emergency Contact Name' name={typedPath(['ecName'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Emergency Contact Phone' name={typedPath(['ecPhones', 'phone'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Emergency Contact Address' name={typedPath(['ecAddress', 'address'])}>
            <Input />
          </Form.Item>
          <Form.Item label='City' name={typedPath(['ecAddress', 'city'])}>
            <Input />
          </Form.Item>
          <Form.Item label='District' name={typedPath(['ecAddress', 'district'])}>
            <Input />
          </Form.Item>
          <Form.Item label='State' name={typedPath(['ecAddress', 'state'])}>
            <Select showSearch options={stateOptions} />
          </Form.Item>
          <Form.Item label='Zip Code' name={typedPath(['ecAddress', 'postalCode'])}>
            <Input />
          </Form.Item>
          <Form.Item label='Country' name={typedPath(['ecAddress', 'country'])}>
            <Select showSearch options={countryOptions} />
          </Form.Item>
        </Tabs.TabPane>
      </Tabs>

      <Form.Item wrapperCol={{ offset: 5 }}>
        <Button type='primary' htmlType='submit' loading={isSubmitting}>
          {submitButtonText}
        </Button>
      </Form.Item>
    </Form>
  )
}

function typedPath(paths: Paths<Person>): Paths<Person> {
  return paths
}

const PaymentStatus: FC<{ value?: string }> = ({ value }) => {
  if (!value) {
    return <>No provision status available</>
  }
  const typeMappings = {
    FAILED: 'error',
    PROVISIONED: 'success',
    PARTIALLY_PROVISIONED: 'warning',
    AWAITING_USER_ACTION: 'info',
  }
  return <Alert message={value.replace('_', '')} type={typeMappings[value]} />
}
