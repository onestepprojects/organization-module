package org.onestep.relief.organization.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Exception class for errors thrown by the organization service.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AuthenticationException extends Exception {

    private String action;
    private String reason;

    @Override
    public String getMessage() {
        return "ERROR: " + action + " - " + reason;
    }
}
