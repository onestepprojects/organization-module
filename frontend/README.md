# Organization Module Frontend

This project was bootstrapped with [Create Vite](https://vitejs.dev/guide/#scaffolding-your-first-vite-project).

## Getting started

### env file

Create a `.env.development.local` file under `frontend/`, and fill it from the latest content from the Slack channel [#environment-file](https://onestep-relief.slack.com/archives/C01TXR5R895)

### Developing with the app webpack dev server (Recommended)

Install dependencies and start module in watch mode.

```bash
# cd organization-module/frontend
npm install
npm start
```

Open the second terminal. Link module and start app dev server.

```bash
# cd one-step-ui
npm install
npm link ../organization-module/frontend
npm start
```

Note: we need also set a `.env.development` file under `one-step-ui`, refer to its README for more details.

Open https://localhost:3000/ in your browser.

The module will rebuild if you make edits.

### Developing with the vite dev server (Experimental)

```bash
# organization-module/frontend
npm install
npm run dev
```

Start a vite dev server and listens on https://localhost:3000/organizations/. It's fast and friendly for developing a single module.

### Test

`npm test`

Run jest unit tests.

### Build

`npm run build`

Builds the app for production to the `dist` folder. It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to be deployed!

See the section about [Build for Production](https://vitejs.dev/guide/build.html#library-mode) for more information.

## Routes

See [Container.tsx](./src/Container.tsx)

- `/organizations` - Dashboard base path
  - Organization
    - `/organizations` - Organization list
    - `/organizations/:organizationId/organizations` - Sub organization list
    - `/organizations/:organizationId` - Organization detail
    - `/organizations/create` - Organization creation
    - `/organizations/:organizationId/update` - Organization update
    - `/organizations/:organizationId/invite` - Invite an user to organization
    - `/organizations/:organizationId/join` - Request to join an organization
    - `/organizations/:organizationId/requests` - Pending join requests of organization
    - `/organizations/:organizationId/organizations` - Sub organization list
    - `/organizations/:organizationId/persons` - Organization person list
    - `/organizations/:organizationId/projects` - Organization project list
    - `/organizations/:organizationId/organizations/create` - Create sub organization
  - Person
    - `/persons` - Person list
    - `/persons/:personId` - Person detail
    - `/persons/:personId/update` - Update my profile
    - `/persons/:personId/organizations` - My organization list
    - `/persons/:personId/projects` - My project list
    - `/persons/:personId/tasks` - My task list
  - Project
    - `/projects` - Project list
    - `/projects/create` - Project creation
    - `/projects/:projectId` - Project detail
    - `/projects/:projectId/update` - Project update
    - `/projects/:projectId/invite` - Invite an user to project
    - `/projects/:projectId/join` - Request to join an project
    - `/projects/:projectId/requests` - Pending join requests of project
    - `/projects/:projectId/projects` - Sub project list
  - Task
    - `/tasks/create` - Task creation
    - `/tasks/:projectId/update` - Task update
  - Error
    - `/*` - Not found - 404 page

## Learn More

You can learn more in the [Vite documentation](https://vitejs.dev/guide/).

To learn React, check out the [React documentation](https://reactjs.org/).
