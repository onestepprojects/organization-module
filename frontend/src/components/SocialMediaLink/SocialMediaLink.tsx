import { type FC } from 'react'

export interface SocialMediaLinkProps {
  type: 'twitter' | 'facebook' | 'linkedin' | 'youtube'
  link: string
}

export const SocialMediaLink: FC<SocialMediaLinkProps> = ({ type, link }) => {
  if (!link) return null

  switch (type) {
    case 'twitter': {
      return (
        <IconLink
          link={link.startsWith('http') ? link : `https://twitter.com/${link}`}
          icon={type}
          color='#55acee'
        />
      )
    }
    case 'facebook': {
      return (
        <IconLink
          link={link.startsWith('http') ? link : `https://facebook.com/${link}`}
          icon={type}
          color='#3b5998'
        />
      )
    }
    case 'linkedin': {
      return (
        <IconLink
          link={link.startsWith('http') ? link : `https://linkedin.com/in/${link}`}
          icon={type}
          color='#0082ca'
        />
      )
    }
    case 'youtube': {
      return (
        <IconLink
          link={link.startsWith('http') ? link : `https://youtube.com/channel/${link}`}
          icon={type}
          color='#ed302f'
        />
      )
    }
    default:
      return null
  }
}

const IconLink: FC<{ link: string; icon: string; color: string }> = ({ link, icon, color }) => (
  <a href={link} target='_blank' rel='noopener noreferrer'>
    <i className={`fab fa-${icon} fa-2x`} style={{ color }} />
  </a>
)
