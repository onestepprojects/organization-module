import { Input, type InputProps } from 'antd'
import { type FC } from 'react'
import { useLazyGetGeographicPositionByAddressQuery } from '../../store'

interface Location {
  latitude: number
  longitude: number
}

interface GeoPositionInputProps extends Omit<InputProps, 'value' | 'onChange'> {
  value?: Location
  onChange?: (value?: Location) => void
}

export const GeoPositionInput: FC<GeoPositionInputProps> = (props) => {
  const { value, onChange, ...restProps } = props
  const [getGeographicPositionByAddress] = useLazyGetGeographicPositionByAddressQuery()

  async function handleChange(value?: string) {
    if (value) {
      const mapData = await getGeographicPositionByAddress(value).unwrap()
      onChange?.(mapData)
    }
  }

  return <Input {...restProps} onChange={(e) => handleChange(e.target.value)} />
}
