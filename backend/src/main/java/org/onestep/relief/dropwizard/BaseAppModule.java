package org.onestep.relief.dropwizard;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public abstract class BaseAppModule extends AbstractModule {

    @Provides
    public SystemEnvValues provideSystemEnvValues() {
        SystemEnvValues values = new SystemEnvValues();
        values.DDB_REGION_NAME = System.getenv("DDB_REGION_NAME");
        values.DDB_PREFIX = System.getenv("DDB_PREFIX");
        values.S3_REGION_NAME = System.getenv("S3_REGION_NAME");
        values.APP_BASE_URL = System.getenv("APP_BASE_URL");
        values.HEALTH_BASE_URL = System.getenv("HEALTH_BASE_URL");
        values.SERVICE_CLIENT_ID = System.getenv("SERVICE_CLIENT_ID");
        values.SERVICE_CLIENT_SECRET = System.getenv("SERVICE_CLIENT_SECRET");
        values.AUTH_USER_PATH = System.getenv("AUTH_USER_PATH");
        values.AUTH_SERVICE_LOGIN_PATH = System.getenv("AUTH_SERVICE_LOGIN_PATH");
        values.BIND_COGNITO_ROLE_PATH = System.getenv("BIND_COGNITO_ROLE_PATH");
        values.PROVISION_PAY_USER_PATH = System.getenv("PROVISION_PAY_USER_PATH");
        values.ISSUE_REWARD_PATH = System.getenv("ISSUE_REWARD_PATH");
        values.GET_REWARDS_PATH = System.getenv("GET_REWARDS_PATH");
        values.PAGO_PERSON_CALLBACK_PATH = System.getenv("PAGO_PERSON_CALLBACK_PATH");
        values.PAGO_ORG_CALLBACK_PATH = System.getenv("PAGO_ORG_CALLBACK_PATH");
        values.ORG_LOGO_BUCKET = System.getenv("ORG_LOGO_BUCKET");
        values.PROJECT_PIC_BUCKET = System.getenv("PROJECT_PIC_BUCKET");
        values.PERSON_PIC_BUCKET = System.getenv("PERSON_PIC_BUCKET");
        values.ORG_PIC_DEFAULT = System.getenv("ORG_PIC_DEFAULT");
        values.PROJECT_PIC_DEFAULT = System.getenv("PROJECT_PIC_DEFAULT");
        values.PERSON_PIC_DEFAULT = System.getenv("PERSON_PIC_DEFAULT");
        return values;
    }
}
