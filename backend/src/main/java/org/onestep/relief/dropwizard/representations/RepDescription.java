package org.onestep.relief.dropwizard.representations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RepDescription {
    private String description;
    private List<RepLocation> serviceArea;
    private List<String> projectTypes;
}
