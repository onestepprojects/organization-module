package org.onestep.relief;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class TestUtils {

    public static String toJson(Object obj) throws JsonProcessingException {
        if (obj == null) {
            return null;
        }
        return new ObjectMapper().writeValueAsString(obj);
    }

    public static String seed() {
        return UUID.randomUUID().toString();
    }

    public static String readFixture(Class<?> clazz, String fileName) throws IOException {
        String dir = clazz.getPackage().getName().replace('.', '/');
        String fixtureFile = "src/test/java/" + dir + "/" + fileName;
        return FileUtils.readFileToString(new File(fixtureFile), StandardCharsets.UTF_8);
    }
}
