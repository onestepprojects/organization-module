import { ScheduleOutlined } from '@ant-design/icons'
import { Space } from 'antd'
import { type Task } from '../../store'
import { type TaskNode } from './typings'

export function buildTaskNode(task: Task): TaskNode {
  const node: TaskNode = {
    type: 'TaskNode',
    key: task.id,
    title: (
      <Space>
        <ScheduleOutlined className='text-green-500' />
        {task.name}
      </Space>
    ),
    task,
    isLeaf: true,
  }
  return node
}
