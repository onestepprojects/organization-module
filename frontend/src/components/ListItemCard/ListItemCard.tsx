import { Button, Card, Image, Typography } from 'antd'
import { type FC } from 'react'
import { Link } from 'react-router-dom'

export interface ListItemCardProps {
  cover?: string
  coverFallback?: string
  title: string
  description?: string
  link: string
}

export const ListItemCard: FC<ListItemCardProps> = ({
  cover,
  coverFallback,
  title,
  description,
  link,
}) => {
  return (
    <Card
      className='w-64 border-neutral-200 rounded-xl shadow'
      bordered={false}
      cover={
        <Image
          className='!rounded-t-xl object-cover'
          src={cover}
          fallback={coverFallback}
          alt='card cover'
          height={192}
          preview={false}
        />
      }
      actions={[
        <Link to={link}>
          <Button type='primary'>View</Button>
        </Link>,
      ]}
    >
      <Card.Meta
        title={
          <Typography.Title level={3} ellipsis>
            {title}
          </Typography.Title>
        }
        description={
          <Typography.Paragraph className='min-h-[44px]' ellipsis={{ rows: 2 }}>
            {description}
          </Typography.Paragraph>
        }
      />
    </Card>
  )
}
