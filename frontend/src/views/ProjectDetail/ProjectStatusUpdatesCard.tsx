import { DeleteOutlined } from '@ant-design/icons'
import { useMemoizedFn } from 'ahooks'
import {
  Button,
  Card,
  Col,
  Empty,
  Form,
  Input,
  Modal,
  Popconfirm,
  Row,
  Timeline,
  Typography,
  type FormInstance,
} from 'antd'
import { useRef, useState, type FC } from 'react'
import type { Project, ProjectStatusUpdate } from '../../store'
import { formatUnixTimestamp } from './utils'

export interface ProjectStatusUpdatesCardProps {
  loading?: boolean
  project: Project
  myPersonId: string
  isAdmin?: boolean
  createProjectStatusUpdate?: (statusUpdate: ProjectStatusUpdate) => Promise<ProjectStatusUpdate>
  removeProjectStatusUpdate?: (statusUpdate: ProjectStatusUpdate) => Promise<ProjectStatusUpdate>
}

export const ProjectStatusUpdatesCard: FC<ProjectStatusUpdatesCardProps> = ({
  loading,
  project,
  myPersonId,
  isAdmin,
  createProjectStatusUpdate,
  removeProjectStatusUpdate,
}) => {
  const statusUpdates = project?.statusUpdatesHistory ?? []

  const formRef = useRef<FormInstance<ProjectStatusUpdate>>()

  // Set the status updates to show all or only 3 items
  const [showAllStatus, setShowAllStatus] = useState(false)

  const handleCreateStatusUpdate = useMemoizedFn(() => {
    Modal.confirm({
      centered: true,
      icon: false,
      closable: true,
      maskClosable: false,
      title: <Typography.Title level={3}>Create Status Update</Typography.Title>,
      content: (
        <Form ref={formRef}>
          <Form.Item
            name='title'
            rules={[{ required: true }, { min: 4, message: "'title' is too short" }]}
          >
            <Input placeholder='Title' />
          </Form.Item>
          <Form.Item
            name='description'
            rules={[{ required: true }, { min: 10, message: "'description' is too short" }]}
          >
            <Input placeholder='Description' />
          </Form.Item>
        </Form>
      ),
      okText: 'Create',
      onOk: async () => {
        const values = await formRef.current?.validateFields()
        if (!values) return true
        const statusUpdate = {
          ...values,
          authorID: myPersonId,
          date: Date.now(),
          projectId: project.id,
        }
        await createProjectStatusUpdate(statusUpdate)
      },
    })
  })

  return (
    <Card
      className='rounded-lg'
      title='Status Updates'
      loading={loading}
      extra={
        isAdmin && (
          <Button
            type='primary'
            ghost
            icon={<i className='mr-2 fas fa-plus-circle' />}
            onClick={handleCreateStatusUpdate}
          >
            Create
          </Button>
        )
      }
    >
      {statusUpdates.length ? (
        <Timeline>
          {statusUpdates
            .slice(0, showAllStatus ? statusUpdates.length : 3)
            .map((statusUpdate, index) => (
              <Timeline.Item className='group' key={index} color={!index ? 'green' : 'blue'}>
                <time className='mr-2 text-slate-500'>
                  {formatUnixTimestamp(statusUpdate.date)}
                </time>
                <div className='font-medium'>{statusUpdate.title}</div>
                <div className='text-slate-500'>{statusUpdate.description}</div>
                {isAdmin && (
                  <Popconfirm
                    title='Are you sure?'
                    description='Are you sure to delete this status?'
                    okText='Remove'
                    okButtonProps={{ danger: true }}
                    onConfirm={() => removeProjectStatusUpdate(statusUpdate)}
                  >
                    <Button
                      className='hidden group-hover:block absolute right-1 top-1'
                      type='link'
                      size='small'
                      danger
                      icon={<DeleteOutlined className='text-sm' />}
                    />
                  </Popconfirm>
                )}
              </Timeline.Item>
            ))}
        </Timeline>
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description='No status' />
      )}

      {statusUpdates.length > 3 ? (
        <Row justify='end'>
          <Col>
            <Button onClick={() => setShowAllStatus(!showAllStatus)}>
              {showAllStatus ? 'Show Latest' : 'Show All'}
            </Button>
          </Col>
        </Row>
      ) : null}
    </Card>
  )
}
