package org.onestep.relief.organization.model;

import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents the information under organization description section The info consists of 3 parts - short
 * description of the organization, its geographical service area and the types of projects they're working on
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Description {

    private String description;
    private List<Location> serviceArea = new ArrayList<>();
    private List<String> projectTypes = new ArrayList<>();

    public void addServiceArea(Location location) {
        serviceArea.add(location);
    }

    public void removeServiceArea(Location location) {
        serviceArea.remove(location);
    }

    public void addProjectType(String projectType) {
        projectTypes.add(projectType);
    }

    public void removeProjectType(String projectType) {
        projectTypes.remove(projectType);
    }

}
