import { buildOrganizationNode } from './buildOrganizationNode'
import { type OrganizationNode, type RootNode } from './typings'

export function buildRootNode(): RootNode {
  const node: RootNode = {
    type: 'root',
    key: 'root',
    title: 'Organizations',
    calcChildren(): OrganizationNode[] | undefined {
      if (node.organizations === undefined) {
        return undefined
      }
      return node.organizations.map((it) => buildOrganizationNode(it))
    },
  }
  return node
}
