import { Button, Card, Image, Popconfirm } from 'antd'
import classNames from 'classnames'
import { lowerCase } from 'lodash-es'
import { type FC } from 'react'
import { Link } from 'react-router-dom'
import { AccreditationDescriptions, SocialMediaLinkList } from '../../components'
import type { AccreditationEntity, Person } from '../../store'

interface PersonProfileCardProps {
  className?: string
  loading?: boolean
  person: Person
  accreditationEntities?: AccreditationEntity[]
  editable?: boolean
}

export const PersonProfileCard: FC<PersonProfileCardProps> = ({
  className,
  loading,
  person,
  accreditationEntities,
  editable,
}) => {
  return (
    <Card
      className={classNames(className, 'rounded-lg')}
      loading={loading}
      cover={
        <Image
          className='!rounded-t-lg'
          preview={false}
          src={person?.profilePicture?.url}
          alt='Profile picture'
        />
      }
      actions={[
        editable && (
          <Link to={`/persons/${person?.uuid}/update`}>
            <Button type='primary' icon={<i className='mr-2 fas fa-edit' />}>
              Edit Profile
            </Button>
          </Link>
        ),
        editable && (
          <Popconfirm
            title='Are you sure you wish to delete yourself?'
            okText='Delete'
            okButtonProps={{ danger: true }}
            onConfirm={async () => {
              // TODO: Should we allow user delete themselves?
            }}
          >
            <Button type='primary' danger icon={<i className='mr-2 fas fa-trash' />}>
              Delete Profile
            </Button>
          </Popconfirm>
        ),
      ].filter(Boolean)}
    >
      <Card.Meta
        className='text-center'
        title={person?.name}
        description={
          <div className='space-y-3'>
            {person?.currentAddress && (
              <div>
                Location:{' '}
                {[person.currentAddress.city, person.currentAddress.state]
                  .filter(Boolean)
                  .join(', ')}
              </div>
            )}
            {person?.email && (
              <div>
                <a
                  href={`mailto:${person.email}`}
                  target='_blank'
                  rel='noopener noreferrer'
                  title='Profile email'
                >
                  {person.email}
                </a>
              </div>
            )}
            <AccreditationDescriptions accreditationEntities={accreditationEntities} />
            {editable && person?.paymentAccount?.status && (
              <div>
                Payid Status:{' '}
                <span
                  className={
                    {
                      PROVISIONED: 'text-green-500',
                      PARTIALLY_PROVISIONED: 'text-yellow-500',
                      AWAITING_USER_ACTION: 'text-blue-500',
                      FAILED: 'text-red-500',
                    }[person.paymentAccount.status]
                  }
                >
                  {lowerCase(person.paymentAccount.status.toLowerCase())}
                </span>
              </div>
            )}
            {person?.socialMedia && <SocialMediaLinkList socialMedia={person.socialMedia} />}
          </div>
        }
      />
    </Card>
  )
}
