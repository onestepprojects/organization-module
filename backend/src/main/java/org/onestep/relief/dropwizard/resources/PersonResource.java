package org.onestep.relief.dropwizard.resources;

import com.google.inject.Inject;
import org.dozer.DozerBeanMapper;
import org.onestep.relief.dropwizard.representations.EventRepr;
import org.onestep.relief.dropwizard.representations.FileRepr;
import org.onestep.relief.dropwizard.representations.PersonRepr;
import org.onestep.relief.dropwizard.representations.RolesTripleResponse;
import org.onestep.relief.organization.model.*;
import org.onestep.relief.organization.service.OrgServiceInterface;
import org.onestep.relief.organization.service.OrganizationServiceException;
import org.onestep.relief.organization.service.OrganizationServiceImpl;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Path("/persons")
@Produces({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
public class PersonResource {
    private final OrgServiceInterface orgInterface;
    private final DozerBeanMapper dtoMapper;
    private final Validator validator;

    /**
     * Constructor initializes a Dozer mapper, a Validator, and the model interface.
     *
     * @param validator
     *            Validator class received from environment
     */
    @Inject
    public PersonResource(OrganizationServiceImpl orgInterface, Validator validator) {
        DozerBeanMapper mapper = new DozerBeanMapper();

        List<String> myList = new ArrayList<>();
        myList.add("dozerMapping.xml");
        mapper.setMappingFiles(myList);

        this.dtoMapper = mapper;
        this.validator = validator;
        this.orgInterface = orgInterface;
    }

    @GET
    public Response getPersons() {
        return Response.status(Status.OK).entity(
                orgInterface.getAllPersons().stream().map(p -> dtoMapper.map(p, PersonRepr.class)).collect(toList()))
                .build();
    }

    @GET
    @Path("/{id}")
    public Response getPersonById(@PathParam("id") String id,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            PersonRepr personRepr = dtoMapper.map(orgInterface.getPersonById(authToken.orElse("UNKNOWN"), id),
                    PersonRepr.class);

            return Response.status(Status.OK).entity(personRepr).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/by-cognito/{cognitoId}")
    public Response getPersonByCognitoId(@PathParam("cognitoId") String cognitoId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            PersonRepr personRepr = dtoMapper
                    .map(orgInterface.getPersonByCognitoId(authToken.orElse("UNKNOWN"), cognitoId), PersonRepr.class);

            return Response.status(Status.OK).entity(personRepr).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/requester/token")
    public Response getRequesterPerson(@HeaderParam("Authorization") Optional<String> authToken) {

        try {
            PersonRepr personRepr = dtoMapper.map(orgInterface.getRequesterPerson(authToken.orElse("UNKNOWN")),
                    PersonRepr.class);

            return Response.status(Status.OK).entity(personRepr).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/get-by-parent/{parentId}")
    public Response getPersonsByParent(@PathParam("parentId") String parentId) {

        try {
            Map<Person, Set<Role>> persons = orgInterface.getPersonsByParent(parentId);
            return Response.status(Status.OK).entity(RolesTripleResponse.personTriples(persons)).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @GET
    @Path("/provision-asset/{personId}/{assetId}")
    public Response provision(@PathParam("personId") String personId, @PathParam("assetId") String assetId) {

        try {
            orgInterface.provisionPersonAsset(personId, assetId);

            return Response.status(Status.OK).entity("DONE").build();

        } catch (OrganizationServiceException exception) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, exception.getMessage());
        }
    }

    @POST
    @Path("/create")

    public Response createPerson(PersonRepr personRequest, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            Utils.buildBadResponse(Status.BAD_REQUEST, "Authentication token required.");

        if (personRequest.getUuid() != null)
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Person UUID must be null.");

        if (ReliefValidators.invalidPhoneLabels(personRequest))
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Invalid phone labels.");

        Set<ConstraintViolation<PersonRepr>> violations = validator.validate(personRequest);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Status.BAD_REQUEST, Utils.getViolation(violations));

        try {
            Person person = dtoMapper.map(personRequest, Person.class);
            PersonRepr personResp = dtoMapper.map(orgInterface.createPerson(authToken.orElse("UNKNOWN"), person),
                    PersonRepr.class);

            return Response.status(Status.OK).entity(personResp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("/update")

    public Response updatePerson(PersonRepr personRequest, @HeaderParam("Authorization") Optional<String> authToken) {

        if (!authToken.isPresent())
            Utils.buildBadResponse(Status.BAD_REQUEST, "Authentication token required.");

        Set<ConstraintViolation<PersonRepr>> violations = validator.validate(personRequest);
        if (violations.size() > 0)
            return Utils.buildBadResponse(Status.BAD_REQUEST, Utils.getViolation(violations));

        if (ReliefValidators.invalidPhoneLabels(personRequest))
            return Utils.buildBadResponse(Status.BAD_REQUEST, "Invalid phone labels.");

        try {
            Person person = dtoMapper.map(personRequest, Person.class);
            Person newPerson = orgInterface.updatePerson(authToken.orElse("UNKNOWN"), person);
            PersonRepr personResp = dtoMapper.map(newPerson, PersonRepr.class);

            return Response.status(Status.OK).entity(personResp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, e.getMessage());
        }
    }

    @DELETE
    @Path("/delete/{personId}")
    public Response deletePerson(@PathParam("personId") String personId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            orgInterface.deletePerson(authToken.orElse("UNKNOWN"), personId);

            return Response.status(Status.OK).entity("DONE").build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.NOT_FOUND, e.getMessage());
        }
    }

    @POST
    @Path("/create-event/{personId}")
    public Response createEvent(EventRepr eventRepr, @PathParam("personId") String personId,
            @HeaderParam("Authorization") Optional<String> authToken) {

        try {
            Event event = dtoMapper.map(eventRepr, Event.class);
            Event newEvent = orgInterface.createEvent(authToken.orElse("UNKNOWN"), personId, event.getActivity(),
                    event.getMessage(), event.getParentType(), event.getParentID());

            return Response.status(Status.OK).entity(dtoMapper.map(newEvent, EventRepr.class)).build();

        } catch (OrganizationServiceException e) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, e.getMessage());
        }
    }

    @POST
    @Path("update-image/{personId}")
    public Response updateImage(FileRepr fileRepr, @PathParam("personId") String personId,
            @HeaderParam("Authorization") String authToken) {

        try {
            File file = dtoMapper.map(fileRepr, File.class);

            Person person = orgInterface.updatePersonImage(authToken, personId, file);
            PersonRepr personResp = dtoMapper.map(person, PersonRepr.class);

            return Response.status(Status.OK).entity(personResp).build();

        } catch (OrganizationServiceException | AuthenticationException e) {
            return Utils.buildBadResponse(Status.BAD_REQUEST, e.getMessage());
        }
    }
}
