package org.onestep.relief.organization.model.DynamoDB.ObjectConverters;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.organization.model.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Converts the complex type File to a string and vice-versa.
 */

public class FileTypeConverter implements DynamoDBTypeConverter<String, File> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String convert(File file) {
        return DynamoDBTypeConverterHelper.getInstance().convert(file, "file", log);
    }

    @Override
    public File unconvert(String s) {
        ObjectMapper objectMapper = new ObjectMapper();
        File file;

        try {
            file = objectMapper.readValue(s, File.class);
            log.debug("unmarshalled file to " + file);
        } catch (IOException e) {
            log.error("error de-marshalling file: " + s, e);
            return null;
        }
        return file;
    }
}
