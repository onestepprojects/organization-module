package org.onestep.relief.organization.model;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IssueRewardRequest {

    private String reason;
    private String rewardType;
    private int amount;
    private String receiverId;
    private String receiverType;
}